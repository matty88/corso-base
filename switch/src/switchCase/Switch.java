package switchCase;

import java.util.Scanner;

public class Switch {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int scelta;
		do{
			System.out.println("MENU");
			System.out.println("1)INSERIMENTO");
			System.out.println("2)MODIFICA");
			System.out.println("3)CANCELLA");
			System.out.println("4)LEGGI");
			System.out.println("5)LEGGI TUTTI");
			System.out.println("6)FINE");
			System.out.println("SCEGLI:");
			scelta=Integer.parseInt(scanner.nextLine());
			switch(scelta) {           //COME SE FOSSE IF CHE SI USA QUANDOABBIAMO UNA SEQUENZA DI VALORI
			case 1:
				//inserimento
				//continue;            //INTERROMPI LOOP E VAI A QUELLO SEGUENTE
				break;                 //TI PERMETTE DI INTERROMPERE QUALUNQUE CICLO
			case 2:
				//modifica
				break; 
			case 3:
				//cancella
				break; 
			case 4:
				//leggi
				break; 
			case 5:
				//leggi tutti
				break; 
			case 6:
				break;
			default:
				System.out.println("Valore non consetito, inserisci un valore da 1 a 6");   
			}
		} while(scelta!=6);
	}
}


