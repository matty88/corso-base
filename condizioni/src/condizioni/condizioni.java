package condizioni;

//UN INTERAZIONE PERMETTE DI ESEGUIRE UNA PORZIONE DI CODICE SECONDO UNA CONDIZIONE
//SE CONDIZIONE E' VERA VIENE ESEGUITA, ALTRIMENTI VIENE ESEGUITO L'ELSE
//IF-ELSE = BINARIO
//IF-ELSE-ELSE IF = ENNARIO


public class condizioni {

	public static void main(String[] args) {
		int numero;
		int numero2;
		String nome;
		String nome2;

		//---CONDIZIONE IF/ELSE
		numero = 10;
		if(numero %5 ==0) {
			System.out.println("Pari");
		} else {
			System.out.println("Dispari");
		}

		//---CONDIZIONE IF/ELSE CON OPERATORI LOGICI
		numero2 = 7;
		if(numero2 >=5 && numero2 <=10) {
			System.out.println("Vero");
		} else {
			System.out.println("Falso");
		}

		if(numero2 >=5 || numero <=10) {  
			System.out.println("Vero");  
		} else { 
			System.out.println("Falso"); 
		}

		//--CONFRONTO TRA STRINGHE
		nome = "Mario";
		nome2 = "Roberto";
		if(nome.equals(nome2)) {
			System.out.println("Vero"); 
		} else {
			System.out.println("Falso"); 
		}

		//---CONFRONTO TRA STRINGHE TRAMITE METODI (IN BASE AL CODICE ASCII DA SX  A DX) CON ELSE IF
		if(nome.compareTo(nome2)>0) {
			System.out.println(nome+" � maggiore"); 
		} else if (nome.compareTo(nome2)==0) {
			System.out.println(nome+" e "+nome2+" sono uguali");
		} else {
			System.out.println(nome2+" � maggiore");
		}
	}

	public void visualizzaPari() {
		for(int x =1; x<=10;x++) {
			if(x%2==0) {
				System.out.println(x);
			}
		}
	}

	public void visualizzaPariBis() {
		for(int i=2; i<=10; i+=+2)
			System.out.println(i);
	}

	public void visualizzaPariTris() {
		for(int i=1; i<=10; i++) {
			if(i%2!=0) {
				continue;              //INTERROMPI LOOP SE DISPARI E VAI A QUELLO SEGUENTE
			}
			System.out.println(i);
		}
	}

}
