package view;

public class Test {

	public void itera(int inizio,int fine) {
		String a ="*";
		String somma = "";
		for(int x=inizio;x<fine;x++) {
			somma+=a;
			System.out.println(somma);
		}
	}

	public void iteraFor(int[] arr) {
		if(arr != null) {                       
			for (int i : arr) {  
				System.out.println(i);
			}
		}
	}

	public void iteraForDue() {
		int[] var = new int[5];
		var[0]=1;
		var[1]=12;
		var[2]=15;
		var[3]=4;
		var[4]=23;
		for(int i: var) {
			System.out.println(i);
		}
	}
}
