package vettori;
//VETTORE E' UNA VARIABILE MULTIDIMENSIONALE, OVVERO UN CONTENITORE DI VARIABILI

public class vettori {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
vettori();
	}

//METODO CON VETTORE CON CICLO FOR
	public static void vettori() {
int[] v = new int[5];
v[0] = 10;    
v[1] = 20;    
v[2] = 30;
v[3] = 40;
v[4] = 50;
for (int i=0; i<=4; i++) {
	System.out.println(v[i]);
}

//METODO CON VETTORE CON CICLO WHILE
String[] caratteri = new String[10];
String carattereStop;
int index;
caratteri[0]="A";
caratteri[1]="B";
caratteri[2]=",";
caratteri[3]="!";
caratteri[4]="*";
caratteri[5]="{";
caratteri[6]="}";
caratteri[7]="T";
caratteri[8]=" ";
caratteri[9]="/";
carattereStop="*";
index = 0;
while (caratteri[index]!=carattereStop) {
	System.out.println(caratteri[index]);
	index++;
}

	}

}
