<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dettaglio utente</title>
</head>
<body>

	<a href="/SpringSoft3/viewController">Ritorna alla tabella degli utenti</a>

	<h2><b>Dettaglio utente</b></h2>
	<br>
	<p><b>Nome:</b> ${u.getNome()}</p>
	<br>
	<p><b>Cognome:</b> ${u.getCognome()}</p>
	<br>
	<p><b>Username:</b> ${a.getUsername()}</p>
	<br>
	<p><b>Email:</b> ${a.getEmail()}</p>

</body>
</html>