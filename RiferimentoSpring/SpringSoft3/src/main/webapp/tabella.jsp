<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tabella utenti</title>
</head>
<body>
	
	<a href="home.jsp">Torna all'home page</a>
	
	<table border="1">
	<tr>
	<th>Username</th>
	<th>Password</th>
	<th>Email</th>
	<th>Dettaglio</th>
	<th>Aggiungi un progetto</th>
	<th>Modifica</th>
	<th>Cancella</th>
	</tr>
	<c:forEach items="${listaAccount}"  var="a" >
	<tr>
	<td>${a.getUsername()}</td>
	<td>${a.getPassword()}</td>
	<td>${a.getEmail()}</td>
	<td><a href="searchController/cercaDip?id=${a.getId()}">Dettaglio</a></td>
	<td><a href="viewController3?idutente=${a.getId()}">Aggiungi</a></td>
	<td><a href="updateController1/modificaUtenti1?id=${a.getId()}">Modifica</a></td>
	<td><a href="deleteController/cancellaUtente?id=${a.getId()}">Cancella</a></td>
	</tr>
	</c:forEach>
	</table>

</body>
</html>