<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tabella utenti</title>
</head>
<body>
	
	<a href="home.jsp">Torna all'home page</a>
	
	<table border="1">
	<tr>
	<th>Progetto</th>
	<th>Descrizione</th>
	<th>Dipendenti</th>
	<th>Modifica</th>
	<th>Cancella</th>
	</tr>
	<c:forEach items="${listaProgetti}"  var="p" >
	<tr>
	<td>${p.getProgetto()}</td>
	<td>${p.getDescrizione()}</td>
	<td>${p.getUtenti()}</td>
	<th><a href="updateController1/...?id=${a.getId()}">Modifica</a></th> 
	<th><a href="deleteController/...?id=${a.getId()}">Cancella</a></th>
	</tr>
	</c:forEach>
	</table>

</body>
</html>