<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>        
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Form di modifica</title>
</head>
<body>

	<form action="/SpringSoft3/updateController2/modificaUtenti2" method="post">
	<label>Username</label>
	<input type="text" name="username" value="${a.getUsername()}">
	<br>
	<br>
	<label>Password</label>
	<input type="password" name="password" value="${a.getPassword()}">
	<br>
	<br>
	<label>Email</label>
	<input type="text" name="email" value="${a.getEmail()}">
	<br>
	<br>
	<select name="ruolo">
	<option value="admin">Admin</option>
	<option value="guest">Guest</option>
	</select>
	<br>
	<br>
	<label>Nome</label>
	<input type="text" name="nome" value="${u.getNome()}">
	<br>
	<br>
	<label>Cognome</label>
	<input type="text" name="cognome" value="${u.getCognome()}">
	<br>
	<input type="hidden" name="id" value="${u.getId()}">
	<br>
	<br>
	<button type="submit">Submit</button>
	</form>

</body>
</html>