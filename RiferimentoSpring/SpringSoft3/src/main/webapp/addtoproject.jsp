<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Aggiungi al progetto</title>
</head>
<body>
	
	<a href="home.jsp">Torna all'home page</a>
	
	<form action="insertController/insertIntoProgetto" method="post">
	<select name="idprogetto">
	<c:forEach items="${listaProgetti}"  var="p" >
	<option value="${p.getId()}">${p.getProgetto()}</option>
	</c:forEach>
	</select>
	<input type="hidden" name="idutente" value="${u.getId()}">
	<br>
	<button type="submit">Aggiungi</button>
	</form>

</body>
</html>