package com.example.SpringSoft3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.model.Progetto;
import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/viewController3"})
public class VisualizzaPjUtenti {

	@Autowired
	private Services service;
	
	@RequestMapping(name="viewProgettiUtenti", method=RequestMethod.GET)
	public String visualizzaProgetti(@RequestParam String idutente, ModelMap model) { //model è l'equivalente di HttpServletRequest
		List<Progetto> listaProgetti = service.getAllPj();
		int id=Integer.parseInt(idutente);
		Utente u = service.getUtente(id);
		model.addAttribute("listaProgetti", listaProgetti);
		model.addAttribute("u", u);
		
		return "addtoproject.jsp";
	}
	
	
	
}
