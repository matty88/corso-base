package com.example.SpringSoft3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.SpringSoft3.model.Utente;

public interface RepoUtente extends JpaRepository<Utente, Integer>{

	Utente findById(int id);
	
}
