package com.example.SpringSoft3.service.interfaces;

import java.util.List;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.model.Progetto;
import com.example.SpringSoft3.model.Ruolo;
import com.example.SpringSoft3.model.Utente;

public interface Services {
	
	//Inserisco utente e account
	void inserisciUtente(Utente u);
	
	//Cerco il ruolo
	Ruolo getRu(String ruolo);
	
	//Cerco tutti gli utenti
	List<Utente> getAllUtenti();
	
	//Cerco tutti gli account
	List<Account> getAllAcc();
	
	//Cerco un utente tramite id
	Utente getUtente(int id);
	
	//Cerco un account tramite id
	Account getAcc(int id);
	
	//Cancello utente e account
	void cancellaUtente(Utente u);
	
	//Inserisco progetto
	void inserisciProgetto(Progetto p);
	
	//Cerco tutti i progetti
	List<Progetto> getAllPj();
	
	//Cerco progetto tramite id
	Progetto getPj(int id);
	
	
}
