package com.example.SpringSoft3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/deleteController"})
public class Cancella {

	@Autowired
	private Services service;
	
	@RequestMapping(value="cancellaUtente", method=RequestMethod.GET)
	public String rimozione(@RequestParam String id) {
		
		int idaccount=Integer.parseInt(id);
		
		Utente u=service.getUtente(idaccount);
		service.cancellaUtente(u);
		
		return "home.jsp";
	}
	
}
