package com.example.SpringSoft3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringSoft3.model.Account;

public interface RepoAccount extends JpaRepository<Account, Integer> {

	Account findById(int id);

	
}
