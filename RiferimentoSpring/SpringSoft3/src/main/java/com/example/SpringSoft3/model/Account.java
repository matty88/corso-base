package com.example.SpringSoft3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="accounts")
public class Account {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	public void setId(int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}
	
	@Column
	private String username;
	
	public void setUsername(String username) {
		this.username=username;
	}
	
	public String getUsername() {
		return username;
	}
	
	@Column
	private String password;
	
	public void setPassword(String password) {
		this.password=password;
	}
	
	public String getPassword() {
		return password;
	}
	
	@Column
	private String email;
	
	public void setEmail(String email) {
		this.email=email;
	}
	
	public String getEmail() {
		return email;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_utente",nullable=false) //con @JoinColumn definisco la chiave esterna (nella oneTOone non occorre specificare la pk di utente)
	private Utente utente;
	
	public void setUtente(Utente utente) {
		this.utente=utente;
	}
	
	public Utente getUtente() {
		return utente;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_ruolo", referencedColumnName="id") //id è la pk nella tabella ruoli
	private Ruolo ruolo;
	
	public void setRuolo(Ruolo ruolo) {
		this.ruolo=ruolo;
	}
	
	public Ruolo getRuolo() {
		return ruolo;
	}
	
}
