package com.example.SpringSoft3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringSoft3.model.Ruolo;

public interface RepoRuolo extends JpaRepository<Ruolo, Integer>{

	Ruolo findByRuolo(String ruolo);
	
}


