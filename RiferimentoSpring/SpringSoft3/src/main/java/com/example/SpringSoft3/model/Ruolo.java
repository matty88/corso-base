package com.example.SpringSoft3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ruoli")
public class Ruolo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private int id;
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	@Column(name="ruolo", nullable=false)
	private String ruolo;
	
	public void setRuolo(String ruolo) {
		this.ruolo=ruolo;
	}
	
	public String getRuolo() {
		return ruolo;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ruolo", cascade=CascadeType.ALL) //se cancello ruolo, elimino tutti gli account collegati ad esso
	private List<Account> account = new ArrayList<Account>(); //con lazy alloco in memoria la lista solo se utilizzata
	
	public void setAccount(List<Account> account) {
		this.account=account;
	}
	
	public List<Account> getAccount(){
		return account;
	}
	
	
	
}
