package com.example.SpringSoft3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/updateController1"})
public class Modifica1 {
	
	@Autowired
	private Services service;
	
	@RequestMapping(value="modificaUtenti1", method=RequestMethod.GET)
	public String modifica1(@RequestParam int id, ModelMap model) {
		model.addAttribute("a", service.getAcc(id));
		model.addAttribute("u", service.getUtente(id));
		
		return "modifica.jsp";
	}
	
}
