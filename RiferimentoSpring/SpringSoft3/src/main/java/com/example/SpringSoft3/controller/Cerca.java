package com.example.SpringSoft3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/searchController"})
public class Cerca {

	@Autowired
	private Services service;
	
	@RequestMapping(value="cercaDip", method=RequestMethod.GET)
	public String cercaDipendente(@RequestParam String id, ModelMap model) {
		
		int idutente=Integer.parseInt(id);
		
		Utente u=service.getUtente(idutente);
		Account a=service.getAcc(idutente);
		
		model.addAttribute("u", u);
		model.addAttribute("a", a);
		
		return "dettaglio.jsp";
	}
	
}
