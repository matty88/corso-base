package com.example.SpringSoft3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/viewController"})
public class Visualizza {

	@Autowired
	private Services service;
	
	@RequestMapping(name="viewUtenti", method=RequestMethod.GET)
	public String visualizza(ModelMap model) { //model è l'equivalente di HttpServletRequest
		List<Account> listaAccount = service.getAllAcc();
		model.addAttribute("listaAccount", listaAccount);
		
		return "tabella.jsp";
	}
	
}
