package com.example.SpringSoft3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.model.Progetto;
import com.example.SpringSoft3.model.Ruolo;
import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/insertController"})
public class Inserimento {

	@Autowired 
	private Services services; //attivo i servizi crud/dao iniettando l'oggetto derivante dall'interfaccia
	
	@RequestMapping(value="insertUtenti", method=RequestMethod.POST)
	public String inserimento(@RequestParam String nome, 
			@RequestParam String cognome, 
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam String email,
			@RequestParam String ruolo) { 
			
			Utente u = new Utente();
			u.setNome(nome);
			u.setCognome(cognome);
			
			Ruolo r = new Ruolo();
			r=services.getRu(ruolo);
			
			Account a = new Account();
			a.setRuolo(r);
			a.setUsername(username);
			a.setPassword(password);
			a.setEmail(email);
			
			
			a.setUtente(u); //inserisco utente in account, così se cancello l'uno, cancello anche l'altro
			u.setAccount(a); //inserisco utente in utente, così se cancello l'uno, cancello anche l'altro
			
			services.inserisciUtente(u);
			
			return "success.jsp";
		
	}
	
	@RequestMapping(value="insertProgetti", method=RequestMethod.POST)
	public String inserimentoProgetto(@RequestParam String progetto, 
			@RequestParam String descrizione) { 
			
			Progetto p = new Progetto();
			
			p.setProgetto(progetto);
			p.setDescrizione(descrizione);
			
			services.inserisciProgetto(p);
			
			return "home.jsp";
		
	}
	
	@RequestMapping(value="insertIntoProgetto", method=RequestMethod.POST)
	public String inserimentoUtenteIntoProgetto(
			@RequestParam String idprogetto, 
			@RequestParam String idutente) { 
			
			Progetto p = services.getPj(Integer.parseInt(idprogetto));
			Utente u = services.getUtente(Integer.parseInt(idutente));
			p.getUtenti().add(u);
			services.inserisciProgetto(p);
			
			return "home.jsp";
		
	}
	
	
}
