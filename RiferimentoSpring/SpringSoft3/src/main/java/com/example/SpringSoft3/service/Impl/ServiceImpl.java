package com.example.SpringSoft3.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.model.Progetto;
import com.example.SpringSoft3.model.Ruolo;
import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.repository.RepoAccount;
import com.example.SpringSoft3.repository.RepoProgetto;
import com.example.SpringSoft3.repository.RepoRuolo;
import com.example.SpringSoft3.repository.RepoUtente;
import com.example.SpringSoft3.service.interfaces.Services;

@Service //ATTENZIONE A NON DIMENTICARE QUESTA ANNOTATION!!!
public class ServiceImpl implements Services {

	@Autowired 
	private RepoUtente repoUtente;
	
	@Autowired
	private RepoRuolo repoRuolo;
	
	@Autowired
	private RepoAccount repoAccount;
	
	@Autowired
	private RepoProgetto repoProgetto;
	
	@Override
	public void inserisciUtente(Utente u) {
		repoUtente.save(u);
	}
	
	@Override
	public Ruolo getRu(String ruolo) {
		return repoRuolo.findByRuolo(ruolo);
	}
	
	@Override
	public List<Utente> getAllUtenti(){
		return repoUtente.findAll(); //findAll ritorna un arraylist di oggetti
	}
	
	@Override
	public List<Account> getAllAcc(){
		return repoAccount.findAll();
	}
	
	@Override
	public Account getAcc(int id) {
		return repoAccount.findById(id);
	}
	
	@Override
	public Utente getUtente(int id) {
		return repoUtente.findById(id);
	}
	
	@Override
	public void cancellaUtente(Utente u) {
		repoUtente.delete(u);
	}
	
	@Override
	public void inserisciProgetto(Progetto p) {
		repoProgetto.save(p);
	}
	
	@Override
	public List<Progetto> getAllPj() {
		return repoProgetto.findAll(); 
	}
	
	@Override
	public Progetto getPj(int id) {
		return repoProgetto.findById(id);
	}
	
}
