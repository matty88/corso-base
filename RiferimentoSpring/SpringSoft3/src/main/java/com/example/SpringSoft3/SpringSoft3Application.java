package com.example.SpringSoft3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSoft3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSoft3Application.class, args);
	}

}
