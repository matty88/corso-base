package com.example.SpringSoft3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.SpringSoft3.model.Account;
import com.example.SpringSoft3.model.Ruolo;
import com.example.SpringSoft3.model.Utente;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/updateController2"})
public class Modifica2 {
	
		@Autowired 
		private Services services; //attivo i servizi crud/dao iniettando l'oggetto derivante dall'interfaccia
		
		@RequestMapping(value="modificaUtenti2", method=RequestMethod.POST)
		public String inserimento(
				@RequestParam String id,
				@RequestParam String nome, 
				@RequestParam String cognome, 
				@RequestParam String username,
				@RequestParam String password,
				@RequestParam String email,
				@RequestParam String ruolo,
				ModelMap model) { //equivalente dell'oggetto RequestDispatcher
				
				Utente u = new Utente();
				u.setId(Integer.parseInt(id)); //specificando l'id sovrascrivo la riga
				u.setNome(nome);
				u.setCognome(cognome);
				
				Ruolo r = new Ruolo();
				r=services.getRu(ruolo);
				
				Account a = new Account();
				a.setId(Integer.parseInt(id));
				a.setRuolo(r);
				a.setUsername(username);
				a.setPassword(password);
				a.setEmail(email);
				
				
				a.setUtente(u); //inserisco utente in account, così se cancello l'uno, cancello anche l'altro
				u.setAccount(a); //inserisco utente in utente, così se cancello l'uno, cancello anche l'altro
				
				services.inserisciUtente(u);
				
				return "home.jsp";
			
		}

}
