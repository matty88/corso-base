package com.example.SpringSoft3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.SpringSoft3.model.Progetto;

public interface RepoProgetto extends JpaRepository<Progetto, Integer> {

	Progetto findById(int id);
	
}
