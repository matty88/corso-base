package com.example.SpringSoft3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.SpringSoft3.model.Progetto;
import com.example.SpringSoft3.service.interfaces.Services;

@Controller
@RequestMapping({"/viewController2"})
public class VisualizzaPJ {

	@Autowired
	private Services service;
	
	@RequestMapping(name="viewProgetti")
	public String visualizzaProgetti(ModelMap model) { //model è l'equivalente di HttpServletRequest
		List<Progetto> listaProgetti = service.getAllPj();
		model.addAttribute("listaProgetti", listaProgetti);
		
		return "tabellaprogetti.jsp";
	}
	
	
	
}
