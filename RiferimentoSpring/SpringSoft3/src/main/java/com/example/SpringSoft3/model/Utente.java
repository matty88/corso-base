package com.example.SpringSoft3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="utenti")
public class Utente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	@Column
	private String nome;
	
	public void setNome(String nome) {
		this.nome=nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	@Column
	private String cognome;
	
	public void setCognome(String cognome) {
		this.cognome=cognome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	@OneToOne(fetch=FetchType.EAGER, mappedBy="utente", cascade=CascadeType.ALL) //mappedBy significa che collego l'oggetto account a quello della tabella che usa la pk nel legame
	private Account account;
	
	public void setAccount(Account account) {
		this.account=account;
	}
	
	public Account getAccount() {
		return account;
	}
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name="tabella_utenteProgetto", 
	joinColumns=@JoinColumn(name="id_utente"),
	inverseJoinColumns=@JoinColumn(name="id_progetto"))
	private List<Progetto> listaProgetti = new ArrayList<Progetto>();
	
	public void setProgetti(List<Progetto> listaProgetti) {
		this.listaProgetti=listaProgetti;
	}
	
	public List<Progetto> getProgetti(){
		return listaProgetti;
	}
	
	@Override
	public String toString() {
		return nome+" "+cognome;
	}
	
	
	
}
