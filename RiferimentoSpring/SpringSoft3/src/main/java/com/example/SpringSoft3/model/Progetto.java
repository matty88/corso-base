package com.example.SpringSoft3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="progetti")
public class Progetto {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private int id;
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	@Column(name="progetto",nullable=false, unique=true)
	private String progetto;
	
	public void setProgetto(String progetto) {
		this.progetto=progetto;
	}
	
	public String getProgetto() {
		return progetto;
	}
	
	@Column(name="descrizione",nullable=false)
	private String descrizione;
	
	public void setDescrizione(String descrizione) {
		this.descrizione=descrizione;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name="tabella_utenteProgetto", //creo sia qui che in Utente la tabella normalizzata
	joinColumns=@JoinColumn(name="id_progetto"),
	inverseJoinColumns=@JoinColumn(name="id_utente"))
	private List<Utente> listaUtenti = new ArrayList<Utente>();
	
	public void setUtenti(List<Utente> listaUtenti) {
		this.listaUtenti=listaUtenti;
	}
	
	public List<Utente> getUtenti(){
		return listaUtenti;
	}
	
	
	
	
	
}
