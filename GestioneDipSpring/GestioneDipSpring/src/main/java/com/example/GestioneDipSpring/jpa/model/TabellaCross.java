package com.example.GestioneDipSpring.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabella_dipendente_progetto")

public class TabellaCross {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private int id_progetto;
	
	@Column(nullable=false)
	private int id_dipendente;
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setIdProgetto(int id_progetto) {
		this.id_progetto=id_progetto;
	}
	
	public int getIdProgetto() {
		return id_progetto;
	}
	
	public void setIdDipendente(int id_dipendente) {
		this.id_dipendente=id_dipendente;
	}
	
	public int getIdDipendente() {
		return id_dipendente;
	}
	
	
	public TabellaCross(int id, int id_progetto, int id_dipendente) {
		super();
		this.id = id;
		this.id_progetto = id_progetto;
		this.id_dipendente = id_dipendente;
	}
	
	public TabellaCross() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + id_dipendente;
		result = prime * result + id_progetto;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabellaCross other = (TabellaCross) obj;
		if (id != other.id)
			return false;
		if (id_dipendente != other.id_dipendente)
			return false;
		if (id_progetto != other.id_progetto)
			return false;
		return true;
	}
	
}
