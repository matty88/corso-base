package com.example.GestioneDipSpring.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.GestioneDipSpring.jpa.model.Account;

public interface RepoAccInterface extends JpaRepository<Account, Integer> {
	
	Account findById(int id);
	
	//@Query("select count (id) from Account where ruolo.id=?1")
	@Query("select count (*) from Account a where a.ruolo.id = :ruolo")
	Long countByRuolo (@Param("ruolo")int ruolo);
	
}
 