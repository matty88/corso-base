package com.example.GestioneDipSpring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.GestioneDipSpring.jpa.model.Account;
import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.jpa.model.TabellaCross;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;

@Controller
@RequestMapping({"/InserimentoController"})

public class InserimentoController {

	@Autowired
	private ServiceDipInterface service;

	@RequestMapping(value="inserimento", method=RequestMethod.GET)
	public String inserimentoGet(ModelMap model) {
		return "inserimento.jsp";
	}

	//VALUE ASSOCIATO A QUESTO SERVIZIO
	@RequestMapping(value="inserimento", method=RequestMethod.POST)    //GESTISCO RICHIESTE DI TIPO POST.PERMETTE QUANDO INVOCHIAMO IL SERVIZIO DI FAR PARTIRE QUESTO METODO 
	//RICHIEDO PARAMETRI ED IL LORO TIPO
	public String inserimentoPost(
			@RequestParam String nome, 
			@RequestParam String cognome, 
			@RequestParam String eta, 
			@RequestParam("luogo_di_nascita") String luogoDiNascita, 
			@RequestParam String sesso, 
			@RequestParam("codice_fiscale") String codiceFiscale,
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam String email, 
			@RequestParam String ruolo, ModelMap model) {    //MODELMAP E' UN OGGETTO CHE GESTISCE LA VISTA OVVERO IL RIFERIMENTO AD UNA CLASSE

		Dipendente d=new Dipendente();
		d.setNome(nome);
		d.setCognome(cognome);
		d.setEta(Integer.parseInt(eta));
		d.setLuogoDiNascita(luogoDiNascita);
		d.setSesso(sesso);
		d.setCodiceFiscale(codiceFiscale);

		Account a = new Account();
		a.setEmail(email);
		a.setUsername(username);
		a.setPassword(password);	
		a.setRuolo(service.getRuolo(ruolo));

		//INSERISCO L'UNO NELL'ALTRO COSI' SE CANCELLO L'UNO CANCELLO ANCHE L'ALTRO
		a.setDipendente(d); 
		d.setAccount(a); 

		//DOBBIAMO COMUNICARE COL SERVICE ED INIETTARE LA DIPENDENZA
		service.inserisci(d);

		//GESTIAMO I MODEL SETTANDO GLI ATTRIBUTI
		model.addAttribute("messaggio", "Inserimento avvenuto con successo!");  //BISOGNA SETTARLO CON BOOLEANO PER GESTIRE VALORE DI RITORNO
		model.addAttribute("nome",d.getNome());
		model.addAttribute("username",a.getUsername());
		model.addAttribute("ruolo",a.getRuolo().getRuolo());

		//DISPATCHER AD UNA PAGINA CHE DEVO CREARE
		return "ins.jsp";	
	}
	
	@RequestMapping(value="inserisciDipendenteProgetto/{id}", method=RequestMethod.GET)
	public String inserisciDipendenteProgetto(@PathVariable(value = "id") String id, ModelMap model) {
		Dipendente d = service.getDipendente(Integer.parseInt(id));
		List<Progetto> pList = service.allpList();
		model.addAttribute("d", d);
		model.addAttribute("listaProgetti", pList);
		return "inserisci_dip_progetto.jsp";
	}

	@RequestMapping(value="inserisciDipendenteProgetto/inserimentoInProgetto", method=RequestMethod.POST) 
	public String inserimentoDipProgetto(@RequestParam String idPro, @RequestParam String idDip) {
		Progetto p = service.getProgetto(Integer.parseInt(idPro));
		Dipendente d = service.getDipendente(Integer.parseInt(idDip));
		p.getdList().add(d);
		service.inserisciProgetto(p);
		return "index.jsp";
	}

	@RequestMapping(value="inserimentoProgetto", method=RequestMethod.GET)
	public String inserimentoProgetto(ModelMap model) {
		return "inserimento_progetto.jsp";
	}

	@RequestMapping(value="inserimentoProgetto", method=RequestMethod.POST)
	public String inserimentoProgettoPost(@RequestParam String progetto, @RequestParam String desc) {
		Progetto p = new Progetto();
		p.setProgetto(progetto);
		p.setDescrizione(desc);
		service.inserisciProgetto(p);
		return "index.jsp";
	}	
	
	@RequestMapping(value="assegnaDip", method=RequestMethod.GET)
	public String assegnaDip(@RequestParam String idPro, @RequestParam String idDip) {
		TabellaCross tc=new TabellaCross();
		tc.setIdProgetto(Integer.parseInt(idPro));
		tc.setIdDipendente(Integer.parseInt(idDip));
		service.insertTab(tc);
		return "index.jsp";
	}
}
