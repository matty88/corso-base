package com.example.GestioneDipSpring.jpa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

@Entity     //DEFINISCE LA CLASSE COME UN ENTITA'(POJO) PER IL DATABASE. LA CONSEGNAMO AD HIBERNATE PER POTER GESTIRE ED ESEGUIRE LA PERSISTENZA
@Table(name="dipendenti")  //ESEGUE IL MAPPING VERSO LA TABELLA DEL DATABASE
public class Dipendente {
	
	//FACCIAMO MAPPING DELL'ENTITA' VERSO LA TABELLA
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)       
	private int id;	
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String cognome;
	
	@Column(nullable=false)
	private int eta;
	
	@Column(name="luogo_di_nascita", nullable=false)  //NAME SI UTILIZZA SOLO SE IL NOME DELLA COLONNA E' DIFFERENTE(IN QUESTO CASO POTREBBE ANCHE NON ESSERE UTILIZZATO)
	private String luogoDiNascita;
	
	@Column(nullable=false)
	private String sesso;
	
	@Column(name="codice_fiscale", nullable=false, unique=true, columnDefinition="CHAR(16)")
	private String codiceFiscale;
	
	@OneToOne(fetch=FetchType.EAGER, mappedBy="dipendente", cascade=CascadeType.ALL) //mappedBy SI AGGANCIA ALL'ATTRIBUTO DIPENDENTE DI ACCOUNT DOVE HO INSERITO LA ANNOTATION ALLA CHIAVE ESTERNA @OneToONE, significa che collego l'oggetto account a quello della tabella che usa la pk nel legame
	private Account account;
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)  //FA LA STESSA COSA DI PROGETTO MA A CHIAVE INVERTITA
	@JoinTable(name="tabella_dipendente_progetto",
	joinColumns=@JoinColumn(name="id_dipendente"),
	inverseJoinColumns=@JoinColumn(name="id_progetto"))
	private List<Progetto> pList = new ArrayList<Progetto>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public void setEta(int eta) {
		this.eta=eta;
	}	
	public int getEta() {
		return eta;
	}
	public void setLuogoDiNascita(String luogoDiNascita) {
		this.luogoDiNascita=luogoDiNascita;
	}
	public String getLuogoDiNascita() {
		return luogoDiNascita;
	}
	public void setSesso(String sesso) {
		this.sesso=sesso;
	}
	public String getSesso() {
		return sesso;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale=codiceFiscale.toUpperCase();
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public void setAccount(Account account) {
		this.account=account;
	}	
	public Account getAccount() {
		return account;
	}
	
	public void setProgetti(List<Progetto> pList) {
		this.pList=pList;
	}
	
	public List<Progetto> getProgetti() {
		return pList;
	}
	
	@Override
	public String toString() {
		return id+ ", " +nome+ ", " +cognome+", "+eta+" ,"+luogoDiNascita+", "+sesso+" ,"+codiceFiscale+" ,"+account;
	}
	public Dipendente(int id, String nome, String cognome, int eta, String luogoDiNascita, String sesso, String codiceFiscale, Account account) {
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.eta= eta;
		this.luogoDiNascita=luogoDiNascita;
		this.sesso=sesso;
		this.codiceFiscale=codiceFiscale;
		this.account=account;
	}
	public Dipendente() {
		// TODO Auto-generated constructor stub
	}
}
