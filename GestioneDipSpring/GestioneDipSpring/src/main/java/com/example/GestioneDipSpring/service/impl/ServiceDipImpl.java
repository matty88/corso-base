package com.example.GestioneDipSpring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.GestioneDipSpring.jpa.model.Account;
import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.jpa.model.Ruolo;
import com.example.GestioneDipSpring.jpa.model.TabellaCross;
import com.example.GestioneDipSpring.jpa.repository.RepoAccInterface;
import com.example.GestioneDipSpring.jpa.repository.RepoDipInterface;
import com.example.GestioneDipSpring.jpa.repository.RepoProgettoInterface;
import com.example.GestioneDipSpring.jpa.repository.RepoRuoloInterface;
import com.example.GestioneDipSpring.jpa.repository.RepoTabInterface;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;

//PER ESEGUIRE I METODI DELL'INTERFACE ABBIAMO BISOGNO DI UNA CLASSE CHE RAPPRESENTA IL COMPONENTE SERVICE
//LEGATO A REPOSITORY CHE ESEGUE OPERAZIONI DAO
@Service

public class ServiceDipImpl implements ServiceDipInterface{
	
	//INIETTO E UTILIZZO LA DIPENDENZA DELL'OGGETTO(IN QUESTO CASO REPODIPINTERFACE)
	//SEMPRE PRIVATO PERCHE' DEV'ESSERE VISIBILE SOLO ALL'INTERNO DI QUESTA CLASSE
	//IN QUESTO CASO I METODI COMPOSTI COME FINDBYID
	@Autowired
	private RepoDipInterface rdi;
	
	@Autowired
	private RepoAccInterface rai;
	
	@Autowired
	private RepoRuoloInterface rri;
	
	@Autowired
	private RepoProgettoInterface rpi;
	
	@Autowired
	private RepoTabInterface rti;
	
	@Override
	public void inserisci(Dipendente d) {
		//PER UTILIZZARE UN METODO DEL REPOSITORY DEVO INIETTARE LA DIPENDENZA DI QUEL REPOSITORY
		rdi.save(d);
	}

	@Override
	public Dipendente getDipendente(int id) {
		return rdi.findById(id);
	}
	
	@Override
	public Dipendente getDipendente(String cf) {
		return rdi.findByCodiceFiscale(cf);
	}

	@Override
	public void cancella(Dipendente d) {
		rdi.delete(d);	
	}

	@Override
	public List<Dipendente> getDipendentiList() {
		return rdi.findAll();
	}

	@Override
	public Account getAccount(int id) {
		return rai.findById(id);
	}

	@Override
	public List<Account> getAccountList() {
		return rai.findAll();
	}

	@Override
	public Ruolo getRuolo(String r) {
		return rri.findByRuolo(r);
	}

	@Override
	public Ruolo getRuolo(int id) {
		return rri.findById(id);
	}

	@Override
	public Dipendente getDipendente(String nome, String cognome) {
		return rdi.findByNomeCognome(nome, cognome);
	}
	
	@Override
	public List<Dipendente> getDipendenteList(String nome, String cognome) {
		return rdi.findByNomeAndCognome(nome, cognome);
	}

	@Override
	public void inserisciProgetto(Progetto p) {
		rpi.save(p);	
	}

	@Override
	public List<Progetto> allpList() {
		return rpi.findAll();
	}

	@Override
	public Progetto getProgetto(int id) {
		return rpi.findById(id);
	}
	
	@Override
	public Progetto getProgetto(String progetto) {
		return rpi.findByProgetto(progetto);
	}
	
	@Override
	public void cancellaProgetto(Progetto progetto) {
		rpi.delete(progetto);
	}

	@Override
	public Long countRuolo(int ruolo) {
		return rai.countByRuolo(ruolo);
	}

	@Override
	public Page<Dipendente> findAll(Pageable pageable) {
		return rdi.findAll(pageable);
	}
	
	@Override
	public List<Dipendente> getDipAssegnati(int id_progetto) {
		return rti.getDipAssegnati(id_progetto);
	}

	@Override
	public List<Dipendente> getDipNonAssegnati(int id_progetto) {
		return rti.getDipNonAssegnati(id_progetto);
	}
	
	@Override
	public TabellaCross getTab(int id_dipendente, int id_progetto) {
		return rti.findByDipProg(id_dipendente,id_progetto);
	}
	
	@Override
	public void cancellaTab(TabellaCross tc) {
		rti.delete(tc);	
	}
	
	@Override
	public void insertTab(TabellaCross tc) {
		rti.save(tc);
	}

}
