package com.example.GestioneDipSpring.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
	
	@Entity
	@Table(name="accounts")
	public class Account {
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int id;
		
		@Column(nullable=false, unique=true)
		private String username;
		
		@Column(nullable=false)
		private String password;
		
		@Column(nullable=false, unique=true)
		private String email;
		
		@OneToOne(fetch=FetchType.EAGER)    //INDICO LE ISTANZE DELLE ENTITA' CHE VENGONO RECUPERATE IMMEDIATAMANETE
		@JoinColumn(name="id_dipendente",nullable=false) //@JoinColumn DEFINISCE LA CHIAVE ESTERNA CHE PUNTA ALL ID DEL DIPENDENTE (NELLA OneToOne NON OCCORRE SPECIFICARE LA PRIMARYKEY DI DIPENDENTE)
		private Dipendente dipendente;
		
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="id_ruolo", referencedColumnName="id")    //ID E' LA PRIMARY KEY NELLA TABELLA RUOLI
		private Ruolo ruolo;
		
		public void setId(int id) {
			this.id=id;
		}

		public int getId() {
			return id;
		}
			
		public void setUsername(String username) {
			this.username=username;
		}
		
		public String getUsername() {
			return username;
		}		
		
		public void setPassword(String password) {
			this.password=password;
		}
		
		public String getPassword() {
			return password;
		}
	
		public void setEmail(String email) {
			this.email=email;
		}
		
		public String getEmail() {
			return email;
		}
		
		public void setDipendente(Dipendente d) {
			this.dipendente=d;
		}
		
		public Dipendente getDipendente() {
			return dipendente;
		}
		
		public void setRuolo(Ruolo r) {
			this.ruolo=r;
		}
		
		public Ruolo getRuolo() {
			return ruolo;
		}
		
		public Account() {			
		}
		
		public Account(String username, String password, String email) {
			this.email=email;
			this.password=password;
			this.username=username;
		}
		
		public String toString() {
			return email+", "+username;
		}
}
