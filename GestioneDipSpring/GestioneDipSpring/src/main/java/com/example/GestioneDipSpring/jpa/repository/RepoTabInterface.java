package com.example.GestioneDipSpring.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.TabellaCross;

public interface RepoTabInterface extends JpaRepository<TabellaCross, Integer>{
	
	@Query("FROM Dipendente d where d.id IN (SELECT id_dipendente FROM TabellaCross WHERE id_progetto=?1)")
	List<Dipendente> getDipAssegnati(int id_progetto);
	
	@Query("FROM Dipendente d where d.id NOT IN (SELECT id_dipendente FROM TabellaCross WHERE id_progetto=?1)")
	List<Dipendente> getDipNonAssegnati(int id_progetto);

	@Query("FROM TabellaCross t where t.id_dipendente=?1 AND t.id_progetto=?2")
	TabellaCross findByDipProg(int id_dipendente, int id_progetto);
	
}
