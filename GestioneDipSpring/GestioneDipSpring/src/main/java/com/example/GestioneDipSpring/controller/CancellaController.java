package com.example.GestioneDipSpring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.jpa.model.TabellaCross;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;

@Controller
@RequestMapping({"/CancellaController"})

public class CancellaController {
	
	@Autowired
	private ServiceDipInterface service;
	
	@RequestMapping(value="cancella", method=RequestMethod.GET)
	public String cancellaDip(@RequestParam String id) {
		Dipendente d = service.getDipendente(Integer.parseInt(id));
		service.cancella(d);
		return "canc.jsp";
	}

	@RequestMapping(value="cancellaProgetto", method=RequestMethod.GET)
	public String cancellaProgetto(@RequestParam String progetto, ModelMap model) {
		Progetto p = service.getProgetto(progetto);
		service.cancellaProgetto(p);
		model.addAttribute("msg","Progetto cancellato "+progetto);
		return "index.jsp";
	}

	@RequestMapping(value="rimuoviDip", method=RequestMethod.GET)
	public String rimuoviDip(@RequestParam String idPro, @RequestParam String idDip) {
		TabellaCross tc = service.getTab(Integer.parseInt(idDip), Integer.parseInt(idPro));
		service.cancellaTab(tc);
		return "index.jsp";
	}

}
