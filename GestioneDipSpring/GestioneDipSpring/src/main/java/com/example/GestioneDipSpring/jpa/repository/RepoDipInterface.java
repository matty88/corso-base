package com.example.GestioneDipSpring.jpa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.GestioneDipSpring.jpa.model.Dipendente;

//CON JPA ESEGUIAMO ORM
//SPRING DATA CHE SFRUTTA HIBERNATE SI PONE TRA APPLICAZIONE E DB
public interface RepoDipInterface extends JpaRepository<Dipendente,Integer> {
	
	//AGGANCIAMO IL REPOSITORY ALLA SPECIFICA ENTITA' DIPENDENTE
	//TUTTE LE QUERY SARANNO SU QUELLA SPECIFICA ENTITA'
	
	//A DIFFERENZA DI SAVE E DELETE DEVO DICHIARARE QUESTO METODO PERCHE' E' COMPOSTO(CUSTOM)
	Dipendente findById(int id);
	
	Dipendente findByCodiceFiscale(String cf);
	
	@Query("FROM Dipendente d WHERE d.nome=?1 AND d.cognome=?2")
	Dipendente findByNomeCognome(String nome, String cognome);
	
	List<Dipendente> findByNomeAndCognome(String nome, String cognome);
	
	Page<Dipendente> findAll(Pageable pageable);
	
}
