package com.example.GestioneDipSpring.jpa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ruoli")
public class Ruolo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private String ruolo;
	
	//SE CANCELLO RUOLO ELIMINO TUTTI GLI ACCOUNT COLLEGATI
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ruolo", cascade=CascadeType.ALL)
	private List<Account> aList=new ArrayList<Account>();   //CON LAZY ALLOCO IN MEMORIA LA LISTA SOLO SE UTILIZZATA
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setRuolo(String r) {
		this.ruolo=r;
	}
	
	public String getRuolo() {
		return ruolo;
	}
	
	public void setAList(List<Account> aList) {
		this.aList=aList;
	}
	
	public List<Account> getAList() {
		return aList;
	}
	
	
}
