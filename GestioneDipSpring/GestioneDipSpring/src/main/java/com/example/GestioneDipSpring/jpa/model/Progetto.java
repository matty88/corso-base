package com.example.GestioneDipSpring.jpa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="progetti")
public class Progetto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false, unique=true)
	private String progetto;
	
	@Column(nullable=false)
	private String descrizione;
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)  //INDICA CHE LA RELAZIONE è MANYTOMANY
	@JoinTable(name="tabella_dipendente_progetto",              //SPECIFICA IL NOEM DELLA TABELLA D'APPOGGIO AUTOMATICAMENTE
	joinColumns= @JoinColumn(name="id_progetto"),              //SPECIFICA LA CHIAVE INTERNA DA UTILIZZARE 
	inverseJoinColumns=@JoinColumn(name="id_dipendente"))      //SPECIFICA LA CHIAVE ESTERNA
	private List<Dipendente> dList = new ArrayList<Dipendente>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProgetto() {
		return progetto;
	}

	public void setProgetto(String progetto) {
		this.progetto = progetto;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<Dipendente> getdList() {
		return dList;
	}

	public void setdList(List<Dipendente> dList) {
		this.dList = dList;
	}
		
}
