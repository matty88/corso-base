package com.example.GestioneDipSpring.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.GestioneDipSpring.jpa.model.Progetto;

public interface RepoProgettoInterface extends JpaRepository<Progetto,Integer>{
	
	Progetto findById(int id);

	Progetto findByProgetto(String progetto);

}
