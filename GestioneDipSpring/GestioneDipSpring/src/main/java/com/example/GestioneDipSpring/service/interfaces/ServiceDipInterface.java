package com.example.GestioneDipSpring.service.interfaces;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.GestioneDipSpring.jpa.model.Account;
import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.jpa.model.Ruolo;
import com.example.GestioneDipSpring.jpa.model.TabellaCross;

public interface ServiceDipInterface {
	
	//DICHIARIAMO IL METODO DI INSERIMENTO
	//CREIAMO QUESTA INTERFACCIA PER INIETTARNE LA DIPENDENZA ALL'INTERNO DI UN COMPONENTE
	public void inserisci(Dipendente d);
	
	//CERCO DIPENDENTE TRAMITE ID
	Dipendente getDipendente(int id);
	
	Dipendente getDipendente(String cf);
	
	Dipendente getDipendente(String nome, String cognome);
	
	List<Dipendente> getDipendenteList(String nome, String cognome);
	
	Account getAccount(int id);
	
	Ruolo getRuolo(String r);
	
	Ruolo getRuolo(int id);
	
	void cancella(Dipendente d);
	
	void inserisciProgetto(Progetto p);
	
	List<Progetto> allpList();
	
	Progetto getProgetto(int id);
	
	Progetto getProgetto(String progetto);
	
	void cancellaProgetto(Progetto progetto);
	
	//CERCO TUTTI I DIPENDENTI
	List<Dipendente> getDipendentiList();
	
	List<Account> getAccountList();
	
	Long countRuolo(int ruolo);
	
	Page<Dipendente> findAll(Pageable pageable);
	
	List<Dipendente> getDipAssegnati(int id_progetto);

	List<Dipendente> getDipNonAssegnati(int id_progetto);
	
	TabellaCross getTab(int id_dipendente,int id_progetto);
	
	void cancellaTab(TabellaCross tc);
	
	void insertTab(TabellaCross tc);
	
}
