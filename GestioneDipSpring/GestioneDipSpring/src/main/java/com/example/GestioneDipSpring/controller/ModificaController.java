package com.example.GestioneDipSpring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.GestioneDipSpring.jpa.model.Account;
import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;

@Controller
@RequestMapping({"/ModificaController"})

public class ModificaController {

	@Autowired
	private ServiceDipInterface service;
	
	@RequestMapping(value="modifica", method=RequestMethod.GET)
	public String insModifica(@RequestParam String id, ModelMap model) {
		model.addAttribute("d",service.getDipendente(Integer.parseInt(id)));
		return "modifica.jsp";
	}

	@RequestMapping(value="modifica", method=RequestMethod.POST)
	public String modificaDip(
			@RequestParam String id,
			@RequestParam String nome,
			@RequestParam String cognome,
			@RequestParam String eta,
			@RequestParam("luogo_di_nascita") String luogoDiNascita,
			@RequestParam String sesso,
			@RequestParam("codice_fiscale") String codiceFiscale,
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam String email,
			@RequestParam String ruolo,
			@RequestParam("id_account") String idAccount, ModelMap model) {

		Dipendente d= new Dipendente();
		d.setId(Integer.parseInt(id));
		d.setNome(nome);
		d.setCognome(cognome);
		d.setEta(Integer.parseInt(eta));
		d.setLuogoDiNascita(luogoDiNascita);
		d.setSesso(sesso);
		d.setCodiceFiscale(codiceFiscale);

		Account a = new Account();
		a.setId(Integer.parseInt(idAccount));
		a.setEmail(email);
		a.setUsername(username);
		a.setPassword(password);
		a.setRuolo(service.getRuolo(ruolo));

		a.setDipendente(d); 
		d.setAccount(a); 

		service.inserisci(d);
		model.addAttribute("messaggio", "Inserimento avvenuto con successo!");
		model.addAttribute("nome", d.getNome());
		model.addAttribute("username", a.getUsername());
		model.addAttribute("ruolo",a.getRuolo().getRuolo());
		return "ins.jsp";
	}
	
	@RequestMapping(value="modificaProgetto", method=RequestMethod.GET)
	public String modificaProgetto(@RequestParam String idPro, ModelMap model) {
		model.addAttribute("progetto",service.getProgetto(Integer.parseInt(idPro)));
		return "modifica_progetto.jsp";
	}

	@RequestMapping(value="modificaProgetto", method=RequestMethod.POST)
	public String modificaProgettoPost(@RequestParam String idPro, String nomePro, @RequestParam String descPro, ModelMap model) {
		Progetto p= new Progetto();
		p.setId(Integer.parseInt(idPro));
		p.setProgetto(nomePro);
		p.setDescrizione(descPro);
		service.inserisciProgetto(p);
		model.addAttribute("msg", "Progetto Modificato con successo");
		return "index.jsp";
	}

	
}
