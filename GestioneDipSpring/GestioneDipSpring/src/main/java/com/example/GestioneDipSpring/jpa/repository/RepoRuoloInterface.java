package com.example.GestioneDipSpring.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.GestioneDipSpring.jpa.model.Ruolo;

public interface RepoRuoloInterface extends JpaRepository<Ruolo,Integer>{
	
	Ruolo findByRuolo(String ruolo);
	
	Ruolo findById(int id);
	
	
}
