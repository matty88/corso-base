package com.example.GestioneDipSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//START DEL PROGETTO

@SpringBootApplication
public class GestioneDipSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneDipSpringApplication.class, args);
	}

}
