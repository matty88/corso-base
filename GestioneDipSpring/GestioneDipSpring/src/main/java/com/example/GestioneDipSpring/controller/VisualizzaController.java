package com.example.GestioneDipSpring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.jpa.model.Progetto;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;


@Controller
@RequestMapping({"/VisualizzaController"})    //DEFINISCE IL NOME DEL COMPONENTE

public class VisualizzaController {

	//INIETTO E UTILIZZO LA DIPENDENZA DELL'OGGETTO
	//SEMPRE PRIVATO PERCHE' DEV'ESSERE VISIBILE SOLO ALL'INTERNO DI QUESTA CLASSE
	@Autowired   //new ServiceDipInterface
	private ServiceDipInterface service;

	@RequestMapping(method=RequestMethod.GET)
	public String visualizzaDipIndex(ModelMap model) {
		List<Dipendente>dList = service.getDipendentiList();
		model.addAttribute("listaDip", dList);
		return "visualizza.jsp";
	}

	@RequestMapping(value="visualizza/{id}", method=RequestMethod.GET)
	public String visualizzaDipDettaglio(@PathVariable(value = "id") String id, ModelMap model) {
		model.addAttribute("d",service.getDipendente(Integer.parseInt(id)));
		return "visualizza_dettaglio.jsp";
	}

	@RequestMapping(value="visualizzaProgetti")
	public String visualizzaProgetti(ModelMap model) {
		List<Progetto> pList = service.allpList();
		//pList=pList.stream().filter(Objects::nonNull).collect(Collectors.toList());  //PRENDE LA LISTA APRE LO STREAM FILTRA TUTTI GLI OGGETTI DIVERSI DA NULL E LI COLLEZIONO
		model.addAttribute("listaProgetti", pList);
		return "visualizza_progetti.jsp";
	}
	
	@RequestMapping(value="visualizzaRuoli", method=RequestMethod.GET)
	public String visualizzaRuoli(ModelMap model) {
		model.addAttribute("admins",service.countRuolo(1));
		model.addAttribute("guests",service.countRuolo(2));
		return "visualizza_ruoli.jsp";
	}

	@RequestMapping(value="visualizzaDipAssegnati", method=RequestMethod.GET)
	public String dipAssegnati(@RequestParam String idPro, ModelMap model) {
		model.addAttribute("idPro", idPro);
		model.addAttribute("listaAssegnati", service.getDipAssegnati(Integer.parseInt(idPro)));
		model.addAttribute("listaNonAssegnati", service.getDipNonAssegnati(Integer.parseInt(idPro)));
		return "visualizza_dip_assegnati.jsp";
	}

	@RequestMapping(value="visualizza", method=RequestMethod.GET)
	public String visualizzaDipPag(
			@RequestParam(defaultValue = "0") int page,         //PAGINA CORRENTE
			@RequestParam(defaultValue = "10") int size,         //LUNGHEZZA PAGINA
			@RequestParam(defaultValue = "id") String sort,	    //SORT INDICA COLONNA DEL DB DALLA QUALE ORDINARE
			@RequestParam(defaultValue = "asc") String order,   //ORDER PUO' ASSUMERE VALORE ASC E DESC DELL'ORDER BY DI SQL
			ModelMap model) {  
		//		if("asc".equals(order)) {
		//			sorting=Sort.by(sort).ascending();
		//		} else {
		//			sorting=Sort.by(sort).descending();
		//		}
		Pageable paging = PageRequest.of(page, size, Sort.by(new Order(getSortDirection(order),sort)));           //ISTANZIO OGGETTO DI TIPO PAGEABLE A CUI DO DUE QUERY PARAM E UN OGGETTO SORT CON DENTRO CRITERI DI ORDINAMENTO		
		Page<Dipendente>dList = service.findAll(paging);        //USO JPA PER LEGGERE LE PAGINE CORRISPONDENTI USANDO LA PAGINAZIONE DELL'OGGETTO PAGEABLE
		model.addAttribute("paginaCorrente", page);
		model.addAttribute("maxPage", dList.getTotalPages());
		model.addAttribute("maxSize", dList.getTotalElements());
		model.addAttribute("sort", sort);
		model.addAttribute("order", order);
		model.addAttribute("size", size);
		model.addAttribute("listaDip", dList.getContent());
		model.addAttribute("admins",service.countRuolo(1));
		model.addAttribute("guests",service.countRuolo(2));
		return "visualizza.jsp";
	}

	private Sort.Direction getSortDirection(String order) {
		return "asc".equalsIgnoreCase(order) ? Sort.Direction.ASC: Sort.Direction.DESC;
	}
}
