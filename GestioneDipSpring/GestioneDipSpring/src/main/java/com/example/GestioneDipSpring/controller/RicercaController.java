package com.example.GestioneDipSpring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.GestioneDipSpring.jpa.model.Dipendente;
import com.example.GestioneDipSpring.service.interfaces.ServiceDipInterface;

@Controller
@RequestMapping({"/RicercaController"})

public class RicercaController {

	@Autowired
	private ServiceDipInterface service;
	
	@RequestMapping(value="ricercaCF", method=RequestMethod.GET)
	public String cercaDipCf(@RequestParam String cf, ModelMap model) {
		Dipendente d = service.getDipendente(cf);
		model.addAttribute("d", d);
		return "visualizza_cf.jsp";
	}

	@RequestMapping(value="ricercaNC", method=RequestMethod.GET)
	public String cercaDipNC(
			@RequestParam String nome, 
			@RequestParam String cognome, ModelMap model) {
		List<Dipendente> dList=service.getDipendenteList(nome, cognome);
		model.addAttribute("listaDip",dList);
		return "visualizza_nome_cognome.jsp";
	}
}
