<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dettaglio dipendente</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
</head>
<body>
	<nav class="navbar navbar-expand nav-c">
		<a class="navbar-brand" href="#"><i class="fas fa-binoculars"
			style="margin-right: 10px"></i>ELENCO DIPENDENTI</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="../index.jsp">HOME</a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#"
					id="navbarDropdownMenuLink" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false"> DIPENDENTI </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item"
							href="../InserimentoController/inserimento">AGGIUNGI
							DIPENDENTI</a> <a class="dropdown-item btn btn-primary"
							data-toggle="modal" data-target="#exampleModal"
							href="../VisualizzaController/visualizzaRuoli">VISUALIZZA
							RUOLI</a>
					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#"
					id="navbarDropdownMenuLink" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false"> PROGETTI </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item"
							href="../VisualizzaController/visualizzaProgetti">VISUALIZZA
							PROGETTI</a> <a class="dropdown-item"
							href="../InserimentoController/inserimentoProgetto">AGGIUNGI
							PROGETTO</a>
					</div>
				<li class="nav-item"><a class="nav-link" href="../../ricerca.jsp">RICERCA</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="wrapper">
		<input type="hidden" name="id" value="${d.id}"> <input
			type="hidden" name="id_account" value="${d.account.id}">
		<ul class="list-group user-detail">
			<li class="list-group-item active">DETTAGLIO DIPENDENTE</li>
			<li class="list-group-item">EMAIL - <span style="color:red">${d.account.email}</span></li>
			<li class="list-group-item">USERNAME - <span style="color:red">${d.account.username}</span></li>
			<li class="list-group-item">NOME - <span style="color:red">${d.nome}</span></li>
			<li class="list-group-item">COGNOME - <span style="color:red">${d.cognome}</span></li>
			<li class="list-group-item">ET� - <span style="color:red">${d.eta}</span></li>
			<li class="list-group-item">LUOGO DI NASCITA - <span style="color:red">${d.luogoDiNascita}</span></li>
			<li class="list-group-item">SESSO - <span style="color:red">${d.sesso}</span></li>
			<li class="list-group-item">CODICE FISCALE - <span style="color:red">${d.codiceFiscale}</span></li>
			<li class="list-group-item">RUOLO - <span style="color:red">${d.account.ruolo.ruolo}</span></li>
		</ul>
		<button type="button" style="margin-top:10px" title="Torna al menu principale" class="btn btn-dark"
				onclick="location.href='../index.jsp'">
				<i class="fa fa-undo-alt" aria-hidden="true"></i></button>
	</div>


	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-5.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-brand, .nav-c a {
	font-size: 13px;
	padding-top: 1px !important;
}

.navbar-nav {
margin-top:3px;
}

.wrapper {
	width: 25%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 12px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span, h5, .btn, .user-detail {
	font-size: 12px;
}

.navigation {
	float: right;
}

.border {
	border-style: none none solid none !important;
	color: #fff;
}

#sizePagina, .current-page {
	border-radius: .25rem
}

.current-page {
	width: 30px;
	text-align: center;
	background-color: #fff;
}

.nav-c a {
	color: #2c2c2c;
}
</style>
</body>
</html>