<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Aggiungi dipendente a progetto</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="wrapper">
		<form action="inserimentoInProgetto" method="post">
			<input type="hidden" name="idDip" value="${d.id}">
			<div class="input-group">
				<select class="custom-select" name="idPro">
					<c:forEach items="${listaProgetti}" var="p">
						<option value="${p.id}">${p.progetto}</option>
					</c:forEach>
				</select>
				<div class="input-group-append">
					<button class="btn btn-primary" type="submit">Invia</button>
				</div>
			</div>
		</form>
	</div>
	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-5.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.wrapper {
	width: 50%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 12px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span, h5, .btn {
	font-size: 12px;
}

.navigation {
	float: right;
}

span {
	color: #fff;
	margin-right: 15px;
	text-transform: uppercase;
}

.border {
	border-style: none none solid none !important;
	color: #fff;
}

#sizePagina, .current-page {
	border-radius: .25rem
}

.current-page {
	width: 30px;
	text-align: center;
	background-color: #fff;
}

.custom-select {
font-size:13px;
}


</style>

<script>
setTimeout('window.close()',15000);
</script>
</body>
</html>