<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifica Dipendente</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
</head>
<nav class="navbar navbar-expand nav-c">
	<a class="navbar-brand" href="#"><i class="fas fa-project-diagram"
		style="margin-right: 10px"></i>AGGIUNGI PROGETTO</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNavDropdown">
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="../index.jsp">HOME</a>
			</li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#"
				id="navbarDropdownMenuLink" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"> DIPENDENTI </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item"
						href="../InserimentoController/inserimento">AGGIUNGI
						DIPENDENTI</a> <a class="dropdown-item btn btn-primary"
						data-toggle="modal" data-target="#exampleModal"
						href="../VisualizzaController/visualizzaRuoli">VISUALIZZA
						RUOLI</a>
				</div></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#"
				id="navbarDropdownMenuLink" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"> PROGETTI </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item"
						href="../VisualizzaController/visualizzaProgetti">VISUALIZZA
						PROGETTI</a> <a class="dropdown-item"
						href="../InserimentoController/inserimentoProgetto">AGGIUNGI
						PROGETTO</a>
				</div>
			<li class="nav-item"><a class="nav-link" href="../ricerca.jsp">RICERCA</a>
			</li>
		</ul>
	</div>
</nav>
<body>
	<div class="wrapper">
		<form action="modifica" method="post">
			<input type="hidden" name="id" value="${d.id}"> <input
				type="hidden" name="id_account" value="${d.account.id}"><br>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="username">Username</label> <input type="text"
						class="form-control" name="username" id="username"
						value="${d.account.username}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="codice_fiscale">Codice fiscale</label> <input
						type="text" name="codice_fiscale" class="form-control"
						id="codice_fiscale" value="${d.codiceFiscale}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="email">E-mail</label> <input type="email"
						class="form-control" name="email" id="email"
						value="${d.account.email}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="password">Password</label> <input type="password"
						class="form-control" name="password" id="password"
						value="${d.account.password}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="nome">Nome</label> <input type="text"
						class="form-control" name="nome" id="nome" value="${d.nome}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="cognome">Cognome</label> <input type="text"
						class="form-control" name="cognome" id="cognome"
						value="${d.cognome}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="luogo_di_nascita">Luogo di nascita</label> <input
						type="text" name="luogo_di_nascita" class="form-control"
						id="luogo_di_nascita" value="${d.luogoDiNascita}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="eta">Et�</label> <input type="text" name="eta"
						class="form-control" id="eta" value="${d.eta}" />
				</div>
			</div>

			<div class="form-row">
				<c:choose>
					<c:when test="${d.sesso=='Maschio'}">
						<div class="col-md-6 mb-3">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="maschio"
									name="sesso" value="Maschio" checked /> <label
									class="form-check-label" for="maschio">Maschio</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="femmina"
									name="sesso" value="Femmina"> <label
									class="form-check-label" for="femmina">Femmina</label>
							</div>
						</div>
					</c:when>
					<c:when test="${d.sesso=='Femmina'}">
						<div class="col-md-6 mb-3">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="maschio"
									name="sesso" value="Maschio" /> <label class="form-check-label"
									for="maschio">Maschio</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="femmina"
									name="sesso" value="Femmina" checked> <label
									class="form-check-label" for="femmina">Femmina</label>
							</div>
						</div>
					</c:when>
				</c:choose>
				<c:choose>
					<c:when test="${d.account.ruolo.ruolo=='Admin'}">
						<div class="col-md-6 mb-3">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="admin"
									name="ruolo" value="Admin" checked /> <label
									class="form-check-label" for="admin">Admin</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="guest"
									name="ruolo" value="Guest"> <label
									class="form-check-label" for="guest">Guest</label>
							</div>
						</div>
					</c:when>
					<c:when test="${d.account.ruolo.ruolo=='Guest'}">
						<div class="col-md-6 mb-3">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="admin"
									name="ruolo" value="Admin" /> <label class="form-check-label"
									for="admin">Admin</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="guest"
									name="ruolo" value="Guest" checked> <label
									class="form-check-label" for="guest">Guest</label>
							</div>
						</div>
					</c:when>
				</c:choose>
			</div>
			<div class="form-group">
				<div class="form-check">
					<input class="form-check-input" type="checkbox"
						name="autorizzazione" id="autorizzazione" checked> <label
						id="auth" class="form-check-label" for="autorizzazione">Autorizzo
						il trattamento dei dati</label>
				</div>
			</div>
			<div class="actions">
				<button class="btn btn-primary" type="submit">INVIA</button>
				<button class="btn btn-danger" type="reset">RESET</button>
				<button type="button" title="Torna al menu principale" class="btn btn-dark"
				onclick="location.href='../index.jsp'">
				<i class="fa fa-undo-alt" aria-hidden="true"></i></button>
			</div>
		</form>
	</div>

	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-5.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-nav {
	margin-top: 3px;
}

.navbar-brand, .nav-c a {
	font-size: 13px;
	padding-top: 1px !important;
}

.wrapper {
	width: 50%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 12px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span, h5, .btn, .form-control {
	font-size: 12px;
}

.navigation {
	float: right;
}

span {
	color: #fff;
	margin-right: 15px;
	text-transform: uppercase;
}

.border {
	border-style: none none solid none !important;
	color: #fff;
}

#sizePagina, .current-page {
	border-radius: .25rem
}

.current-page {
	width: 30px;
	text-align: center;
	background-color: #fff;
}

.nav-c a {
	color: #2c2c2c;
}

label {
	text-transform: uppercase;
	font-size: 12px;
	color: #2c2c2c;
}
</style>
</body>
</html>