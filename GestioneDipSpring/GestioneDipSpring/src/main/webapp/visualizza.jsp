<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<script>
function iframe(id){
	mywindow = window.open("../InserimentoController/inserisciDipendenteProgetto/"+id, "mywindow", "location=0,status=0,scrollbars=0,width=450,height=250,resizable=0");
	mywindow.moveTo(150, 150);
}
</script>
<script>
function cancella(id) {
	if(confirm("Confermi la cancellazione?")) {
		location.href="../CancellaController/cancella?id="+id;
		return true;
	}
	return false;
}
</script>
<script>
	function ordina(newSort) {
		var oldSort='${sort}';
		var order='${order}';
		/*IF NUOVO SORT = VECCHIO SORT - CAMBIA ORDER*/
		if(oldSort==newSort && order=='asc'){
			order='desc';
		} else if(oldSort==newSort && order=='desc') {
			order='asc';
		}
		location.href="visualizza?page="+'${page}'+"&'size="+'${size}'+"&sort="+newSort+"&order="+order;	
	}
</script>
<meta charset="ISO-8859-1">
<title>Elenco Dipendenti</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script>
$(function(){
$('#sizePagina').on('change', function () {
var size = $(this).val(); 
if (size) { 
window.location = "visualizza?page=0&sort="+'${sort}'+"&order="+'${order}'+"&size="+size; 
}
return false;
});
});
</script>
</head>
<body>
	<nav class="navbar navbar-expand nav-c">
		<a class="navbar-brand" href="#"><i class="fas fa-binoculars"
			style="margin-right: 10px"></i>ELENCO DIPENDENTI</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#"
					id="navbarDropdownMenuLink" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false"> DIPENDENTI </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item"
							href="../InserimentoController/inserimento">AGGIUNGI
							DIPENDENTI</a> <a class="dropdown-item btn btn-primary"
							data-toggle="modal" data-target="#exampleModal"
							href="../VisualizzaController/visualizzaRuoli">VISUALIZZA
							RUOLI</a>
					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#"
					id="navbarDropdownMenuLink" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false"> PROGETTI </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item"
							href="../VisualizzaController/visualizzaProgetti">VISUALIZZA
							PROGETTI</a> <a class="dropdown-item"
							href="../InserimentoController/inserimentoProgetto">AGGIUNGI
							PROGETTO</a>
					</div>
				<li class="nav-item"><a class="nav-link" href="../ricerca.jsp">RICERCA</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- MODALE RUOLI  -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">VISUALIZZA RUOLI</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table">
						<thead class="thead-dark">

							<tr>
								<th>ADMIN</th>
								<th>GUEST</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${admins}</td>
								<td>${guests}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">CHIUDI</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /MODALE RUOLI  -->

	<div class="content">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('account.username')">USERNAME<i
							class="fas fa-caret-down" style="margin-left: 5px"></i></span></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('nome')">NOME<i class="fas fa-caret-down"
							style="margin-left: 5px"></i></span></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('cognome')">COGNOME</span><i
						class="fas fa-caret-down" style="margin-left: 5px"></i></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('eta')">ET�<i class="fas fa-caret-down"
							style="margin-left: 5px"></i></span></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('luogoDiNascita')">LUOGO DI NASCITA<i
							class="fas fa-caret-down" style="margin-left: 5px"></i></span></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('sesso')">SESSO<i class="fas fa-caret-down"
							style="margin-left: 5px"></i></span></th>
					<th scope="col"><span style="cursor: pointer"
						onclick="ordina('codiceFiscale')">CODICE FISCALE<i
							class="fas fa-caret-down" style="margin-left: 5px"></i></span></th>
					<th scope="col"></th>
					<th scope="col"></th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<c:forEach items="${listaDip}" var="d">
				<tbody>
					<tr>
						<th scope="row">${d.account.username}</th>
						<td>${d.nome}</td>
						<td>${d.cognome}</td>
						<td>${d.eta}</td>
						<td>${d.luogoDiNascita}</td>
						<td>${d.sesso}</td>
						<td>${d.codiceFiscale}</td>
						<td class="buttons" title="Dettaglio dipendente"><button
								class="btn btn-dark btn-sm"
								onclick="location.href='visualizza/${d.id}'">
								<i class="fa fa-info" aria-hidden="true"></i>
							</button></td>
						<td class="buttons" title="Modifica dipendente"><button
								class="btn btn-primary btn-sm"
								onclick="location.href='../ModificaController/modifica?id=${d.id}'">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</button></td>
						<td class="buttons" title="Cancella dipendente"><button
								class="btn btn-danger btn-sm" onclick="cancella(${d.id})">
								<i class="fa fa-trash-o" aria-hidden="true"></i>
							</button></td>
						<td class="buttons" title="Aggiungi dipendenti a progetto"><button
								class="btn btn-info btn-sm"
								onclick="iframe(${d.id})">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</button></td>
					</tr>
				</tbody>
			</c:forEach>

		</table>
		<div class="actions">
			<button type="button" title="Aggiungi dipendente"
				class="btn btn-success"
				onclick="location.href='../InserimentoController/inserimento'">
				<i class="fas fa-user-plus"></i>
			</button>
			<div class="navigation">
				<select id="sizePagina" name="sizePagina">
					<c:forEach var="i" begin="1" end="${maxSize}">
						<c:choose>
							<c:when test="${size == i}">
								<option value="${i}" selected>${i}</option>
							</c:when>
							<c:otherwise>
								<option value="${i}">${i}</option>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</select>
				<button type="button" class="btn btn-dark"
					onclick="location.href='visualizza?page=0&size=${size}&sort=${sort}&order=${order}'">&laquo;</button>

				<c:choose>
					<c:when test="${paginaCorrente > 0}">
						<button type="button" class="btn btn-dark"
							onclick="location.href='visualizza?page=${paginaCorrente-1}&size=${size}&sort=${sort}&order=${order}'"><</button>
					</c:when>
					<c:otherwise>
						<button type="button" class="btn btn-dark" onclick="#"><</button>
					</c:otherwise>
				</c:choose>
				<input type="text" class="current-page" value="${paginaCorrente+1}"
					disabled></input>
				<c:choose>
					<c:when test="${paginaCorrente+1 < maxPage}">
						<button type="button" class="btn btn-dark"
							onclick="location.href='visualizza?page=${paginaCorrente+1}&size=${size}&sort=${sort}&order=${order}'">></button>
					</c:when>
					<c:otherwise>
						<button type="button" class="btn btn-dark" onclick="#">></button>
					</c:otherwise>
				</c:choose>
				<button type="button" class="btn btn-dark"
					onclick="location.href='visualizza?page=${maxPage-1}&size=${size}&sort=${sort}&order=${order}'">&raquo;</button>
			</div>
		</div>
	</div>

	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-5.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-nav {
	margin-top: 3px;
}

.navbar-brand, .nav-c a {
	font-size: 13px;
	padding-top: 1px !important;
}

.content {
	width: 75%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 12px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span, h5, .btn {
	font-size: 12px;
}

.navigation {
	float: right;
}

.border {
	border-style: none none solid none !important;
	color: #fff;
}

#sizePagina, .current-page {
	border-radius: .25rem
}

.current-page {
	width: 30px;
	text-align: center;
	background-color: #fff;
}

.nav-c a {
	color: #2c2c2c;
}
</style>
</body>
</html>