package calcolatore;

public class Calcolatrice {
	public double add(double a, double b){
		return a+b;
	}

	public double sott(double a, double b ) {
		return a-b;
	}

	public double molt(double a, double b) {
		return a*b;
	}

	public double div(double a, double b) {
		return a/b;
	}
}