package calcolatore;
import java.util.Scanner;

public class Calcolatore {

	public static void main(String[] args) {
		String res;
		do {
			//double a=20;
			//double b=15;
			Calcolatrice c=new Calcolatrice();
			System.out.println(c.add(10, 20));
			System.out.println(c.sott(20, 2));
			System.out.println(c.molt(10, 30));
			System.out.println(c.div(40, 20));
			Scanner input=new Scanner(System.in);
			System.out.println("Vuoi ricominciare?");
			res = input.nextLine();
		} while (res.equalsIgnoreCase("si") || res.equalsIgnoreCase("s"));
	}
}