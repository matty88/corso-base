package eserciziJava;

import java.util.Scanner;

import model.Fattura;

public class Esercizi {
	Scanner scanner = new Scanner(System.in);
	Metodi m = new Metodi();

	public void contatore(int fine) {
		for(int i=1; i<=fine;i++) {
			System.out.println("Valore: "+i);
		}
	}

	public void visualizzaInteri() {
		int[] numeri = {1,24,45,56,68,32,346,547,6,76};
		for(int index=0; index<numeri.length; index++) {
			System.out.println(numeri[index]);
		}
	}

	public void sommaNumeri() {
		int somma=0;
		int[] numeri = {1,24,45,56,68,32,346,547,6,76};
		for(int index=0; index<numeri.length; index++) {
			System.out.println(numeri[index]);
			somma+=numeri[index];
		}
		System.out.println("Somma: "+somma);
	}

	public void pariDispari() {
		int pari = 0, dispari = 0;
		int[] numeri = {1,24,45,56,68,32,346,547,6,76};
		for(int index=0; index<numeri.length; index++) {
			System.out.println(numeri[index]);
			if(numeri[index]%2==0) {
				pari++;
			} else {
				dispari++;
			}
		}
		System.out.println("Numeri pari: "+pari);
		System.out.println("Numeri dispari: "+dispari);
	}

	public void contaDaInizioAFine(int inizio, int fine) {
		for(int i=inizio; i<=fine; i++) {
			System.out.println(i);
		}
	}

	public void contaDaInizioAFineSoloPari(int inizio, int fine) {
		for(int i=inizio; i<=fine; i++) {
			if(i%2==0) {
				System.out.println(i);
			}
		}
	}

	public void contaDaInizioAFineStepDue (int inizio,int fine) {
		for(int i=inizio; i<=fine; i+=+2) {
			//if(i%2==1) {
			System.out.println(i);
			//}
		}
	}

	public void contaAlContrario(int fine) {
		for(int i=fine; i>=1; i--) {
			System.out.println(i);
		}
	}

	public int totalizza() {
		int valore;
		int somma=0;
		String resp;
		do {
			valore=m.leggiIntero("Inserisci valore: ");
			somma+=valore;	
			resp=m.leggiStringa("Vuoi continuare? ");		
		}while(resp.equalsIgnoreCase("si") || resp.equalsIgnoreCase("s"));
		return somma;
	}

	public void selezioneUno() {
		int[] vett = {10,20,34,54,34,67,87,56,45,34,12,89,45,23,45,4,98,76,17,23};
		for(int index=0; index<vett.length; index++) {
			if(vett[index]>30) {
				System.out.println(vett[index]);
			}
		}
	}

	public void selezioneDue() {
		int[] vett = {15,2,43,554,37,89,14,56,45,45,12,56,5,78,48,4,99,78,567,26};
		for(int index=0; index<vett.length; index++) {
			if(vett[index]>=20 && vett[index]<50) {
				System.out.println(vett[index]);
			}
		}
	}

	public void selezioneTre() {
		int[] vett = {35,20,453,54,17,249,45,576,28,42,2,5,76,39,48,4,23,72,51,16};
		for(int index=0; index<vett.length; index++) {
			if(vett[index]<=20 || vett[index]>=40) {
				System.out.println(vett[index]);
			}
		}
	}

	public Fattura[] inserisciFattura() {
		Fattura[] fatture=new Fattura[4];
		for(int index=0; index<fatture.length; index++) {
			fatture[index] = new Fattura();
			fatture[index].numero=m.leggiStringa("Fattura "+(index+1)+":");
			fatture[index].data=m.leggiStringa("Data "+(index+1)+":");
			fatture[index].cliente=m.leggiStringa("Cliente "+(index+1)+":");
			fatture[index].imponibile=m.leggiDecimale("Imponibile "+(index+1)+":");			
		}
		return fatture;
	}

	public void stampaFatture(Fattura[] fatture) {
		for(int x=0;x<fatture.length;x++) {
			System.out.println(fatture[x].toString());
		}
	}


	public void ordinamentoFatturaCliente(Fattura[] fatture) {
		for(int i=0;i<fatture.length;i++) {
			for(int x=0;x<(fatture.length-1-i);x++) {
				String chiavePrimaria = fatture[x].cliente+fatture[x].numero;
				String chiaveSecondaria = fatture[x+1].cliente+fatture[x+1].numero;
				if(chiavePrimaria.compareTo(chiaveSecondaria)>0) {
					//SWAP
					Fattura temp=fatture[x];
					fatture[x]=fatture[x+1];
					fatture[x+1]=temp;
				}
			}
		}

	}

	public void stampaPerCliente(Fattura[] fatture) {
		System.out.println("STAMPA FATTURE PER CLIENTE");
		for(int cont=0;cont<fatture.length;cont++) {
			System.out.println(cont+1+")"+"Fattura cliente=" + fatture[cont].cliente + ", numero=" + fatture[cont].numero + ", data=" + fatture[cont].data + ", imponibile=" + fatture[cont].imponibile);
		}
	}

	public void rotturaCodice (Fattura[] fatture) {
		int x=0;
		int conta=0;
		double somma=0, imposta, importo;
		String appCliente;
		appCliente=fatture[x].cliente;
		do{
			if (fatture[x].cliente.equals(appCliente)) {
				conta++;
				somma+=fatture[x].imponibile;
			} else {
				imposta=somma*0.22;
				importo=somma+imposta;
				System.out.println("Numero fatture del cliente "+appCliente+":"+conta);
				System.out.println("Totale fatturato del cliente "+appCliente+":"+somma+ " Totale imposta: "+imposta+ " Totale importo: "+importo);
				appCliente=fatture[x].cliente;
				conta=1;
				somma=fatture[x].imponibile;
			}
			x++;
		}while(x<fatture.length);
		imposta=somma*0.22;
		importo=somma+imposta;
		System.out.println("Numero fatture del cliente "+appCliente+":"+conta);
		System.out.println("Totale fatturato del cliente "+appCliente+":"+somma+ " Totale imposta: "+imposta+ " Totale importo: "+importo);
	}

	public void ast() {
		String a ="*";
		String somma = "";
		for(int i=0;i<10;i++) {
			somma+=a;
			System.out.println(somma);
		}
	}
}


