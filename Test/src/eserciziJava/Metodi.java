package eserciziJava;

import java.util.Scanner;

public class Metodi {
	Scanner scanner = new Scanner(System.in);

	public String leggiStringa(String msg) {
		System.out.print(msg);
		return scanner.nextLine();
	} 

	public int leggiIntero(String num) {
		while (true) {  //WHILE TRUE
			try { 
				System.out.print(num);
				return Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}

	public double leggiDecimale(String num) {  
		while (true) { 
			try {          
				System.out.print(num);                           
				return Double.parseDouble(scanner.nextLine());    
			} catch (NumberFormatException e) {
				System.out.println("***ERRORE: non hai inserito un numero decimale valido!");
			}
		}
	}

}
