package model;

public class Persona {
	public int id;
	public String nome;
	public String cognome;
	public int eta;
	
	public String toString() {
		return id+", "+nome+", "+cognome+", "+eta;
	}
}
