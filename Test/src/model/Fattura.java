package model;

public class Fattura {
	public String numero;
	public String data;
	public String cliente;
	public double imponibile;
	public String toString() {
		return numero+", "+data+", "+cliente+", "+imponibile;
	}
}
