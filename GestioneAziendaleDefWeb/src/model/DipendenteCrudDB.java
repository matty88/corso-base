package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DipendenteCrudDB implements IDipendenteCrudDB {

	@Override
	public boolean inserisci(Dipendente d) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDB();
			String sql="insert into dipendenti (nome,cognome,data_di_nascita,luogo_di_nascita,sesso,codice_fiscale,stipendio,id_ruolo_aziendale,id_titolo_di_studio) values (?,?,?,?,?,?,?,?,?)";
			ps=conn.prepareStatement(sql);
			ps.setString(1,d.getNome());
			ps.setString(2,d.getCognome());
			ps.setDate(3,d.getDataDiNascita());
			ps.setString(4,d.getLuogoDiNascita());
			ps.setString(5,d.getSesso());
			ps.setString(6,d.getCodiceFiscale());
			ps.setDouble(7,d.getStipendio());
			ps.setInt(8,d.getIdRuoloAziendale());
			ps.setInt(9,d.getIdTitoloDiStudio());
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore d'inserimento! "+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	@Override
	public boolean modifica(int id, Dipendente d) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDB();
			String sql="update dipendenti set nome=?,cognome=?,data_di_nascita=?,luogo_di_nascita=?,sesso=?,codice_fiscale=?,stipendio=?,id_ruolo_aziendale=?,id_titolo_di_studio=? where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,d.getNome());
			ps.setString(2,d.getCognome());
			ps.setDate(3,d.getDataDiNascita());
			ps.setString(4,d.getLuogoDiNascita());
			ps.setString(5,d.getSesso());
			ps.setString(6,d.getCodiceFiscale());
			ps.setDouble(7,d.getStipendio());
			ps.setInt(8,d.getIdRuoloAziendale());
			ps.setInt(9,d.getIdTitoloDiStudio());
			ps.setInt(10,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di modifica!"+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	@Override
	public boolean cancella(int id) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDB();
			String sql="delete from dipendenti where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di cancellazione!");
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	@Override
	public Dipendente leggi(int id) {
		ResultSet rs=null;
		Dipendente dip=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDB();
			String sql="select * from dipendenti where id=?";
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1,id);
			rs=ps.executeQuery();
			if(rs.first()){
				dip=new Dipendente(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getDate("data_di_nascita"), 
						rs.getString("luogo_di_nascita"), 
						rs.getString("sesso"), 
						rs.getString("codice_fiscale"), 
						rs.getDouble("stipendio"), 						
						rs.getInt("id_ruolo_aziendale"),
						rs.getInt("id_titolo_di_studio"));
			}
			rs.close();
			ps.close(); 
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dip;
	}
	
	@Override
	public ArrayList<DipendenteDTO> leggi(int lunghezzaPagina, int paginaCorrente) {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		DipendenteDTO dip;
		int offSet=0;
		ArrayList<DipendenteDTO> dList=new ArrayList<DipendenteDTO>();
		try {
			conn=ConnettoreDB.getDB();
			String sql="select * from v_dipendenti order by id asc limit ? offset ?";
			ps=conn.prepareStatement(sql);
			offSet=(paginaCorrente-1)*lunghezzaPagina;
			ps.setInt(1, lunghezzaPagina);
			ps.setInt(2, offSet);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new DipendenteDTO(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getDate("data di nascita"), 
						rs.getString("luogo di nascita"), 
						rs.getString("sesso"), 
						rs.getString("codice fiscale"), 
						rs.getDouble("stipendio"), 		
						rs.getString("ruolo aziendale"),
						rs.getString("titolo di studio"));
				dList.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dList;
	}
	
	@Override
	public ArrayList<DipendenteDTO> leggiDTO() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		DipendenteDTO dip;
		ArrayList<DipendenteDTO> dList=new ArrayList<DipendenteDTO>();
		try {
			conn=ConnettoreDB.getDB();
			String sql="select * from v_dipendenti";
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new DipendenteDTO(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getDate("data di nascita"), 
						rs.getString("luogo di nascita"), 
						rs.getString("sesso"), 
						rs.getString("codice fiscale"), 
						rs.getDouble("stipendio"), 		
						rs.getString("ruolo aziendale"),
						rs.getString("titolo di studio"));
				dList.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dList;
	}
	
	@Override
	public int getTotalePagine(int lunghezzaPagina) {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		int record=-1;
		try {
			conn=ConnettoreDB.getDB();                         
			String sql="select count(*) as record from dipendenti";      
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rs=ps.executeQuery();
			if(rs.first()) {
				record=rs.getInt("record");
			}
			ps.close();
			rs.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura numero record!"+e.getMessage());
			return record;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return record;
		}
		return (int)Math.ceil((double)record/lunghezzaPagina);
	}

}
