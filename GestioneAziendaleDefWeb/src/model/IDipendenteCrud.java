package model;

import java.util.HashMap;

public interface IDipendenteCrud {

	public boolean inserisci(Dipendente d);   

	public boolean modifica(int id, Dipendente d);

	public boolean cancella(int id);

	public Dipendente leggi(int id);

	public HashMap<Integer,Dipendente> leggi();
	
	public boolean salva();
	
	public boolean carica();

}
