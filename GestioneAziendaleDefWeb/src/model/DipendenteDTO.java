package model;

import java.sql.Date;

public class DipendenteDTO extends Persona {

	private double stipendio;
	private String ruoloAziendale;
	private String titoloDiStudio;

	public double getStipendio() {
		return stipendio;
	}

	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}

	public String getRuoloAziendale() {
		return ruoloAziendale;
	}

	public void setRuoloAziendale(String ruoloAziendale) {
		this.ruoloAziendale = ruoloAziendale;
	}

	public String getTitoloDiStudio() {
		return titoloDiStudio;
	}

	public void setTitoloDiStudio(String titoloDiStudio) {
		this.titoloDiStudio = titoloDiStudio;
	}

	@Override
	public String toString() {
		return super.toString() + ", " + stipendio + ", " + ruoloAziendale
				+ ", " + titoloDiStudio;
	}
	
	public DipendenteDTO(int id, String nome, String cognome, Date dataDiNascita, String luogoDiNascita, String sesso,
			String codiceFiscale, double stipendio, String ruoloAziendale, String titoloDiStudio) {
		super(id, nome, cognome, dataDiNascita, luogoDiNascita, sesso, codiceFiscale);
		this.stipendio = stipendio;
		this.ruoloAziendale = ruoloAziendale;
		this.titoloDiStudio = titoloDiStudio;
	}
	
	public DipendenteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ruoloAziendale == null) ? 0 : ruoloAziendale.hashCode());
		long temp;
		temp = Double.doubleToLongBits(stipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((titoloDiStudio == null) ? 0 : titoloDiStudio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DipendenteDTO other = (DipendenteDTO) obj;
		if (ruoloAziendale == null) {
			if (other.ruoloAziendale != null)
				return false;
		} else if (!ruoloAziendale.equals(other.ruoloAziendale))
			return false;
		if (Double.doubleToLongBits(stipendio) != Double.doubleToLongBits(other.stipendio))
			return false;
		if (titoloDiStudio == null) {
			if (other.titoloDiStudio != null)
				return false;
		} else if (!titoloDiStudio.equals(other.titoloDiStudio))
			return false;
		return true;
	}
}
