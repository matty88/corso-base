package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;

public class DipendenteCrud implements IDipendenteCrud{

	private HashMap<Integer,Dipendente>dMap = new HashMap<Integer,Dipendente>();

	@Override
	public boolean inserisci(Dipendente d) {
		if(dMap.get(d.getId())==null) {
			dMap.put(d.getId(),d);
			return true;
		}
		return false;
	}

	@Override
	public boolean modifica(int id, Dipendente d) {
		dMap.replace(id,d);
		return true;
	}

	@Override
	public boolean cancella(int id) {
		dMap.remove(id);
		return true;
	}

	@Override
	public Dipendente leggi(int id) {
		return dMap.get(id);
	}

	@Override
	public HashMap<Integer, Dipendente> leggi() {
		return dMap;
	}

	@Override
	public boolean salva() {
		boolean status=false;
		try{                                     
			FileWriter file = new FileWriter("c://Dati//file.dat");     
			BufferedWriter output = new BufferedWriter(file);     
			for(int key: dMap.keySet()) {   
				output.write(dMap.get(key).toString());           
				output.newLine();                                 
			}
			output.close();  
			status = true;
		}catch (IOException e) {                                          
			System.out.println("Attenzione errore:"+e.getMessage());     
		}
		return status;
	}

	@Override
	public boolean carica() {
		boolean status=false;
		try {
			FileReader file = new FileReader("c://Dati//file.dat");      
			BufferedReader input = new BufferedReader(file);
			String line = null;
			while ((line = input.readLine()) != null) {           
				String[] attributi=line.split(",");                      
				Dipendente d = new Dipendente();
				d.setId(Integer.parseInt(attributi[0].trim()));         
				d.setNome(attributi[1].trim());                
				d.setCognome(attributi[2].trim());
				d.setDataDiNascita(Date.valueOf(attributi[3].trim()));
				d.setLuogoDiNascita(attributi[4].trim());
				d.setSesso(attributi[5].trim());
				d.setCodiceFiscale(attributi[6]);
				d.setStipendio(Double.valueOf(attributi[7].trim()));
				d.setIdRuoloAziendale(Integer.parseInt(attributi[8].trim()));  
				d.setIdTitoloDiStudio(Integer.parseInt(attributi[9].trim()));  		
				dMap.put(d.getId(),d);
			}
			input.close();
			status = true;
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return status;       
	}

//	public DipendenteCrud() {
//		carica();
//	}


}

