package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Dipendente;
import model.DipendenteCrudDB;


/**
 * Servlet implementation class GestioneAziendale
 */
@WebServlet("/gestione_aziendale")
public class GestioneAziendale extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestioneAziendale() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		DipendenteCrudDB dc = new DipendenteCrudDB();
		Dipendente d;
		RequestDispatcher view = null;
		ArrayList<String> errori = new ArrayList<String>();
		int scelta;
		scelta=Integer.parseInt(request.getParameter("scelta"));
		switch(scelta) {              
		case 1:
			d=new Dipendente();
			d.setNome(request.getParameter("nome"));
			d.setCognome(request.getParameter("cognome"));
			d.setDataDiNascita(Date.valueOf((request.getParameter("data_di_nascita"))));
			d.setLuogoDiNascita(request.getParameter("luogo_di_nascita"));
			d.setSesso(request.getParameter("sesso"));
			d.setCodiceFiscale(request.getParameter("codice_fiscale").toUpperCase());
			d.setStipendio(Double.parseDouble((request.getParameter("stipendio"))));
			d.setIdRuoloAziendale(Integer.parseInt((request.getParameter("ruolo_aziendale"))));
			d.setIdTitoloDiStudio(Integer.parseInt((request.getParameter("titolo_di_studio"))));
			dc.inserisci(d);
			response.sendRedirect("inserimento.html");
			break;           
		case 2:
			d=new Dipendente();
			d.setId(Integer.parseInt(request.getParameter("id")));
			d.setNome(request.getParameter("nome"));
			d.setCognome(request.getParameter("cognome"));
			d.setDataDiNascita(Date.valueOf((request.getParameter("data_di_nascita"))));
			d.setLuogoDiNascita(request.getParameter("luogo_di_nascita"));
			d.setSesso(request.getParameter("sesso"));
			d.setCodiceFiscale(request.getParameter("codice_fiscale").toUpperCase());
			d.setStipendio(Double.parseDouble((request.getParameter("stipendio"))));
			d.setIdRuoloAziendale(Integer.parseInt((request.getParameter("ruolo_aziendale"))));
			d.setIdTitoloDiStudio(Integer.parseInt((request.getParameter("titolo_di_studio"))));
			dc.inserisci(d);
			response.sendRedirect("inserimento.html");
			break;
		case 3:
			int id=Integer.parseInt(request.getParameter("id"));
			dc.cancella(id);
			response.sendRedirect("cancellazione.html");
			break; 
		case 4:
			id=Integer.parseInt(request.getParameter("id"));
			request.setAttribute("dipendente",dc.leggi(id));
			view=request.getRequestDispatcher("ricerca_dipendente.jsp");
			view.forward(request, response);
			break; 
		case 5:
			int sceltaPag = 0;
			int lunghezzaPagina = 5;
			int paginaCorrente = 1;
			switch(sceltaPag) {
			case 1:
				paginaCorrente = 1;
				break;
			case 2:
				if(paginaCorrente>1) {
					paginaCorrente--;				
				}
				break;
			case 3:
				if(paginaCorrente<dc.getTotalePagine(lunghezzaPagina)) {
					paginaCorrente++;			
				}
				break;
			case 4:
				paginaCorrente = dc.getTotalePagine(lunghezzaPagina);
				break;
			}
			request.setAttribute("dipendenti", dc.leggi(lunghezzaPagina,paginaCorrente));
			view=request.getRequestDispatcher("visualizza_dipendenti.jsp");
			view.forward(request, response);
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
