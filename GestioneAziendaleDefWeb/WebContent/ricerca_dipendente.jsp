<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Visualizza dipendente</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="content">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">ID</th>
					<th scope="col">NOME</th>
					<th scope="col">COGNOME</th>
					<th scope="col">LUOGO DI NASCITA</th>
					<th scope="col">DATA DI NASCITA</th>
					<th scope="col">SESSO</th>
					<th scope="col">TITOLO DI STUDIO</th>
					<th scope="col">CODICE FISCALE</th>
					<th scope="col">RUOLO AZIENDALE</th>
					<th scope="col">STIPENDIO</th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row"></th>
					<td>${dipendente.getId()}</td>
					<td>${dipendente.getNome()}</td>
					<td>${dipendente.getCognome()}</td>
					<td>${dipendente.getLuogoDiNascita()}</td>
					<td>${dipendente.getDataDiNascita()}</td>
					<td>${dipendente.getSesso()}</td>
					<td>${dipendente.getTitoloDiStudio()}</td>
					<td>${dipendente.getCodiceFiscale()}</td>
					<td>${dipendente.getRuoloAziendale()}</td>
					<td>${dipendente.getStipendio()}</td>
					<td class="buttons"><button class="btn btn-primary btn-sm"
							onclick="location.href='gestione_aziendale?scelta=2&id=<${dipendente.getId()}'">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</button></td>
					<td class="buttons"><button class="btn btn-danger btn-sm"
							onclick="conferma(${dipendente.getId()})">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</button></td>
			</tbody>
		</table>
		<div class="redirect">
			<a href="gestione_aziendale?scelta=5"><i class="fas fa-undo-alt"></i>TORNA ALL ELENCO</a>
		</div>
	</div>
	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-brand {
	font-size: 13px;
	padding-top: 1px !important;
}

.fas {
	margin-right: 10px;
}

.content {
	width: 87%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 11px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span {
	font-size: 12px;
}

.navigation {
	float: right;
}

#sizePagina {
	border-radius: .25rem
}

span {
	color: #fff;
	margin-right: 15px;
	text-transform: uppercase;
}

.redirect {
	text-align:center;
}

a {	
	font-size: 13px;
	color: #2c2c2c;
}

a:hover {
	color: #fff;
}
</style>

</body>
</html>