package model;

public class Dirigente extends Manager {

	private String livelloFunzionale;

	public String getLivelloFunzionale() {
		return livelloFunzionale;
	}

	public void setLivelloFunzionale(String livelloFunzionale) {
		this.livelloFunzionale = livelloFunzionale;
	}

	public Dirigente(int id, String nome, String cognome, int eta, String sesso, Double stipendio, String ruolo,
			String areaResponsabilita, String livelloFunzionale) {
		super(id, nome, cognome, eta, sesso, stipendio, ruolo, areaResponsabilita);
		this.livelloFunzionale = livelloFunzionale;
	}

	public Dirigente() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + ", Livello funzionale = " + livelloFunzionale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((livelloFunzionale == null) ? 0 : livelloFunzionale.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dirigente other = (Dirigente) obj;
		if (livelloFunzionale == null) {
			if (other.livelloFunzionale != null)
				return false;
		} else if (!livelloFunzionale.equals(other.livelloFunzionale))
			return false;
		return true;
	}

//	public Dirigente(String nome, String cognome, int eta, Double stipendio, String ruolo,
//			String areaResponsabilita) {
//		super(nome, cognome, eta, stipendio, ruolo, areaResponsabilita);
//		// TODO Auto-generated constructor stub
//	}

	
	
}
