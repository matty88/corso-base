package model;

import java.util.List;

public interface IPersonaCrud {
	
public boolean inserisci(Persona p);   //ABSTRACT
	
	public boolean modifica(int index, Persona p);
	
	public boolean cancella(int id);
	
	public Persona leggi(int id);
	
	public List<Persona> leggi();

}
