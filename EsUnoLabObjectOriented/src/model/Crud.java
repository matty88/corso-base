package model;

import java.util.ArrayList;
import java.util.List;

public class Crud implements IPersonaCrud {

	List<Persona>persone = new ArrayList<Persona>();

	@Override
	public boolean inserisci(Persona p) {   
		persone.add(p);
		return true;
	}

	@Override
	public List<Persona> leggi() {
		return persone;                        
	}

	@Override
	public boolean modifica(int id, Persona p) {  
		persone.set(id, p);   
		return true;
	}

	@Override
	public boolean cancella(int id) {
		persone.remove(id);
		return true;
	}

	@Override
	public Persona leggi(int id) { 
		int indice=cercaIndice(id);
		if(indice==-1){
			return null;
		} else {
			return persone.get(indice);           
		}
	}

	private int cercaIndice(int id) {
		int indice = 0;
		for(Persona p : persone) {
			if(id==p.getId()) {
				return indice;
			}
			indice++;
		}
		return -1;
	}

	public Persona cercaCognome(String cognome) {
		return (cognome!=null && !cognome.isBlank()) ?   //CONTROLLA COGNOME ED EVITA CHE COGNOME SIA NULL O BLANK
				leggi().stream()                         //STREAMMA L'ARRAYLIST    
				.filter(p -> cognome.equalsIgnoreCase(p.getCognome()))   //DURANTE LO STREAM SELEZIONA SOLO GLI ELEMENTI CHE RISPETTANO IL PREDICATO
				.findFirst().orElse(null)             //DEGLI ELEMENTI CHE RIMANGONO DAL FILTRO PRENDI SOLO IL PRIMO E SE NON C'� NE SONO TORNA NULL
				:null;                          //L'ELSE DELL IF TERNARIO
	}
}
