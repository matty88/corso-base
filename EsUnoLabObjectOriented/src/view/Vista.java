package view;

import java.util.List;
import java.util.Scanner;
import model.Crud;
import model.Dipendente;
import model.Dirigente;
import model.Manager;
import model.Persona;

public class Vista {

	Scanner scanner = new Scanner(System.in);
	Crud c = new Crud();

	public void menuInizialeInserimento() {
		printMsg("1) Persona");
		printMsg("2) Dipendente");
		printMsg("3) Manager");   
		printMsg("4) Dirigente");   
		printMsg("5) Torna al menu principale");
	}

	public void menuInizialePrincipale() {
		printMsg("1) Inserimento");
		printMsg("2) Modifica");
		printMsg("3) Cancella");
		printMsg("4) Trova");
		printMsg("5) Visualizza elenco");
		printMsg("6) Visualizza scheda");
		printMsg("7) Esci");
	}

	public void confronto(Persona p, Persona p1) {
		if(p.equals(p1)) {
			printMsg("");
			printMsg("Gli oggetti sono uguali\n");
		} else {
			printMsg("");
			printMsg("Gli oggetti sono diversi\n");
		}
	}

	public void creaPersona (Persona p) {
		printMsg("");
		p.setId(leggiIntero("Id: "));
		p.setNome(leggiStringa("Nome: "));
		p.setCognome(leggiStringa("Cognome: "));
		p.setEta(leggiIntero("EtÓ: "));
		p.setSesso(leggiStringa("Sesso: "));
		if(p instanceof Dirigente) {
			Dirigente d = (Dirigente) p;
			d.setStipendio(leggiDecimale("Stipendio: "));
			d.setRuolo(leggiStringa("Ruolo: "));
			d.setAreaResponsabilita(leggiStringa("Area responsabilitÓ: "));
			d.setLivelloFunzionale(leggiStringa("Livello funzionale: "));
		} else if (p instanceof Manager) {
			Manager man = (Manager) p;
			man.setStipendio(leggiDecimale("Stipendio: "));
			man.setRuolo(leggiStringa("Ruolo: "));
			man.setAreaResponsabilita(leggiStringa("Area responsabilitÓ: "));			
		} else if (p instanceof Dipendente) {
			Dipendente d = (Dipendente) p;
			d.setStipendio(leggiDecimale("Stipendio: "));
			d.setRuolo(leggiStringa("Ruolo: "));
		}	
	}

	public void modificaPersona(Persona p) {
		boolean flag;
		String appAttributo; 
		printMsg("");
		appAttributo = leggiStringa("Nome:["+p.getNome()+"]:");
		if(!appAttributo.equals("")) {
			p.setNome(appAttributo);
		}
		appAttributo = leggiStringa("Cognome:["+p.getCognome()+"]:");
		if(!appAttributo.equals("")) {
			p.setCognome(appAttributo);
		}
		do {
			flag=false;
			appAttributo = leggiStringa("EtÓ:["+p.getEta()+"]:");
			if(!appAttributo.equals("")) {
				try {
					p.setEta(Integer.parseInt(appAttributo));
				} catch(NumberFormatException e) {
					printMsg("Inserire un numero intero!");
					flag=true;
				}
			}
		}while(flag);
		appAttributo = leggiStringa("Sesso:["+p.getSesso()+"]:");
		if(!appAttributo.equals("")) {
			p.setSesso(appAttributo);
		}
		if(p instanceof Dirigente) {
			Dirigente d = (Dirigente) p;
			do {
				flag=false;
				appAttributo = leggiStringa("Stipendio:["+d.getStipendio()+"]:");
				if(!appAttributo.equals("")) {
					try {
						d.setStipendio(Double.parseDouble(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			appAttributo = leggiStringa("Ruolo:["+d.getRuolo()+"]:");
			if(!appAttributo.equals("")) {
				d.setRuolo(appAttributo);
			}
			appAttributo = leggiStringa("Area responsabilitÓ:["+d.getAreaResponsabilita()+"]:");
			if(!appAttributo.equals("")) {
				d.setAreaResponsabilita(appAttributo);
			}
			appAttributo = leggiStringa("Livello funzionale:["+d.getLivelloFunzionale()+"]:");
			if(!appAttributo.equals("")) {
				d.setLivelloFunzionale(appAttributo);
			}
		} else if (p instanceof Manager) {
			Manager man = (Manager) p;
			do {
				flag=false;
				appAttributo = leggiStringa("Stipendio:["+man.getStipendio()+"]:");
				if(!appAttributo.equals("")) {
					try {
						man.setStipendio(Double.parseDouble(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			appAttributo = leggiStringa("Ruolo:["+man.getRuolo()+"]:");
			if(!appAttributo.equals("")) {
				man.setRuolo(appAttributo);
			}
			appAttributo = leggiStringa("Area responsabilitÓ:["+man.getAreaResponsabilita()+"]:");
			if(!appAttributo.equals("")) {
				man.setAreaResponsabilita(appAttributo);
			}		
		} else if (p instanceof Dipendente) {
			Dipendente d = (Dipendente) p;
			do {
				flag=false;
				appAttributo = leggiStringa("Stipendio:["+d.getStipendio()+"]:");
				if(!appAttributo.equals("")) {
					try {
						d.setStipendio(Double.parseDouble(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			appAttributo = leggiStringa("Ruolo:["+d.getRuolo()+"]:");
			if(!appAttributo.equals("")) {
				d.setRuolo(appAttributo);
			}
		}	
	}

	public void stampaArray(List<Persona> p) {
		for(int index=0; index<p.size(); index++) {
			printMsg(p.get(index).toString()+"\n");
		}
	}

	public void stampaArrayDue(List<Persona> pList) {
		if(pList !=null && !pList.isEmpty()) {
			for(Persona p: pList) {
				printMsg(p+"\n");
			}
		}
	}

	public void printMsg(String msg) {
		System.out.println(msg);
	}

	public static void stampaScheda(Persona p) {
		System.out.println("");
		System.out.println("Nome: "+p.getNome());
		System.out.println("Cognome: "+p.getCognome());
		System.out.println("EtÓ: "+p.getEta());
		System.out.println("Sesso: "+p.getSesso());
		if(p instanceof Dirigente) {
			Dirigente d = (Dirigente) p;
			System.out.println("Stipendio: "+d.getStipendio());
			System.out.println("Ruolo: "+d.getRuolo());
			System.out.println("Area responsabilitÓ: "+d.getAreaResponsabilita());
			System.out.println("Livello funzionale: "+d.getLivelloFunzionale());
		} else if (p instanceof Manager) {
			Manager man = (Manager) p;
			System.out.println("Stipendio: "+man.getStipendio());
			System.out.println("Ruolo: "+man.getRuolo());
			System.out.println("Area responsabilitÓ: "+man.getAreaResponsabilita());			
		} else if (p instanceof Dipendente) {
			Dipendente d = (Dipendente) p;
			System.out.println("Stipendio: "+d.getStipendio());
			System.out.println("Ruolo: "+d.getRuolo());
		}	
		System.out.println("");
	}	

	public void stampaSchedaDue(List<Persona>p) {
		for(int index=0; index<p.size(); index++) {
			printMsg("");
			printMsg("Nome: "+p.get(index).getNome());
			printMsg("Cognome: "+p.get(index).getCognome());
			printMsg("EtÓ: "+p.get(index).getEta());
			printMsg("Sesso: "+p.get(index).getSesso());
			if(p.get(index) instanceof Dirigente) {
				Dirigente d = (Dirigente) p.get(index);
				printMsg("Stipendio: "+d.getStipendio());
				printMsg("Ruolo: "+d.getRuolo());
				printMsg("Area responsabilitÓ: "+d.getAreaResponsabilita());
				printMsg("Livello funzionale: "+d.getLivelloFunzionale());
			} else if (p.get(index) instanceof Manager) {
				Manager man = (Manager) p.get(index);
				printMsg("Stipendio: "+man.getStipendio());
				printMsg("Ruolo: "+man.getRuolo());
				printMsg("Area responsabilitÓ: "+man.getAreaResponsabilita());			
			} else if (p.get(index) instanceof Dipendente) {
				Dipendente d = (Dipendente) p.get(index);
				printMsg("Stipendio: "+d.getStipendio());
				printMsg("Ruolo: "+d.getRuolo());
			}	
			printMsg("");
		}
	}

	public int cercaId(int id, List<Persona> pList) {
		for(int i=0;i<pList.size();i++) {
			if(id==pList.get(i).getId()) {
				return i;
			}
		}
		return -1;
	}

	public String leggiStringa(String msg) {
		printMsg(msg);
		return scanner.nextLine();
	} 

	public int leggiIntero(String num) {
		while (true) {  //WHILE TRUE
			try { 
				printMsg(num);
				return Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}

	public double leggiDecimale(String num) {  
		while (true) { 
			try {          
				printMsg(num);                           
				return Double.parseDouble(scanner.nextLine());    
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero decimale valido!");
			}
		}
	}

	/* 
	 *::METHOD REFERENCE EQUIVALE A SCRIVERE pList.forEach(p -> stampaScheda(p));
	 QUESTO FUNZIONA PERCHE' L'INPUT DI STAMPA SCHEDA E' DELLA STESSA CLASSE DI UN SINGOLO
	 ELEMENTO DELL'ARRAY
	 */

	public void stampaSchede(List<Persona> pList) {
		if(pList !=null && !pList.isEmpty()) {
			pList.forEach(Vista::stampaScheda);
		}
	}


	public void cercaScheda(Persona p) {
		if(p !=null) {
			stampaScheda(p);
		} else {
			printMsg("Nessun utente trovato!");
		}
	}

	//	public Persona cercaCognome(String cognome) {
	//		return (cognome!=null && !cognome.isBlank()) ?   //CONTROLLA COGNOME ED EVITA CHE COGNOME SIA NULL O BLANK
	//				c.leggi().stream()                         //STREAMMA L'ARRAYLIST    
	//				.filter(p -> cognome.equalsIgnoreCase(p.getCognome()))   //DURANTE LO STREAM SELEZIONA SOLO GLI ELEMENTI CHE RISPETTANO IL PREDICATO
	//				.findFirst().orElse(null)             //DEGLI ELEMENTI CHE RIMANGONO DAL FILTRO PRENDI SOLO IL PRIMO E SE NON C'Ŕ NE SONO TORNA NULL
	//				:null;                          //L'ELSE DELL IF TERNARIO
	//	}

	public Persona cercaPerId(int id) {
		return (id!=-1) ? c.leggi().stream()                         
				.filter(p -> id == p.getId())   
				.findFirst().orElse(null)            
				:null;                          
	}

}
