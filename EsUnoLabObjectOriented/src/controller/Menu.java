package controller;

import model.Crud;
import model.Dipendente;
import model.Dirigente;
import model.Manager;
import model.Persona;
import view.Vista;

public class Menu {
	Vista v = new Vista();
	Crud c = new Crud();
	Persona p;

	public void menuPrincipale() {
		int scelta;
		String risp;
		do {
			v.menuInizialePrincipale();
			scelta=v.leggiIntero("Scegli una voce di menu: \n");
			switch(scelta) {              
			case 1:
				menuInserimento();
				break; 
			case 2:
				v.printMsg("");
				v.stampaArray(c.leggi());
				
				break;
			case 3:			
				v.printMsg("");
				v.stampaArray(c.leggi());
				do {
					scelta=v.leggiIntero("Scegli Id della persona da cancellare: ");
					scelta=v.cercaId(scelta, c.leggi());
					if(scelta==-1) {
						risp=v.leggiStringa("Id non trovato, premi INVIO per riprovare.");
					}
				}while(scelta==-1);
				if(scelta!=-1) {
					p=c.leggi(scelta);
					v.stampaSchedaDue(c.leggi());
					risp=v.leggiStringa("Vuoi confermare la cancellazione? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						c.cancella(scelta);
						v.printMsg("\nCancellazione effettuata con successo");
					} else {
						risp=v.leggiStringa("Cancellazione annullata, premi INVIO per continuare.");
					}
				}
				break;
			case 4:
				String cognome = v.leggiStringa("Inserisci cognome: ");
				v.cercaScheda(c.cercaCognome(cognome));
				break;
			case 5:
				v.stampaArray(c.leggi());
				break;
			case 6:
				v.stampaSchede(c.leggi());
				break; 
			case 7:
				break;	
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 7");   
			}
		} while(scelta!=7);
		v.printMsg("Programma terminato");
	}

	private void menuInserimento() {
		int scelta;
		do {
			v.menuInizialeInserimento();
			scelta=v.leggiIntero("Cosa vuoi inserire?: ");
			switch(scelta) {              
			case 1:
				sceltaOggetto(scelta);
				break;           
			case 2:
				sceltaOggetto(scelta);
				break;
			case 3:
				sceltaOggetto(scelta);
				break; 
			case 4:
				sceltaOggetto(scelta);
				break; 
			case 5:
				break;			
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 5");   
			}
		} while(scelta!=5);
		v.printMsg("Programma terminato");
	}

	private void sceltaOggetto(int num) {
		if(num==1) {
			p = new Persona(); 
		} else if (num==2) {	
			p = new Dipendente();
		} else if (num==3) {	
			p = new Manager(); 
		} else {
			p = new Dirigente();
		}
		v.creaPersona(p);
		c.inserisci(p);
		v.printMsg("\nInserimento avvenuto con successo!");
	}
}

