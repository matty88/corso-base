package esValRif;

public class Avvio {

	public static void main(String[] args) {
		PassaggioPerValoreRiferimento ppvr = new PassaggioPerValoreRiferimento();
		//ESERCIZIO 1
		String[] nomi = {"Mario", "Roberto", "Pietro", "Valentina", "Andrea"};  //ASSEGNAMENTO
		ppvr.stampaNominativi(nomi);
		//ESERCIZIO 2
		int[] numeri = ppvr.creaVettoreInteri();
		char car;
		int iCar = 64;
		for(int x=0; x<numeri.length; x++) {
			iCar++;
			car=(char)iCar;
			System.out.print(car+")"+numeri[x]+" ");
		}
		System.out.println("");
		//System.out.println("a)"+numeri[0]+" b)"+numeri[1]+" c)"+numeri[2]+" d)"+numeri[3]+" e)"+numeri[4]+" f)"+numeri[5]+" g)"+numeri[6]+" h)"+numeri[7]+" i)"+numeri[8]+" l)"+numeri[9]);
		//ESERCIZIO 3
		int[] vett = ppvr.creaVettoreInteri();
		ppvr.stampaVettore(vett);
		//ESERCIZIO 4
		Cerchio c1 = new Cerchio();
		Rettangolo r1 = new Rettangolo();
		Triangolo t1 = new Triangolo();
		//VALORI CERCHIO
		c1.raggio = ppvr.leggiDecimale("Dai un valore decimale al raggio del cerchio: ");
		ppvr.infoCerchio(c1);
		//VALORI RETTANGOLO
		r1.latoA = ppvr.leggiDecimale("Dai un valore decimale al lato A del rettangolo: ");
		r1.latoB = ppvr.leggiDecimale("Dai un valore decimale al lato B del rettangolo: ");
		ppvr.infoRettangolo(r1);	
		//VALORI TRIANGOLO
		do {
			t1.latoA = ppvr.leggiDecimale("Dai un valore decimale al lato A del triangolo: ");
			t1.latoB = ppvr.leggiDecimale("Dai un valore decimale al lato B del triangolo: ");
			t1.latoC = ppvr.leggiDecimale("Dai un valore decimale al lato C del triangolo: ");
			if (!ppvr.verificaTriangoloValido(t1)) {
				System.out.println("Il triangolo non � valido");
			}
		} while (!ppvr.verificaTriangoloValido(t1));
		ppvr.infoTriangolo(t1);
	}
}
