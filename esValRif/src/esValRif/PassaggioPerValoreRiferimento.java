package esValRif;
import java.util.Scanner;
public class PassaggioPerValoreRiferimento {

	public void stampaNominativi(String[] nomi) {
		for(int index=0; index<nomi.length; index++) {
			System.out.println(index+1+")"+nomi[index]);
		}		
	}

	public int[] creaVettoreInteri() {
		int[] numeri = new int[10];
		for(int i=0; i<numeri.length; i++) {
			numeri[i]= leggiIntero("Dai un numero alla variabile " + (i+1) + " del vettore:");	
		}
		return numeri;
	}

	public void stampaVettore(int[] vett) {
		for(int x=0; x<vett.length; x++) {
			System.out.println(vett[x]+" ");
		}
	}

	public void infoCerchio(Cerchio c) {
		double circonferenza, area;
		circonferenza = (c.raggio*2)*Math.PI;
		area = Math.PI*Math.pow(c.raggio,2);
		System.out.println("La circonferenza del cerchio �: "+circonferenza);
		System.out.println("L'area del cerchio �: "+area);
	}

	public void infoRettangolo(Rettangolo r) {
		double perimetro, area;
		perimetro = (r.latoA+r.latoB)*2;
		area = r.latoA*r.latoB;
		System.out.println("Il perimetro del rettangolo �: "+perimetro);
		System.out.println("L'area del rettangolo �: "+area);
	}

	public void infoTriangolo(Triangolo t) {
		double perimetro, area, p;
		p = (t.latoA+t.latoB+t.latoB)/2;
		area = Math.sqrt(p*(p-t.latoA)*(p-t.latoB)*(p-t.latoC));
		perimetro = t.latoA+t.latoB+t.latoC;
		System.out.println("Il perimetro del triangolo �: "+perimetro);
		System.out.println("L'area del triangolo �: "+area);
	}
	
	public boolean verificaTriangoloValido(Triangolo t) {
		return (t.latoA+t.latoB)>t.latoC && (t.latoA+t.latoC)>t.latoB && (t.latoB+t.latoC)>t.latoA;
	}

	public int leggiIntero(String num) {
		while (true) {  //WHILE TRUE
			try { 
				Scanner input=new Scanner(System.in);
				System.out.print(num);
				return Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}

	public double leggiDecimale(String msg) {  
		while (true) { 
			try { 
				Scanner input=new Scanner(System.in);            
				System.out.print(msg);                           
				return Double.parseDouble(input.nextLine());    
			} catch (NumberFormatException e) {
				System.out.println("***ERRORE: non hai inserito un numero decimale valido!");
			}
		}
	}
}
