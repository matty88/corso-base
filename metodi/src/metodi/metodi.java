package metodi;
//import metodi.MetodiInputOutput;
public class metodi {
	
	//I METODI SONO UN INSIEME DI ISTRUZIONI(TASK) CHE DETERMINANO IL COMPORTAMENTO DI UNA CLASSE
	//I METODI STATIC SI USANO SOLO NELLE CLASSI D'AVVIO(MAIN)
	//I METODI PUBLIC POSSONO ESSERE USATI LIBERAMENTE NEL PROGRAMMA E DA TUTTE LE CLASSI
	//GLI ARGOMENTI SONO VARIABILI CHE VENGONO PASSATE IN INGRESSO AL METODO ED INIZIALIZZATE QUANDO QUESTI VIENE CHIAMATO
	//PER CONVENZIONE SI USA IL PREFISSO GET
	//HANNO UN NOME MNEMONICO(PER FARTI CAPIRE A COSA SERVONO)
	//NELLA DICHIARAZIONE DEL METODO SE TI RESTITUISCE QUALCOSA, BISOGNA INDICARE IL TIPO DEL VALORE RESTITUITO(INT, BOOLEAN, DOUBLE, STRING)
	//RETURN = DAMMI IL CONTENUTO (SE NON E' VOID)
	//SE E' DI TIPO VOID NON ESSENDOCI UN RETURN GESTIAMO SOLO IL METODO CHIAMANDOLO DIRETTAMENTE
	//I METODI CHE HANNO UN RETURN VANNO GESTITI STAMPANDOLI O ASSEGNANDOLI AD UNA VARIABILE
	
	public static void main(String[] args) {
		//PASSAGGIO PER VALORI O RIFERIMENTO
		PassaggioPerValoreRiferimento ppvr = new PassaggioPerValoreRiferimento();
		System.out.println(ppvr.divisioneConValore(10.0, 2.0));
		//passaggio per valore (che avviene per i valori primitivi(boolean, int, char e double) sto assegnando a due variabili locali direttamente un contenuto
		double[] decimali = {10.0, 2.0};
		System.out.println(ppvr.divisioneConRiferimento(decimali));
		//passaggio per riferimento in questo caso assegnamo l'indirizzo di memoria perch� � un vettore(ma pu� essere anche un oggetto) e non un valore primitivo
		
		MetodiInputOutput metodi= new MetodiInputOutput();   //istanza (crea un riferimento v per poter utilizzare un istanza nuova e cio� un metodo presente in un'altra classe)
		//VARIABILI DICHIARATE
		String mioSaluto, risposta;
		int a, b;
		double dividendo, divisore;
		//---
		saluto("Hello World");
		System.out.println(divisione(10,5));
		System.out.println(somma(23,45));
		//FUNZIONE NELLA FUNZIONE
		System.out.println(somma(somma(5,(int)divisione(10,3)),somma(20,30)));   //CUSTA UN DOUBLE IN INTERO
		System.out.println((int)divisione(10,3));
		//---
		do {
			mioSaluto = metodi.leggiStringa("Inserisci un saluto:");
			saluto(mioSaluto);
			mioSaluto = metodi.leggiStringa("Inserisci un altri saluto:");
			a = metodi.leggiIntero("Primo valore:");
			b = metodi.leggiIntero("Secondo valore:");
			System.out.println("Somma: "+somma(a,b));
			dividendo = metodi.leggiDecimale("Dividendo:");
			divisore = metodi.leggiDecimale("Divisore:");
			System.out.println("Divisione: "+divisione(dividendo,divisore));
			risposta= metodi.leggiStringa("Vuoi continuare?");
			//} while(risposta.equals("si") || risposta.equals("si"));                        //se risponde si torner� alla condizione sopra dove c'� il do
		} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));       //elimina la distinzione tra maiuscolo e minuscolo

	}

	//---METODO VOID (RICEVE ARGOMENTI MA NON RESTITUISCE NULLA, SOLO UNA STAMPA)
	public static void saluto(String saluto) {
		System.out.println(saluto);
	}

	//---METODO MATEMATICO (RICEVE ARGOMENTI E RESTITUISCE UN RETURN CHE VA STAMPATO O ASSEGNATO AD UNA VARIABILE. IN QUEST'ULTIMO CASO res E QUINDI STAMPATA NEL METODO MAIN)
	public static double divisione(double dividendo, double divisore) {
		double res;
		res = dividendo/divisore;
		return res;
	}

	public static int somma(int a, int b) {
		int risultato;
		risultato = a+b;
		return risultato;
	}
}
