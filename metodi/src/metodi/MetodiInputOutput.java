package metodi;
import java.util.Scanner;

public class MetodiInputOutput {

	public String leggiStringa(String msg) {      //restituisce un valore di tipo stringa
		Scanner input=new Scanner(System.in);     //per poter usare il nextln //System.in = leggere dalla tastiera
		System.out.print(msg);                    //-ln per non andare accapo
		return input.nextLine();				  //leggi dalla tastiera
	}

	public int leggiIntero(String msg) {              //restituisce un valore di tipo stringa
		Scanner input=new Scanner(System.in);         //per poter usare il nextln //System.in = leggere dalla tastiera
		System.out.print(msg);                        //-ln per non andare accapo
		return Integer.parseInt(input.nextLine());    //leggi dalla tastiera e converti numero stringa in numero intero
	}

	public double leggiDecimale(String msg) {            //restituisce un valore di tipo stringa
		Scanner input=new Scanner(System.in);            //per poter usare il nextln //System.in = leggere dalla tastiera
		System.out.print(msg);                           //-ln per non andare accapo
		return Double.parseDouble(input.nextLine());     //leggi dalla tastiera e converti numero stringa in numero decimale
	}
}
