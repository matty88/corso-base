package metodi;
//PASSAGGIO PER VALORE
//AVVIENE PER I VALORI PRIMITIVI AI QUALI VIENE ASSEGNATO DIRETTAMENTE UN CONTENUTO

//PASSAGGIO PER RIFERIMENTO
//ASSEGNIAMO L'INDIRIZZO DI MEMORIA PERCHE' UN VETTORE O UN OGGETTO NON E' UN VALORE PRIMITIVO


public class PassaggioPerValoreRiferimento {

	public double divisioneConValore(double dividendo, double divisore) {
		return dividendo/divisore;
	}

	public double divisioneConRiferimento(double[] decimali) {
		return decimali[0]/decimali[1];
	}
}
