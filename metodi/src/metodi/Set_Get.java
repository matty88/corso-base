package metodi;

public class Set_Get {
	private int id;

	//ASSEGNANO O LEGGONO UN VALORE
	//set = ASSEGNA (void)
	//get = LEGGI(DI SOLITO SENZA PARAMETRI IN INGRESSO) (string)
	public void setId(int id) {
		this.id = id;       //THIS PREFISSO DELL'ATTRIBUTO
		}
	
	public int getId() {
		return id;           //IL THIS SI PUO' EVITARE DI METTERLO 
		}

}
