package esCinque;
import es.Metodi;

public class esCinque {

	public static void main(String[] args) {
		String risposta;
		do {
			modulo();
			Metodi metodi = new Metodi();
			risposta = metodi.leggiStringa("Vuoi ripetere l'inserimento?:");
		} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));
	}

	public static void modulo() {
		Metodi metodi = new Metodi();
		int valore = metodi.leggiIntero("Dai un valore:");
		if(valore%2==0) {
			System.out.println("il numero "+ valore + " � pari");
		} else {
			System.out.println("il numero "+ valore + " � dispari");
		}
	}

}
