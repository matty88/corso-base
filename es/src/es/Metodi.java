package es;
import java.util.Scanner;

public class Metodi {

	public int leggiIntero(String num) {
		Scanner input=new Scanner(System.in);
		System.out.print(num);
		return Integer.parseInt(input.nextLine());       
	}

	public String leggiStringa(String msg) {
		Scanner input=new Scanner(System.in);
		System.out.print(msg);
		return input.nextLine();       
	}
}