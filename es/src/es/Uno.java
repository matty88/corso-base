package es;

public class Uno {

	public static void main(String[] args) {
		Metodi metodi = new Metodi();
		int a = metodi.leggiIntero("Dai un valore alla variabile a:");
		int b = metodi.leggiIntero("Dai un valore alla variabile b:");
		//swap
		int temp = a;
		System.out.println("***PRIMA DELLO SWAP***");
		System.out.println("La variabile a � uguale: "+a);
		System.out.println("La variabile b � uguale: "+b);
		a = b;
		b = temp;
		System.out.println("***DOPO LO SWAP***");
		System.out.println("La variabile a � uguale "+a);
		System.out.println("La variabile b � uguale "+b);
	}
}
