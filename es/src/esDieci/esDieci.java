package esDieci;
import es.Metodi;

public class esDieci {

	public static void main(String[] args) {
		int somma = 0; int contPari = 0; int contDispari = 0; int sommaPari = 0; int sommaDispari = 0; int valoreMin = 0; int valoreMax = 0;
		int[][] matrice = new int[3][5];
		Metodi metodi = new Metodi();
		for(int y=0;y<matrice[0].length;y++) {
			for(int x=0;x<matrice.length;x++) {
				matrice[x][y]=metodi.leggiIntero("Inserisci valore temperatura: matrice["+(x+1)+"]["+(y+1)+"]:");
				somma += matrice[x][y];
				if(matrice[x][y]%2==0) {
					contPari++;
					sommaPari += matrice[x][y];
				} else {
					contDispari++;
					sommaDispari += matrice[x][y];
				}
				if(x==0 && y==0) {
					valoreMin=matrice[x][y];
					valoreMax=matrice[x][y];
				} else {
					if(matrice[x][y]>=valoreMax) {
						valoreMax=matrice[x][y];
					}
					if (matrice[x][y]<=valoreMin) {
						valoreMin=matrice[x][y]; 
					}
				}
			}	
		}
		System.out.println("La somma di tutti i valori �: "+somma);
		System.out.println("Il vettore ha "+contPari+" numeri pari e "+contDispari+" numeri dispari");
		System.out.println("La somma dei numeri pari � "+sommaPari);
		System.out.println("La somma dei numeri dispari � "+sommaDispari);
		System.out.println("Il valore massimo � "+valoreMax);
		System.out.println("Il valore minimo � "+valoreMin);
	}
}
