package esQuattro;
import es.Metodi;

public class esQuattro {

	public static void main(String[] args) {
		int valoreMax, somma = 0;
		Metodi metodi = new Metodi();
		int[] vett = new int[10];
		vett[0]= metodi.leggiIntero("Dai un numero alla variabile " + vett[0] + " del vettore:");
		for(int index=1; index<=9; index++) {
			vett[index] = metodi.leggiIntero("Dai un numero alla variabile " + index + " del vettore:");
			if (vett[index] > vett[index-1]) {
				valoreMax = vett[index];
				System.out.println("Il valore successivo � maggiore del precedente");
			} else {
				valoreMax = vett[index-1];
				System.out.println("Il valore successivo � minore del precedente");
			}
			somma = somma+valoreMax;
		}
		System.out.println("***SOMMA***");
		System.out.println(somma);
	}
}


