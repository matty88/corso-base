package esQuattro;
import es.Metodi;

public class esQuattro2 {

	public static void main(String[] args) {
		int somma = 0;
		Metodi metodi = new Metodi();
		int[] vett = new int[10];
		vett[0]= metodi.leggiIntero("Dai un numero alla variabile 1 del vettore:");
		int valoreMax = vett[0];
		for(int index=1; index<=9; index++) {
			vett[index] = metodi.leggiIntero("Dai un numero alla variabile " + (index+1) + " del vettore:");
			if (vett[index] > vett[index-1]) {
				System.out.println("Il valore successivo � maggiore del precedente");
			} else if (vett[index] < vett[index-1]){
				System.out.println("Il valore successivo � minore del precedente");
			} else {
				System.out.println("Il valore successivo � uguale al precedente");
			}
			if(vett[index]>valoreMax) {
				valoreMax=vett[index];
			}
		}
		System.out.println("Il valore massimo del vettore � "+valoreMax);
	}
}


