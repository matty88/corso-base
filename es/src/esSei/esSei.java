package esSei;
import es.Metodi;

public class esSei {

	public static void main(String[] args) {
		String risposta;
		do {
			pariDispari();
			Metodi metodi = new Metodi();
			risposta = metodi.leggiStringa("Vuoi ripetere l'inserimento?:");
		} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));
	}

	public static void pariDispari () {
		int[] vett = new int[10];
		int contPari = 0, contDispari = 0, sommaPari = 0, sommaDispari = 0;
		Metodi metodi = new Metodi();
		for(int index=0; index<=9; index++) {
			vett[index] = metodi.leggiIntero("Dai un numero alla variabile " + (index+1) + " del vettore:");
		}
		for(int index=0; index<=9; index++) {
		if(vett[index]%2==0) {
				contPari++;
				sommaPari += vett[index];
			} else {
				contDispari++;
				sommaDispari += vett[index];
			}
		}
		System.out.println("Il vettore ha "+contPari+" numeri pari.");
		System.out.println("Il vettore ha "+contDispari+" numeri dispari.");
		System.out.println("La somma dei numeri pari � "+sommaPari);
		System.out.println("La somma dei numeri dispari � "+sommaDispari);
	}
}

