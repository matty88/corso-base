package esTre;
import es.Metodi;

public class esTre2 {

	public static void main(String[] args) {
		Metodi metodi = new Metodi();
		int nuovoValore;
		int valorePrecedente = metodi.leggiIntero("1) Inserisci un valore:");
		int valoreMax = valorePrecedente;
		for(int index=1; index <10; index++) {
			nuovoValore = metodi.leggiIntero(index+1 +")Inserisci valore:");
			if(nuovoValore>valorePrecedente) {
				System.out.println("Il valore inserito � maggiore del precedente");
				valoreMax=nuovoValore;
			} else if (nuovoValore<valorePrecedente) {
				System.out.println("Il valore inserito � minore del precedente");
			} else {
				System.out.println("Il valore inserito � uguale a quello precedente");
			}
			if(nuovoValore>valoreMax) {
		       valoreMax=nuovoValore;
			}
			valorePrecedente=nuovoValore;
		}
		System.out.println("Il valore massimo � "+valoreMax);
	}
}
