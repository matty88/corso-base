package it.tore.logic.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.tore.logic.jpa.entity.Account;
import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.jpa.entity.Progetto;
import it.tore.logic.jpa.entity.Ruolo;
import it.tore.logic.jpa.repository.RepoAccount;
import it.tore.logic.jpa.repository.RepoDipendente;
import it.tore.logic.jpa.repository.RepoProgetto;
import it.tore.logic.jpa.repository.RepoRuolo;
import it.tore.logic.service.interfaces.ServiceDip;


@Service
public class ServiceImplDip implements ServiceDip{

	@Autowired 
	private RepoDipendente repoDipendente;
	@Autowired 
	private RepoAccount repoAccount;
	@Autowired 
	private RepoRuolo repoRuolo;
	@Autowired 
	private RepoProgetto repoProgetto;
	

	//Metodi di Inserimento
	@Override
	public void insertD(Dipendente d) {
		System.out.println(d);
		repoDipendente.save(d);
		
	}
	
	@Override
	public void insertD(List<Dipendente> d) {
		System.out.println(d);
		repoDipendente.saveAll(d);
		
	}
	
	@Override
	public void insertP(Progetto p) {
		repoProgetto.save(p);
		
	}


	
	//Metodi di Modifica
	@Override
	public void modificaD(Dipendente d) {
		repoDipendente.save(d);
	}
	//Metodi di Eliminazione
	@Override
	public void deleteD(Dipendente d) {
          repoDipendente.delete(d);		
	}
	public void deleteP(Progetto p) {
		repoProgetto.delete(p);
	}
	public void deletePJpl() {

		repoProgetto.deletebyJPLproject();
		}
	//Metodi di Ricerca
	@Override
	
    public Dipendente getDip(int id) {
		return repoDipendente.findById(id);
	}

	@Override
	public Dipendente getDip(String cf) {
		return repoDipendente.findBycf(cf);
	}

	@Override
	public Account getAcc(int id) {
		return repoAccount.findById(id);
	}

	@Override
	public Ruolo getRu(int id) {
		return repoRuolo.findByid(id);
	}


	@Override
	public Ruolo getRu(String ruolo) {
		return repoRuolo.findByruolo(ruolo);
	}


	@Override
	public Account getAcc(String user) {
		return repoAccount.findByuser(user);
	}

	@Override
	public List<Dipendente> getListDip(Double stipendio) {
		return repoDipendente.findbyJPLstipendio(stipendio);
	}

	@Override
	public List<Dipendente> getListDip(String comune) {
		return repoDipendente.findbyJPLcomune(comune);
	}
	
	@Override
	public List<Dipendente> getListDip(String nome,String cognome) {
		return repoDipendente.findJPLnomeANDcognome(nome,cognome);
	}
	
	@Override
	public List<Dipendente> getAllDip() {
		return repoDipendente.findAll();
	}
	@Override
	public List<Dipendente> getAllDip(String progetto) {
		return repoDipendente.findbyJPLproject(progetto);
	}
	

	@Override
	public List<Account> getListAccR(int id_ruolo) {
		return repoAccount.findbyJPLruolo(id_ruolo);
	}

	@Override
	public Account getListAcc(int id_dip) {
		return repoAccount.findbyJPLdip(id_dip);
	}

	@Override
	public List<Dipendente> getListDip() {
		return repoDipendente.findbyJPLproject();
	}

	public Progetto getProject(int id) {
		return repoProgetto.findByid(id);
	}
	public Progetto getProject(String progetto) {

		return repoProgetto.findByprogetto(progetto);
	}
	public List<Progetto> getListProject(){
		return repoProgetto.findbyJPLproject(); 
	};
	
	public List<Progetto> getAllProject(){
		return repoProgetto.findAll();
	};



}
