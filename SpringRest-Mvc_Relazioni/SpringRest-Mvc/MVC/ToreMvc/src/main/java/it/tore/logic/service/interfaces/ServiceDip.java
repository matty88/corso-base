package it.tore.logic.service.interfaces;


import java.util.List;

import it.tore.logic.jpa.entity.Account;
import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.jpa.entity.Progetto;
import it.tore.logic.jpa.entity.Ruolo;

public interface ServiceDip {
	//Metodi di Inserimento
	void insertD(Dipendente d);
	void insertD(List<Dipendente> listd);
	void insertP(Progetto p);
	
	//Metodi di Modifica
	void modificaD(Dipendente d);
	
	//Metodi di Eliminazione
	void deleteD(Dipendente d);
	void deleteP(Progetto p);
	void deletePJpl();
	
	//Metodi di Ricerca
	Dipendente getDip(int id);
	
	Dipendente getDip(String cf);
	
	List<Dipendente> getListDip(Double stipendio);
	
	List<Dipendente> getListDip(String comune);
	
	List<Dipendente> getListDip();
	
	List<Dipendente> getListDip(String nome,String cognome);
	
	List<Dipendente> getAllDip();
	
	List<Dipendente> getAllDip(String Progetto);
	
	Account getAcc(int id);
	
	Account getAcc(String user);
	
    List<Account> getListAccR(int id_ruolo);
	
	Account getListAcc(int id_dip);
	
	Ruolo getRu(int id);

	Ruolo getRu(String ruolo);
	
	Progetto getProject(int id);
	
	Progetto getProject(String progetto);
	
	List<Progetto> getListProject();
	
	List<Progetto> getAllProject();

}
