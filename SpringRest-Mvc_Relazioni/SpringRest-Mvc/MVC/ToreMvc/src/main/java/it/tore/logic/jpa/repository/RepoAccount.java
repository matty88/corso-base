package it.tore.logic.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.tore.logic.jpa.entity.Account;

public interface RepoAccount extends JpaRepository<Account, Integer>{
	Account findById(int id);
	Account findByuser(String user);
	
	@Query("from Account where id_ruolo=?1")
	List<Account> findbyJPLruolo(int id_ruolo);
	
	@Query("from Account where id_dip=?1")
	Account findbyJPLdip(int id_dip);

}
