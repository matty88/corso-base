package it.tore.logic.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.tore.logic.jpa.entity.Account;
import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.jpa.entity.Progetto;
import it.tore.logic.jpa.entity.Ruolo;
import it.tore.logic.jpa.repository.RepoDipendente;
import it.tore.logic.service.interfaces.ServiceDip;

@Controller
@RequestMapping({"/insertCtrl"})
public class InsertController {
	
	@Autowired
	private ServiceDip serviceDip;
   	
	//inserimento dipendente e account 
	@RequestMapping(value="insertD", method=RequestMethod.POST)
	public String insertDip(@RequestParam String nome,
			@RequestParam String cognome,
			@RequestParam String cf,
			@RequestParam String comune,
			@RequestParam String sesso,
			@RequestParam String data_di_nascita,
			@RequestParam String stipendio,@RequestParam String user,
			@RequestParam String password,@RequestParam String ruolo, ModelMap model) {
	
	    Dipendente d=new Dipendente();
	    d.setNome(nome);
	    d.setCognome(cognome);
	    d.setcf(cf);
	    d.setComune(comune);
	    d.setSesso(sesso);
	    d.setData_di_nascita(LocalDate.parse(data_di_nascita));
	    d.setStipendio(Double.parseDouble(stipendio));
	    Ruolo r=serviceDip.getRu(ruolo);
	    Account a=new Account();
	    a.setUser(user);
	    a.setPassword(password);
	    a.setRuolo(r);
	    a.setDipendente(d);
	    d.setAccount(a);
	    serviceDip.insertD(d);
	    model.addAttribute("messaggio", "dipendente registrato");

	    ;
	   
		return "index";
	}
	//inserimento progetto
	@RequestMapping(value="insertP",  method=RequestMethod.POST)
	public String insertP(@RequestParam String progetto, ModelMap model) {
	
	    Progetto p=new Progetto();
	    p.setProgetto(progetto);
	    model.addAttribute("messaggio", "progetto non registrato");
	    serviceDip.insertP(p);
	    ;
	   
		return "index";
	}
	//inserimento lista dipendenti associata ad un progetto nella tabella di cross
	@RequestMapping(value="insertDtoP",  method=RequestMethod.POST)
	public String insertDtoP(@RequestParam Map<String,String> allParams, ModelMap model) {
		List<Progetto> listP=new ArrayList<Progetto>();
		
		String progetto=allParams.get("progetto");
		List<Dipendente> listDip=serviceDip.getAllDip(progetto);
		Progetto p=serviceDip.getProject(progetto);
		listP.add(p);
		int iVar=Integer.parseInt(allParams.get("iVar"));
		for(int i=1; i<iVar; i++) {
			String radioDip=allParams.get("radioDip"+i);			
			if(radioDip.equalsIgnoreCase("Y")) {
				int iDip=Integer.parseInt(allParams.get("dip"+i));
				Dipendente d=serviceDip.getDip(iDip);
				//d.setList(listP);
				listDip.add(d);
			}
		}
		p.setList(listDip);
		serviceDip.insertP(p);
		//serviceDip.insertD(listDip);
		
	   
		return "index";
	}
	

}
