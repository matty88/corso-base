package it.tore.logic.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NavigationController {
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String index(HttpServletRequest request, ModelMap model) {
		return "index";
	}
	@RequestMapping(value="/form",method=RequestMethod.GET)
	public String form(HttpServletRequest request, ModelMap model) {
		return "form";
	}
	
	@RequestMapping(value="/NewFile", method=RequestMethod.GET)
	public String NewFile(HttpServletRequest request, ModelMap model) {
		return "NewFile";
	}
}
