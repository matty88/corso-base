package it.tore.logic.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="progetti")
public class Progetto {
	
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name="id", nullable=false)
private int id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

@Column(name="progetto", nullable=false)
private String progetto;
public String getProgetto() {
	return progetto;
}
public void setProgetto(String progetto) {
	this.progetto = progetto;
}

@ManyToMany
@JoinTable(name="dettaglioProgetti",
joinColumns=@JoinColumn(name="id_progetto"),
inverseJoinColumns=@JoinColumn(name="id_dip"))
private List<Dipendente> listD= new ArrayList<Dipendente>();

public List<Dipendente> getList() {
	return listD;
}
public void setList(List<Dipendente> listP) {
	this.listD = listP;
}
public Progetto() {}
public Progetto(String progetto) {
	super();
	this.progetto = progetto;
}
@Override
public String toString() {
	return "Progetto [id=" + id + ", progetto=" + progetto + ", list=" + listD + "]";
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + id;
	result = prime * result + ((listD == null) ? 0 : listD.hashCode());
	result = prime * result + ((progetto == null) ? 0 : progetto.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Progetto other = (Progetto) obj;
	if (id != other.id)
		return false;
	if (listD == null) {
		if (other.listD != null)
			return false;
	} else if (!listD.equals(other.listD))
		return false;
	if (progetto == null) {
		if (other.progetto != null)
			return false;
	} else if (!progetto.equals(other.progetto))
		return false;
	return true;
}

}
