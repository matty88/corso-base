package it.tore.logic.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import it.tore.logic.jpa.entity.Progetto;

public interface RepoProgetto extends JpaRepository<Progetto, Integer> {
	
	Progetto findByid(int id);
	Progetto findByprogetto(String progetto);
	@Query("FROM Progetto p JOIN p.listD d WHERE d.id = 42")
	List<Progetto> findbyJPLproject();
	@Transactional
	@Modifying
	@Query("DELETE FROM Progetto p WHERE p.id = 6")
	void deletebyJPLproject();


}
