package it.tore.logic.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.tore.logic.jpa.entity.Account;
import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.jpa.entity.Progetto;
import it.tore.logic.jpa.entity.Ruolo;
import it.tore.logic.service.interfaces.ServiceDip;

@Controller
@RequestMapping({"/viewController"})
public class ViewController {
	@Autowired
	private ServiceDip serviceDip;
	//Ritorna la pagina visualiiza che cicla la lista dipendenti popolata dalla select all
	@RequestMapping(value="mainView", method=RequestMethod.POST)
	public String mainView(ModelMap model) {
		List<Dipendente> num=serviceDip.getAllDip();
        
		model.addAttribute("num", num);
		return "visualizza";
	}
	
	//Cerca il dipendente da modificare e lo setta nella pagina formModifica
	@RequestMapping(value="settaDip", method=RequestMethod.POST)
	public String settaDip(@RequestParam String cfEdit, ModelMap model) {
		Dipendente num=serviceDip.getDip(cfEdit);
        Account a=num.getAccount();
        Ruolo r=a.getRuolo();
		model.addAttribute("num", num);
		model.addAttribute("a", a);
		model.addAttribute("r", r);

		return "formModifica";
	}
	
	//fa la select all dei progetti e dei dipendenti che vengono ciclati nella pagina formDipToProgetto
	@RequestMapping(value="settaDipToProject", method=RequestMethod.POST)
	public String settaDipToProject(ModelMap model) {
		
		
		List<Progetto> listProject=serviceDip.getAllProject();
		List<Dipendente> listDip=serviceDip.getAllDip();
		model.addAttribute("listP", listProject);
		model.addAttribute("listDip", listDip);



		return "formDipToProgetto";
	}
	//Ricerca dipendenti per nome e cognome
	@RequestMapping(value="searchByNC", method=RequestMethod.POST)
	public String searchByNC(@RequestParam String searchN,@RequestParam String searchC,ModelMap model) {
		List<Dipendente> num=new ArrayList<Dipendente>();
		num=serviceDip.getListDip(searchN,searchC);
			model.addAttribute("num", num);
		return "visualizza";

	}
	//Ricerca dipendente per codice fiscale
	@RequestMapping(value="searchBycf", method=RequestMethod.POST)
	public String searchBycf(@RequestParam String searchCf,ModelMap model) {
		List<Dipendente> num=new ArrayList<Dipendente>();		
		 Dipendente d=serviceDip.getDip(searchCf);
			num.add(d);
			model.addAttribute("num", num);
			
		
		return "visualizza";

	}

}
