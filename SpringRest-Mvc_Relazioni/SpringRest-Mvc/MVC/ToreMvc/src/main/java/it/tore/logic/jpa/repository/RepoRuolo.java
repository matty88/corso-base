package it.tore.logic.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.tore.logic.jpa.entity.Ruolo;

public interface RepoRuolo extends JpaRepository<Ruolo, Integer>{
   Ruolo findByid(int id);
   Ruolo findByruolo(String ruolo);
}
