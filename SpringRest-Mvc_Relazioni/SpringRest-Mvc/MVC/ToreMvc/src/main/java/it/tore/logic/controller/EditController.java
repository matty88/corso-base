package it.tore.logic.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.tore.logic.jpa.entity.Account;
import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.jpa.entity.Ruolo;
import it.tore.logic.service.interfaces.ServiceDip;

@Controller
@RequestMapping({"/editCtrl"})
public class EditController {
	@Autowired
	private ServiceDip serviceDip;
   	
	//modifica dipendente ed account
	@RequestMapping(value="editD", method=RequestMethod.POST)
	public String insertDip(@RequestParam String id,String ida,String nome,
			@RequestParam String cognome,
			@RequestParam String cf,
			@RequestParam String comune,
			@RequestParam String sesso,
			@RequestParam String data_di_nascita,
			@RequestParam String stipendio,@RequestParam String user,
			@RequestParam String password,@RequestParam String ruolo, ModelMap model) {
	
	    Dipendente d=new Dipendente();
	    d.setId(Integer.parseInt(id));
	    d.setNome(nome);
	    d.setCognome(cognome);
	    d.setcf(cf);
	    d.setComune(comune);
	    d.setSesso(sesso);
	    d.setData_di_nascita(LocalDate.parse(data_di_nascita));
	    d.setStipendio(Double.parseDouble(stipendio));
	    Ruolo r=serviceDip.getRu(ruolo);
	    Account a=new Account();
	    a.setId(Integer.parseInt(ida));
	    a.setUser(user);
	    a.setPassword(password);
	    a.setRuolo(r);
	    //a.setDipendente(d);
	    d.setAccount(a);
	    serviceDip.insertD(d);
	    model.addAttribute("messaggio", "dipendente modificato");

	    ;
	   
		return "index";
	}

}
