package it.tore.logic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.tore.logic.jpa.entity.Dipendente;
import it.tore.logic.service.interfaces.ServiceDip;

@Controller
@RequestMapping({"/deleteCtrl"})
public class DeleteController {

	@Autowired
	private ServiceDip serviceDip;
	//elimina dipendente,e tutti i record associati nelle altre tabelle
	@RequestMapping(value="deleteD", method=RequestMethod.POST)
	public String deleteD(@RequestParam String cfDelete, ModelMap model) {
		model.addAttribute("messaggio","ciao");
		Dipendente d=serviceDip.getDip(cfDelete);
		
		System.out.println(d);
		serviceDip.deleteD(d);
		return "index";
	}
}
