package it.tore.logic.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name="ruoli")
public class Ruolo {

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="id", nullable=false)
private int id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

@Column(name="ruolo", nullable=false,unique=true)
private String ruolo;
public String getRuolo() {
	return ruolo;
}
public void setRuolo(String ruolo) {
	this.ruolo = ruolo;
}


@OneToMany(fetch=FetchType.LAZY, mappedBy="ruolo",cascade= CascadeType.ALL)
private List<Account> account= new ArrayList<Account>();
public List<Account> getAccount() {
	return account;
}

public void setAccount(List<Account> account) {
	this.account = account;
}

public Ruolo() {}
public Ruolo(String ruolo) {
	super();
	this.ruolo = ruolo;
}
@Override
public String toString() {
	return "Ruolo [ruolo=" + ruolo + "]";
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((account == null) ? 0 : account.hashCode());
	result = prime * result + id;
	result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Ruolo other = (Ruolo) obj;
	if (account == null) {
		if (other.account != null)
			return false;
	} else if (!account.equals(other.account))
		return false;
	
	if (id != other.id)
		return false;
	if (ruolo == null) {
		if (other.ruolo != null)
			return false;
	} else if (!ruolo.equals(other.ruolo))
		return false;
	return true;
}



}
