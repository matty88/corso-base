 <%@ include file="/static/header.jspf" %>
<!-- Main-->
<div class="main">
	<!--Form-->


<div class="row paddingrow">
		<div class="col-md-12 col-sm-12 float-left">
		
<div class="row paddingrow" style="background-color:#d6d6d6">
<div class="col-md-2 col-sm-6 float-left"><h4 class="">Nome</h4></div>
						<div class="col-md-2 col-sm-6 float-left"><h4 class="">Cognome</h4></div>
						<div class="col-md-2 offset-md-1 col-sm-6 float-left"><h4 class="">Mail</h4></div>
						
					<div class="col-md-5 col-sm-6 float-left">
						
						<ul id="topheader" class="nav nav-tabs navCustom">
 
  <li class="nav-item">
    <a class="nav-link active" href="#nm" id="liNM">Nome e cognome</a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link" href="#cf" id="liCF">Codice fiscale</a>
  </li>
  

</ul>
<div id="inptNM" style="display:block">
<form action="searchByNC" method="POST">
<button class="btn btn-danger float-right" type="submit">Ricerca</button>
<input name="searchN" type="search" class="form-control" style="width:60%"
								value="" placeholder="nome">
<input name="searchC" type="search" class="form-control" style="width:60%"
								value="" placeholder="cognome">								
<input name="sceltaS" type="hidden"  value="1">								
</form>
</div>
<div id="inptCF" style="display:none">
<form action="searchBycf" method="POST">
<button class="btn btn-danger float-right" type="submit">Ricerca</button>								
<input name="searchCf" type="search" class="form-control" style="width:60%"
								value="" placeholder="codice fiscale">
<input name="sceltaS" type="hidden"  value="2">								

								</form>	</div>
															
</div>
				
</div>
			<c:forEach var="map" items="${num}">
				<div class="borderT">
					<div class="row paddingrow" style="border-top: solid blue;">
						<div class="col-md-2 col-sm-6 float-left">${map.nome}</div>
						<div class="col-md-2 col-sm-6 float-left">${map.cognome}</div>
						<div class="col-md-4 col-sm-6 float-left">${map.cf}</div>
						<form name="edit" action="settaDip" method="POST">
						<input name="cfEdit" type="hidden" class="form-control"
								value="${map.cf}">
							<div class="col-md-2 col-sm-6 float-left">
								<button type="submit" class="btn btn-primary">Edit</button>
							</div>
						</form>

						<form name="delete" action="/logic/deleteCtrl/deleteD" method="POST">
							<input name="cfDelete" type="hidden" class="form-control"
								value="${map.cf}">
							<div class="col-md-2 col-sm-6 float-left">
								<button type="submit" class="btn btn-danger">Delete</button>
							</div>
						</form>
						<br>
					</div>



				</div>
			</c:forEach>

			<div class="row rowpadding">
				<div class="offset-md-3 col-md-6 col-sm-12 float-left">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li style="width:30%" class="page-item"><a class="page-link" href="http://localhost:8080/CrudServlet/Paginazione?attr=1">Prima
									Pag.</a></li>
							<li style="width:20%" class="page-item"><a class="page-link" href="http://localhost:8080/CrudServlet/Paginazione?attr=2"><i
									class="fas fa-angle-left"></i></a></li>
							<li style="width:20%" class="page-item"><a class="page-link" href="http://localhost:8080/CrudServlet/Paginazione?attr=3"><i
									class="fas fa-angle-right"></i></a></li>
							<li style="width:30%" class="page-item"><a class="page-link" href="http://localhost:8080/CrudServlet/Paginazione?attr=4">Ultima
									Pag.</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="row rowpadding">
				<div class="offset-md-3 col-md-6 col-sm-12 float-left">${pag}</div>
			</div>






		</div>

	</div>


</div>
<!--Fine Main-->

<!--Footer -->

 <%@ include file="/static/footer.jspf" %>
