<%@ include file="/static/header.jspf" %>
        <!-- Main-->
    <div class="main">
                <!--Form-->

      <form name="registrazione" method="POST" action="/logic/editCtrl/editD" class="needs-validation" onsubmit="" novalidate>
        <input value="${num.id}" name="id" type="hidden">
        <input value="${a.id}" name="ida" type="hidden">
        <div class="row paddingrow">
          <!--Nome-->
          <div class="col-md-6 col-sm-12 mb-5">
            
              
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Nome</span>
                    </div>
                    <input value="${num.nome}" onblur="validN(this)" name="nome" type="text" class="form-control" id="validaNome" placeholder="Inserisci Nome" aria-describedby="validationTooltipNome">
                    
                    <div class="errore" id="errorenome">
                      <p>Inserisci almeno 3 caratteri</p>
                    </div>
                    <div class="successo" id="successonome">
                        <i class="fas fa-check fa-2x"></i>
                    </div>
                  </div>
          </div>
                    <!--Cognome-->

          <div class="col-md-6 col-sm-12 mb-5">
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Cognome</span>
                  </div>
                  <input value="${num.cognome}" name="cognome" onblur="validC(this)" type="text" class="form-control" id="validaCognome" placeholder="Inserisci Cognome">
                  <div class="invalid-tooltip">
                    Please choose a unique and valid username.
                  </div>
                      <div class="errore" id="errorecognome">
                          <p>Inserisci almeno 3 caratteri</p>
                      </div>
                      <div class="successo" id="successocognome">
                          <i class="fas fa-check fa-2x"></i>
                      </div>
                  

                </div>
            
          </div>
              <!--CF-->
          <div class="col-md-6 col-sm-12 mb-5">
            
              <div class="col-md-7 float-left col-sm-4" style="padding-left:0">
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="validationTooltipCF">CF</span>
                  </div>
                  <input value="${num.cf}" onblur="validCf(this)" name="cf" type="text" class="form-control" id="validaCf" placeholder="Codice fiscale">
                  <div class="errore" id="errorecf">
                          <p>Codice fiscale non valido</p>
                      </div>
                      <div class="successo" id="successocf">
                          <i class="fas fa-check fa-2x"></i>
                      </div>
                </div>
              </div>
              <div class="col-md-5 float-left col-sm-3">
                  <input value="${num.comune}" name="comune" type="text" class="form-control" id="validationTooltipComune" placeholder="Comune">
              </div>
            
        </div>
                  <!--Stipendio-->
        <div class="col-md-6 col-sm-12 mb-5">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="validationTooltipNazione">Stipendio</span>
                </div>
                <input value="${num.stipendio}" name="stipendio" type="number" class="form-control" id="" placeholder="Inserisci Stipendio">
                <div class="invalid-tooltip">
                  Please choose a unique and valid username.
                </div>
              </div>
          
        </div>
                          <!--Sesso-->
        <div class="col-md-6 col-sm-12 mb-5">
            
              
             
            <c:if test = "${num.sesso == 'M'}">
            <c:set var="sessoM" value="checked"/>
            <c:set var="sessoF" value=""/>
            </c:if>
            <c:if test = "${num.sesso == 'F'}">
            <c:set var="sessoM" value=""/>
            <c:set var="sessoF" value="checked"/>
            </c:if>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Sesso</span>
                </div>
                <span class="input-group-text ml-3">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="sesso" value="M" class="custom-control-input" ${sessoM}>
                    <label class="custom-control-label" for="customRadioInline1">M</label>
                </div></span>
                <span class="input-group-text ml-3">

                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="sesso" value="F" class="custom-control-input" ${sessoF}>
                    <label class="custom-control-label" for="customRadioInline2">F</label>
                  </div></span>
      
            </div>

      </div>
                        
      <div class="col-md-6 col-sm-12 mb-5">
        <div class="col-md-10 float-left  col-sm-4" style="padding-left:0">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Data di nascita</span>
                </div>
                <input value="${num.data_di_nascita}" onblur="okData(this)" name="data_di_nascita" type="date" class="form-control"  id="dateN" value="" placeholder="">
                <div id="erroreGG" class="errore">
                  Data non valida
                </div>
              </div>
        </div>
        
      </div>
                              <!--User-->

      <div class="col-md-6 col-sm-12 mb-5">
            
          <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="validationTooltipUser">User</span>
              </div>
              <input value="${a.user}" name="user" onblur="validU(this)" type="text" class="form-control" id="validaUser" placeholder="Inserisci User">
              <div id="erroreuser" class="errore">
                  User non valido
              </div>
              <div class="successo" id="successouser">
                  <i class="fas fa-check fa-2x"></i>
              </div>
            </div>
    </div>
                                  <!--Ruolo-->
    <div class="col-md-6 col-sm-12 mb-5">
           <div class="form-group">
             <select name="ruolo" class="form-control" id="">
              <option value="${r.ruolo}">Ruolo</option>
              <option value="admin">Admin</option>
              <option value="guest">Guest</option>
             </select>
             </div>            <div id="erroremail" class="errore">
                  Mail non valida
            </div>
            <div class="successo" id="successomail">
                <i class="fas fa-check fa-2x"></i>
            </div>
          
      
    </div>
    <div class="col-md-6 col-sm-12 mb-5">          
        <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Password</span>
            </div>
            <input value="${a.password}" name="password" onblur="validPass()" type="text" class="form-control" id="validaPassword" placeholder="Inserisci Password">
            <div id="errorepassword" class="errore">
                  La password dev'essere almeno 8 caratteri            
            </div>
            <div class="successo" id="successopassword">
                <i class="fas fa-check fa-2x"></i>
            </div>
          </div>
  </div>
  <div class="col-md-6 col-sm-12 mb-5">
      <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Conferma Password</span>
          </div>
          <input value="${a.password}" onblur="validPass()" type="text" class="form-control" id="validaCPassword" placeholder="Conferma Password">
          <div id="errorecpassword" class="errore">
             le password non coincidono </div>
             <div class="successo" id="successocpassword">
                <i class="fas fa-check fa-2x"></i>
            </div>
        </div>
    
  </div>
  <div class="col-md-12">
      <button type="reset"  class="btn btn-light" style="width:40%;margin-bottom:1%">Reset</button>
      <button id="submitvalid" type="submit"  class="btn btn-danger" style="width:100%">Modifica</button>
      <div id="erroreSubmit" class="errore">
        <p>Verifica la correttezza dei campi</p>
      </div>
    </div>


          
      </form>
     
  </div><!--Fine Main-->

  <!--Footer -->    

 <%@ include file="/static/footer.jspf" %>