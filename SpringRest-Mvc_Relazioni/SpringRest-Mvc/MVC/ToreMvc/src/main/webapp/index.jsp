 <%@ include file="/static/header.jspf" %>
                <!-- Carosello-->
         <p style="color:red">${messaggio}</p>

      <div class="row row ml-0 mr-0">
        <div class="col-12">
            <div class="bd-example">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="/logic/img/froome.jpg" class="d-block w-100" alt="...">
                      <div class="carousel-caption d-none d-md-block">
                        <h3>Chris Froome </h3>
                        <p>Vincitore del giro d'Italia 2018.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="/logic/img/tom-dumoulin.jpg" class="d-block w-100" alt="...">
                      <div class="carousel-caption d-none d-md-block">
                        <h3>Tom Domoulin</h3>
                        <p>Vincitore del giro d'Italia 2017.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="/logic/img/nibalimagliarosa.jpg" class="d-block w-100" alt="...">
                      <div class="carousel-caption d-none d-md-block">
                        <h3>Vincenzo Nibali</h3>
                        <p>Vincitore del giro d'Italia 2016.</p>
                      </div>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>

        </div>
      </div>
    </div>
        <!-- Main-->
    <div class="main">
      <!-- Benvenuti al giro-->
      <div class="row paddingrow">
        <div id="benvenuti" class="offset-lg-2 col-lg-8">
             <h2>Benvenuti al Giro d'Italia 2019</h2>
             <p>
                E' stato presentato a Milano il percorso del Giro d�Italia 2019. La 102ma edizione della Corsa Rosa vedr� i corridori sfidarsi attraverso 21 tappe sulle strade del Bel Paese, con partenza da Bologna sabato 11 maggio e arrivo all�Arena di Verona domenica 2 giugno. Ci saranno ben tre cronometro individuali, per un totale di 58,5 km contro il tempo. Un percorso che offrir� poi grandi tapponi di montagna, con i corridori che dovranno scalare salite mitiche come Gavia e Mortirolo. Andiamo quindi a scoprire nel dettaglio il percorso, con l�analisi ai raggi X delle 21 tappe del Giro d�Italia 2019.
             </p>
        </div>
      </div>

    <!-- Form di ricerca-->
    <div class="row paddingrow">
      <div class="offset-lg-2 col-lg-8">
          <div class="col-lg-12 text-center">
              <h4 style="color:red">Ricerca la tappa</h4>
          </div>
        <div class="col-lg-6 float-left z-indexprop">
            <select class="custom-select">
                <option selected>Scegli numero tappa</option>
                <option value="1">Prima tappa</option>
                <option value="2">Seconda tappa</option>
                <option value="3">Terza tappa</option>
                <option value="4">Quarta tappa</option>
              </select>
        </div>
        <div class="col-lg-6 float-left z-indexprop">
            <select class="custom-select">
                <option selected>Scegli citt� di partenza</option>
                <option value="1">Napoli</option>
                <option value="2">Roma</option>
                <option value="3">Palermo</option>
                <option value="3">Catania</option>
              </select>
        </div>
        <div class="col-lg-12 text-center">
            <button id="ricercabtn" type="submit" class="btn btn-dark">Cerca</button>
        </div>
      </div>
    </div>
          <!-- Squadre del giro-->
          <div class="row paddingrow">
              <div class="offset-lg-2 col-lg-8 text-center">
                   <h3>Le squadre del Giro 2019</h3>
              </div>
          </div>
                    <!-- Griglia squadre-->
          <div class="row paddingrow">
              <div class="col-lg-4 col-md-3 col-sm-6 col-12 mb-4">
                   <h5 class="figurtitle">Astana</h5>
                  <figure class="figure text-center">
                      <img src="/logic/img/astana.jpg" class="figure-img img-fluid rounded" alt="Astana">
                      <figcaption class="figure-caption">L'Astana team � una squadra maschile di ciclismo su 
                   strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
                    </figure>
                    <div class="col-lg-12 d-none d-lg-block">
                        <div class="col-lg-3 float-left text-center">
                            <i class="fas fa-trophy text-center"> 10</i>                    
                        </div>
                        <div class="col-lg-3 float-left text-center">
                            <i class="fas fa-bicycle"> 20</i>                    
                        </div>
                        <div class="col-lg-3 float-left text-center">
                            <i class="fas fa-flag"> Ru</i>                  
                        </div>
                        <div class="col-lg-3 float-left text-center">
                            <i class="far fa-calendar-check"></i>                    
                        </div>
                    </div>     
              </div>
              <div class="col-lg-4 col-md-3 col-sm-6 col-12 mb-4">
                  <h5 class="figurtitle">Androni</h5>
                 <figure class="figure text-center">
                     <img src="/logic/img/androni.jpg" class="figure-img img-fluid rounded" alt="Androni">
                     <figcaption class="figure-caption">La Androni Team � una squadra maschile di ciclismo su 
                        strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
                   </figure>
                   <div class="col-lg-12 d-none d-lg-block">
                       <div class="col-lg-3 float-left text-center">
                           <i class="fas fa-trophy text-center"> 10</i>                    
                       </div>
                       <div class="col-lg-3 float-left text-center">
                           <i class="fas fa-bicycle"> 20</i>                    
                       </div>
                       <div class="col-lg-3 float-left text-center">
                           <i class="fas fa-flag"> It</i>                  
                       </div>
                       <div class="col-lg-3 float-left text-center">
                           <i class="far fa-calendar-check"></i>                    
                       </div>
                   </div>     
             </div>
             <div class="col-lg-4 col-md-3 col-sm-6 col-12 mb-4">
                <h5 class="figurtitle">Bahrain-Merida</h5>
               <figure class="figure text-center">
                   <img src="/logic/img/bahrain.jpg" class="figure-img img-fluid rounded" alt="Bahrain-Merida">
                   <figcaption class="figure-caption">La Bahrain-Merida Team � una squadra maschile di ciclismo su 
                      strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
                 </figure>
                 <div class="col-lg-12 d-none d-lg-block">
                     <div class="col-lg-3 float-left text-center">
                         <i class="fas fa-trophy text-center"> 10</i>                    
                     </div>
                     <div class="col-lg-3 float-left text-center">
                         <i class="fas fa-bicycle"> 20</i>                    
                     </div>
                     <div class="col-lg-3 float-left text-center">
                         <i class="fas fa-flag"> Bh</i>                  
                     </div>
                     <div class="col-lg-3 float-left text-center">
                         <i class="far fa-calendar-check"></i>                    
                     </div>
                 </div>     
           </div>
           <div class="col-lg-4 col-md-3 col-sm-6 d-none d-sm-block mb-4">
              <h5 class="figurtitle">Movistar</h5>
             <figure class="figure text-center">
                 <img src="/logic/img/movida.jpg" class="figure-img img-fluid rounded" alt="Movistar">
                 <figcaption class="figure-caption">La Movistar Team � una squadra maschile di ciclismo su 
                   strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
               </figure>
               <div class="col-lg-12 d-none d-lg-block">
                   <div class="col-lg-3 float-left text-center">
                       <i class="fas fa-trophy text-center"> 10</i>                    
                   </div>
                   <div class="col-lg-3 float-left text-center">
                       <i class="fas fa-bicycle"> 20</i>                    
                   </div>
                   <div class="col-lg-3 float-left text-center">
                       <i class="fas fa-flag"> Es</i>                  
                   </div>
                   <div class="col-lg-3 float-left text-center">
                       <i class="far fa-calendar-check"></i>                    
                   </div>
               </div>     
         </div>
         <div class="col-lg-4 d-none d-lg-block">
            <h5 class="figurtitle">Ag2r</h5>
           <figure class="figure text-center">
               <img src="/logic/img/ag2r.jpg" class="figure-img img-fluid rounded" alt="Ag2r">
               <figcaption class="figure-caption">La Ag2r Team � una squadra maschile di ciclismo su 
                 strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
             </figure>
             <div class="col-lg-12 d-none d-lg-block">
                 <div class="col-lg-3 float-left text-center">
                     <i class="fas fa-trophy text-center"> 10</i>                    
                 </div>
                 <div class="col-lg-3 float-left text-center">
                     <i class="fas fa-bicycle"> 20</i>                    
                 </div>
                 <div class="col-lg-3 float-left text-center">
                     <i class="fas fa-flag"> Fr</i>                  
                 </div>
                 <div class="col-lg-3 float-left text-center">
                     <i class="far fa-calendar-check"></i>                    
                 </div>
             </div>     
       </div>
       <div class="col-lg-4 d-none d-lg-block">
          <h5 class="figurtitle">Groupama</h5>
         <figure class="figure text-center">
             <img src="/logic/img/groupama.jpg" class="figure-img img-fluid rounded" alt="Groupama">
             <figcaption class="figure-caption">La Groupama Team � una squadra maschile di ciclismo su 
               strada con licenza UCI World Tour, attiva nel professionismo dal 2007.</figcaption>
           </figure>
           <div class="col-lg-12 d-none d-lg-block">
               <div class="col-lg-3 float-left text-center">
                   <i class="fas fa-trophy text-center"> 10</i>                    
               </div>
               <div class="col-lg-3 float-left text-center">
                   <i class="fas fa-bicycle"> 20</i>                    
               </div>
               <div class="col-lg-3 float-left text-center">
                   <i class="fas fa-flag"> Ru</i>                  
               </div>
               <div class="col-lg-3 float-left text-center">
                   <i class="far fa-calendar-check"></i>                    
               </div>
           </div>     
     </div>          
    </div>
        <!-- Capitani Squadre del giro-->
        <div class="row paddingrow">
          <div class="offset-lg-2 col-lg-8 text-center">
                       <h3>I capitani delle squadre</h3>
          </div>
        </div> 
        <div class="row paddingrow">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <h5 class="figurtitle">Vincenzo Nibali</h5>
               <figure class="figure text-center">
                   <img src="/logic/img/nibali.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption">Vincenzo Nibali � un ciclista su strada italiano che 
                     corre per il team Bahrain-Merida. Professionista dal 2005, � un corridore completo, 
                     in grado di vincere sia le grandi corse a tappe, che le grandi classiche di un giorno.</figcaption>
                 </figure>    
           </div>          
        
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <h5 class="figurtitle">Romain bardet</h5>
               <figure class="figure text-center">
                   <img src="/logic/img/bardet.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption">Romain Bardet � un ciclista su strada francese che corre per il 
                     team AG2R La Mondiale. Scalatore, professionista dal 2012, ha vinto tre tappe al Tour de France, 
                     che ha concluso due volte sul podio.<br> </figcaption>
                 </figure>    
           </div>          
       
            <div class="col-lg-3 col-md-4 col-sm-6 d-none d-sm-block">
                <h5 class="figurtitle">Nairo Quintana</h5>
               <figure class="figure text-center">
                   <img src="/logic/img/quintana.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption">Nairo Alexander Quintana Rojas � un ciclista su strada colombiano 
                     che corre per il team Movistar. Professionista dal 2009, � soprannominato Condor e Nairoman. <br></figcaption>
                 </figure>    
           </div>          
            <div class="col-lg-3 col-sm-6 d-none d-md-none d-lg-block d-sm-block">
                <h5 class="figurtitle">Angel Lopez</h5>
               <figure class="figure text-center">
                   <img src="/logic/img/lopez.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption">Miguel �ngel L�pez Moreno � un ciclista su strada colombiano che corre per il team Astana. Scalatore, nel 2014 si � aggiudicato 
                     la classifica finale del Tour de l'Avenir, la pi� importante corsa a tappe per Under-23.</figcaption>
                 </figure>    
           </div>          
        </div>
          <!-- Il giro intorno al mondo-->
          <div class="row paddingrow">
              <div class="offset-lg-2 col-lg-8 text-center">
                           <h3>Partenze fuori dall'Italia</h3>
              </div>
          </div>
          <div class="row paddingrow">
              <div class="col-lg-2 col-md-4 col-sm-12">
                 <figure class="figure text-center">
                    <img src="/logic/img/amsterdam.jpg" class="figure-img img-fluid rounded" alt="Astana">
                    <figcaption class="figure-caption"><h4>Amsterdam</h4></figcaption>
                  </figure>    
             </div> 
             <div class="col-lg-2 col-md-4 col-sm-12">
                <figure class="figure text-center">
                   <img src="/logic/img/atene.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption"><h4>Atene</h4></figcaption>
                 </figure>    
            </div> 
            <div class="col-lg-2 col-md-4 col-sm-12 d-none d-md-block">
                <figure class="figure text-center">
                   <img src="img/groningen.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption"><h4>Groningen</h4></figcaption>
                 </figure>    
            </div> 
            <div class="col-lg-2 col-md-4 col-sm-12 d-none d-md-block">
                <figure class="figure text-center">
                   <img src="/logic/img/nizza.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption"><h4>Nizza</h4></figcaption>
                 </figure>    
            </div> 
            <div class="col-lg-2 col-md-4 col-sm-12 d-none d-md-block">
                <figure class="figure text-center">
                   <img src="/logic/img/pMonaco.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption"><h4>Monaco</h4></figcaption>
                 </figure>    
            </div> 
            <div class="col-lg-2 col-md-4 col-sm-12 d-none d-md-block">
                <figure class="figure text-center">
                   <img src="/logic/img/verviers.jpg" class="figure-img img-fluid rounded" alt="Astana">
                   <figcaption class="figure-caption"><h4>Verviers</h4></figcaption>
                 </figure>    
            </div> 
              <!--Collapse-->
                       <div class="col-sm-12 text-center d-none d-md-none d-lg-none d-xl-none d-sm-block collapse">
                      <div class="col-sm-12 collapse" id="collapseExample">
                          <figure class="figure text-center">
                             <img src="/logic/img/groningen.jpg" class="figure-img img-fluid rounded" alt="Astana">
                             <figcaption class="figure-caption"><h4>Groningen</h4></figcaption>
                           </figure>    
                      </div> 
                      <div class="col-sm-12 collapse" id="collapseExample" >
                          <figure class="figure text-center">
                             <img src="/logic/img/nizza.jpg" class="figure-img img-fluid rounded" alt="Astana">
                             <figcaption class="figure-caption"><h4>Nizza</h4></figcaption>
                           </figure>    
                      </div> 
                                <!-- Squadre del giro-->
         
              <div class="col-sm-12 text-center">
                   <button type="button" class="btn btn-secondary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Mostra altro</button>
              </div>
            </div>
          
                      

  
          </div> 
  </div><!--Fine Main-->

  <!--Footer -->
   <%@ include file="/static/footer.jspf" %>
  