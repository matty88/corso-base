<%@ include file="static/header.jspf" %>
        <!-- Main-->
    <div class="main">
                <!--Form-->

      <form name="registrazione" method="POST" action="insertCtrl/insertP" class="needs-validation" onsubmit="return checkAll()" novalidate>
        <div class="row paddingrow">
          <!--Nome-->
          <div class="col-md-6 col-sm-12 mb-5">
            
              
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Progetto</span>
                    </div>
                    <input onblur="" name="progetto" type="text" class="form-control" id="" placeholder="Inserisci Progetto" aria-describedby="">
                    
                    <div class="errore" id="errorenome">
                      <p>Inserisci almeno 3 caratteri</p>
                    </div>
                    <div class="successo" id="successonome">
                        <i class="fas fa-check fa-2x"></i>
                    </div>
                  </div>
          </div>
          <div class="col-md-6 col-sm-12 mb-5">  
           <button type="submit" class="btn btn-primary">Inserisci</button>
          </div>
            
      </form>
     
  </div><!--Fine Main-->

  <!--Footer -->    

 <%@ include file="static/footer.jspf" %>