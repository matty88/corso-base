<%@ include file="static/header.jspf" %>
        <!-- Main-->
    <div class="main">
                <!--Form-->
                       
                   

      <form name="registrazione" method="POST" action="insertCtrl/insertP" class="needs-validation" onsubmit="return checkAll()" novalidate>
        <div class="row paddingrow">
          <!--Progetti-->
          <div class="col-md-6 col-sm-12 mb-5">
            
              
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Progetto</span>
                    </div>
                     <select name="progetto" class="form-control" id="">
                     <c:forEach var="mapP" items="${listP}">
                       <option value="${mapP.progetto}">${mapP.progetto}</option>
                     </c:forEach>  
                      </select>
                    
                    <div class="errore" id="errorenome">
                      <p>Inserisci almeno 3 caratteri</p>
                    </div>
                    <div class="successo" id="successonome">
                        <i class="fas fa-check fa-2x"></i>
                    </div>
                  </div>
          </div>
          <!--Dipendenti-->
          <div class="col-md-6 col-sm-12 mb-5">
            <h3>Click yes for add Dip to project</h3>
              
                <div class="input-group">
                 
                    <c:set var="i" value="1" scope="page" />
                     <c:forEach var="map" items="${listDip}">
                     <div class="col-md-6 col-sm-12 mb-5">
                     <div class="col-md-2 col-sm-6 float-left">${map.nome}</div>
						<div class="col-md-2 col-sm-6 float-left">${map.cognome}</div>
						<div class="col-md-4 col-sm-6 float-left">${map.cf}</div>
						</div>
						
                       <div class="custom-control custom-radio custom-control-inline">
                       
                <input type="radio" id="customRadioInline1" name="radioDip${i}" value="Y" class="custom-control-input">
               <label class="custom-control-label" for="customRadioInline1">Yes</label>
               </div>
               <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline2" name="radioDip${i}" value="N" class="custom-control-input" checked="checked">
              <label class="custom-control-label" for="customRadioInline2">No</label>
              </div>
                      <c:set var="i" value="${i + 1}" scope="page"/>
                     </c:forEach>  
                      
                    </div>
                    <div class="errore" id="errorenome">
                      <p>Inserisci almeno 3 caratteri</p>
                    </div>
                    <div class="successo" id="successonome">
                        <i class="fas fa-check fa-2x"></i>
                    </div>
                  </div>
          
          <div class="col-md-6 col-sm-12 mb-5">  
           <button type="submit" class="btn btn-primary">Inserisci</button>
          </div>
        </div>    
      </form>
     
  </div><!--Fine Main-->

  <!--Footer -->    

 <%@ include file="static/footer.jspf" %>