
//Regex
var btn = document.getElementById("submitvalid"); 
var espressione = new RegExp('^[a-zA-Z]{3,}$');
var regexPass = new RegExp('^[a-z1-9-A-Z]{8}$');
var regexMail=new RegExp('^[a-zA-Z0-9.-_]+@[a-zA-Z0-9.-_]+\\.[a-zA-Z]{2,6}$');
var regexCf=new RegExp('^[a-zA-Z]{6}[0-9]{2}[a-zA-Z]{1}[0-9]{2}[a-zA-Z]{1}[0-9]{3}[a-zA-Z]{1}$');

var okN=false; var okC=false;var okMail=false;var okPass=false; var okCPass=false,okCf= false;
var okGG=false; var okMM=false; var okAA=false;checkEta= false;

//validazione mail
function validMail(nome){
    var errore=document.getElementById("erroremail");
    var successo=document.getElementById("successomail");
    var erN=document.getElementById("validaMail");
    if(!regexMail.test(nome.value))
    {
   errore.style.display="block";
   successo.style.display="none";
   erN.style.outlineStyle="auto";
   erN.style.outlineColor="red";
   okMail= false;
    }
    else
    {
       errore.style.display="none";
       successo.style.display="block";
       erN.style.outlineStyle="auto";
       erN.style.outlineColor="#407114";
       okMail= true;
   }
   }
//validazione cf
function validCf(nome){
    var errore=document.getElementById("errorecf");
    var successo=document.getElementById("successocf");
    var erN=document.getElementById("validaCf");
    if(!regexCf.test(nome.value))
    {
   errore.style.display="block";
   successo.style.display="none";
   erN.style.outlineStyle="auto";
   erN.style.outlineColor="red";
   okCf= false;
    }
    else
    {
       errore.style.display="none";
       successo.style.display="block";
       erN.style.outlineStyle="auto";
       erN.style.outlineColor="#407114";
       okCf= true;
   }
   }

//Validazione nome
function validN(nome){
 var errore=document.getElementById("errorenome");
 var successo=document.getElementById("successonome");
 var erN=document.getElementById("validaNome");
 if(!espressione.test(nome.value))
 {
  errore.style.display="block";
  successo.style.display="none";
  erN.style.outlineStyle="auto";
  erN.style.outlineColor="red";
  okN= false;
 }
 else
 {
    errore.style.display="none";
    successo.style.display="block";
    erN.style.outlineStyle="auto";
    erN.style.outlineColor="#407114";
    okN= true;
}
}
//validazione cognome
function validC(cognome){
    var errore=document.getElementById("errorecognome");
    var successo=document.getElementById("successocognome");
    var erN=document.getElementById("validaCognome");
    if(!espressione.test(cognome.value))
    {
        errore.style.display="block";
        successo.style.display="none";
        erN.style.outlineStyle="auto";
        erN.style.outlineColor="red";
        okC=false;

    }
    else
    {
        errore.style.display="none";
        successo.style.display="block";
        erN.style.outlineStyle="auto";
        erN.style.outlineColor="#407114";
        okC=true;    
    }
   }


//valida data di nascita
function okData(date){
	var data= new Date();
    var mese = data.getMonth()+1;
    var giorno = data.getDate();
    var anno = data.getFullYear();
    
	text=date.value;
    var aa = text.substring(0, 4);
    var mm = text.substring(5, 7);
    var gg = text.substring(8, 10);
    if (aa>anno || aa <1900)
    	{ 
    	checkEta= false;
    	return
    	}
    
    var diffA=anno-aa;
    var diffM=mese-mm;
    var diffG=giorno-gg;
    
    
    if(diffA<18)
    {
       checkEta= false;
    }
    else if(diffA==18 && diffM<0)
    {
        checkEta= false;
    }
    else if(diffA==18 && diffM>=0 && diffG<0){
        checkEta= false;
    }
    else{
     checkEta=true;
    }
    

}
/*
    //DateCompare
    function dateCompare(){
    var checkEta=false;
    var data= new Date();
    var mese = data.getMonth()+1;
    var giorno = data.getDate();
    var anno = data.getFullYear();
    var mm=document.registrazione.meseD.value;
    var gg=document.registrazione.giornoD.value;
    var aa=document.registrazione.annoD.value;
    if(aa==0){aa=anno}
       
    var preimpostata = new Date(aa, mm, gg); 
    var pGiorno= preimpostata.getDate();
    var pMese=preimpostata.getMonth();
    var pAnno=preimpostata.getFullYear();
        
        
       var diffA= anno - pAnno;
       var diffM= mese - pMese;
       var diffG= giorno-pGiorno;
       alert(diffA)
       alert(diffM)
       alert(diffG)

       if(diffA<18)
       {
          checkEta= false;
          alert("anno"+checkEta)
       }
       else if(diffA==18 && diffM<0)
       {
           checkEta= false;
           alert("mese")
       }
       else if(diffA==18 && diffM>=0 && diffG<0){
           alert("giorno")
           checkEta= false;
       }
       else{
        checkEta=true;
       }
       return checkEta;
 }
 */
 //Validazione User
 function validU(nome){
    var errore=document.getElementById("erroreuser");
    var successo=document.getElementById("successouser");
    var erN=document.getElementById("validaUser");
    if(!espressione.test(nome.value) || nome.value=="")
    {
     errore.style.display="block";
     successo.style.display="none";
     erN.style.outlineStyle="auto";
     erN.style.outlineColor="red";
okU =false;
    }else{
        errore.style.display="none";
        successo.style.display="block";
        erN.style.outlineStyle="auto";
        erN.style.outlineColor="#407114";
        okU =true;
    }


}
//Validazione password
function validPass(){
    var errore=document.getElementById("errorepassword");
    var successo=document.getElementById("successopassword");
    var erN=document.getElementById("validaPassword");
    if(!regexPass.test(erN.value))
    {
     errore.style.display="block";
     successo.style.display="none";
     erN.style.outlineStyle="auto";
     erN.style.outlineColor="red";
     okPass=false;
    }else{
errore.style.display="none";
successo.style.display="block";
erN.style.outlineStyle="auto";
erN.style.outlineColor="#407114";
okPass =true;
    }

    var errorecp=document.getElementById("errorecpassword");
    var eCpass=document.getElementById("validaCPassword");
    var successocp=document.getElementById("successocpassword");

    if(eCpass.value != erN.value)
    {
     errorecp.style.display="block";
     successocp.style.display="none";
     eCpass.style.outlineStyle="auto";
     eCpass.style.outlineColor="red";
     okCPass=false;
    }else{
        okCPass =true;
errorecp.style.display="none";
successocp.style.display="block";
eCpass.style.outlineStyle="auto";
eCpass.style.outlineColor="#407114";
okCPass=true;
    }

}




function checkAll(){
   var errSbm=document.getElementById("erroreSubmit");
   // var checkEta=dateCompare();
	/*
    if (checkEta==false) 
    {
        alert("Età inferiore ai 18 anni")
        errSbm.style.display="block";
        return false;
    };
   */ 
    if(okN && okC && okMail && okU && okPass && okCf)
    {
       return true
    }else{
        errSbm.style.display="block";
    }
return false;
}

$( '#topheader a' ).on( 'click', function () {
	$( '#topheader' ).find( 'a.active' ).removeClass( 'active' );
	$( this).addClass( 'active' );
});
$("#liNM").click(function(){
	$("#inptNM").show();
	  $("#inptCF").hide();
});
$("#liCF").click(function(){
	$("#inptNM").hide();
	  $("#inptCF").show();
});

$(document).ready(function() {
	   $('#jqueryDiv').toggle(1000);
	   
	  

	});
