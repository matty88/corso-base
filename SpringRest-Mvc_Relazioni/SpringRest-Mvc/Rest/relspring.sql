-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 09, 2019 alle 16:19
-- Versione del server: 10.1.34-MariaDB
-- Versione PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `relspring`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_dip` int(11) DEFAULT NULL,
  `id_ruolo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `accounts`
--

INSERT INTO `accounts` (`id`, `user`, `password`, `id_dip`, `id_ruolo`) VALUES
(18, 'genny121', '1000', 39, 2),
(19, 'H00odd8809', 'wyu99wuiu2u87', 3, 2),
(20, 'gennyU8', '1000', 40, NULL),
(21, 'amada', '1000', 41, 1),
(22, 'AmarcorD', '1000', 42, 1),
(29, 'HASpo9d09', 'wyu99wui92u87', 50, 1),
(30, 'HASp78y09', 'wyu99wui92u87', 51, 1),
(34, 'HAojd8809', 'wyu99wuiu2u87', 55, 2),
(35, 'H0iojd8809', 'wyu99wuiu2u87', 56, 2),
(38, 'H00o8iu809', 'wyu99wuiu2u87', 59, 2),
(39, 'H00owwu809', 'wyu99wuiu2u87', 60, 2),
(41, 'Hp00IKK9', 'wyu99wuiu2u87', 62, 2),
(45, 'Hpopd9', 'wyu99wuiu2u87', 67, 2),
(49, 'H6pd9', 'wyu99wuiu2u87', 71, 2),
(50, 'H6p909d9', 'wyu99wuiu2u87', 72, 2),
(51, 'H6p90999d9', 'wyu99wuiu2u87', 73, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `dettaglioprogetti`
--

CREATE TABLE `dettaglioprogetti` (
  `id` int(11) NOT NULL,
  `id_dip` int(11) NOT NULL,
  `id_progetto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `dettaglioprogetti`
--

INSERT INTO `dettaglioprogetti` (`id`, `id_dip`, `id_progetto`) VALUES
(18, 50, 14),
(8, 41, 13),
(16, 51, 19),
(15, 50, 19),
(14, 47, 19),
(19, 47, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `dipendenti`
--

CREATE TABLE `dipendenti` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `comune` varchar(50) NOT NULL,
  `data_di_nascita` date DEFAULT NULL,
  `sesso` char(1) NOT NULL,
  `stipendio` double NOT NULL,
  `cf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `dipendenti`
--

INSERT INTO `dipendenti` (`id`, `nome`, `cognome`, `comune`, `data_di_nascita`, `sesso`, `stipendio`, `cf`) VALUES
(3, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts0oidermuis8s'),
(39, 'gennaro', 'Rossini', 'napoli', '1980-11-03', 'm', 1000, '67dyusl90doi89s5'),
(40, 'gennaro', 'Rossini', 'napoli', '1980-11-03', 'm', 1000, 'd67dydud78d9d0d9'),
(41, 'gennaro', 'Verdi', 'napoli', '1980-11-03', 'm', 1000, 'd67dyoo078d9d0d9'),
(42, 'Salam', 'Verdi', 'napoli', '1980-11-03', 'm', 1000, 's67dysus7s8sus8s7'),
(47, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts77777sus7s8s'),
(50, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts0oi07sus7s8s'),
(51, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts0oi07tds7s8s'),
(55, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts0oi255d88s8s'),
(56, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts0oi255duis8s'),
(59, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts233dermuis8s'),
(60, 'giovanni', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts23212rmuis8s'),
(62, 'cristian', 'bianchi', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts2uiedrmuis11'),
(67, 'cristian', 'formisano', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts2kkkkis11'),
(71, 'cristian', 'formisano', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts2io9kis11'),
(72, 'cristian', 'formisano', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts2isskis11'),
(73, 'cristian', 'formisano', 'napoli', '1980-11-30', 'm', 1000, 'sfsgsts09skis11');

-- --------------------------------------------------------

--
-- Struttura della tabella `progetti`
--

CREATE TABLE `progetti` (
  `id` int(11) NOT NULL,
  `progetto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `progetti`
--

INSERT INTO `progetti` (`id`, `progetto`) VALUES
(14, 'Progetto2'),
(19, 'Progetto1'),
(27, 'Progetto3');

-- --------------------------------------------------------

--
-- Struttura della tabella `ruoli`
--

CREATE TABLE `ruoli` (
  `id` int(11) NOT NULL,
  `ruolo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ruoli`
--

INSERT INTO `ruoli` (`id`, `ruolo`) VALUES
(1, 'Admin'),
(2, 'Guest');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `FKrv9j1x4vsecmu0dbubo2tqd5m` (`id_dip`) USING BTREE,
  ADD KEY `fk_id_ruolo` (`id_ruolo`);

--
-- Indici per le tabelle `dettaglioprogetti`
--
ALTER TABLE `dettaglioprogetti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKfvhsfxqt79c7ide1ynugpchlm` (`id_progetto`),
  ADD KEY `FK8wy9w5qshsmt7kgco1awcpg9p` (`id_dip`);

--
-- Indici per le tabelle `dipendenti`
--
ALTER TABLE `dipendenti`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cf` (`cf`);

--
-- Indici per le tabelle `progetti`
--
ALTER TABLE `progetti`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ruoli`
--
ALTER TABLE `ruoli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ruolo` (`ruolo`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT per la tabella `dettaglioprogetti`
--
ALTER TABLE `dettaglioprogetti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `dipendenti`
--
ALTER TABLE `dipendenti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT per la tabella `progetti`
--
ALTER TABLE `progetti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT per la tabella `ruoli`
--
ALTER TABLE `ruoli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `FKrv9j1x4vsecmu0dbubo2tqd5m` FOREIGN KEY (`id_dip`) REFERENCES `dipendenti` (`id`),
  ADD CONSTRAINT `fk_id_ruolo` FOREIGN KEY (`id_ruolo`) REFERENCES `ruoli` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
