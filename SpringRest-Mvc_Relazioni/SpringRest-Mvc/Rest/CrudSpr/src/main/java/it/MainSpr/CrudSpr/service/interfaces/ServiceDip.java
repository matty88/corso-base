package it.MainSpr.CrudSpr.service.interfaces;

import it.MainSpr.CrudSpr.jpa.entity.Account;
import it.MainSpr.CrudSpr.jpa.entity.Dipendente;
import it.MainSpr.CrudSpr.jpa.entity.Progetto;
import it.MainSpr.CrudSpr.jpa.entity.Ruolo;

import java.util.List;

import org.json.JSONObject;

public interface ServiceDip {
	//Metodi di Inserimento
	void insertD(Dipendente d);
	void insertP(Progetto p);
	void insertD(List<Dipendente> listd);

	Object[] convertJsonDipendente(JSONObject json);
	
	//Metodi di Modifica
	void modificaD(Dipendente d);
	
	//Metodi di Eliminazione
	void deleteD(Dipendente d);
	
	//Metodi di Ricerca
	Dipendente getDip(int id);
	
	Dipendente getDip(String cf);
	
	List<Dipendente> getListDip(Double stipendio);
	
	List<Dipendente> getListDip(String comune);
	
	List<Dipendente> getListDipProject(String progetto);
	
    List<Dipendente> getAllDip(String progetto);
	
	Account getAcc(int id);
	
	Account getAcc(String user);
	
    List<Account> getListAccR(int id_ruolo);
	
	Account getListAcc(int id_dip);
	
	Ruolo getRu(int id);

	Ruolo getRu(String ruolo);
	
	Progetto getProj(int id);
	
	Progetto getProj(String progetto);
	
}
