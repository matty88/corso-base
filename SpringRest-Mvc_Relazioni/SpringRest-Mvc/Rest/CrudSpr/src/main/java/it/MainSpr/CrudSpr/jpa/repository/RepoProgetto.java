package it.MainSpr.CrudSpr.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.MainSpr.CrudSpr.jpa.entity.Dipendente;
import it.MainSpr.CrudSpr.jpa.entity.Progetto;

public interface RepoProgetto extends JpaRepository<Progetto, Integer> {
	
 Progetto findByid(int id);
 Progetto findByprogetto(String progetto);
}
