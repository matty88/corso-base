package it.MainSpr.CrudSpr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudSprApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSprApplication.class, args);
	}

}
