package it.MainSpr.CrudSpr.jpa.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table (name="dipendenti")
public class Dipendente {

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="id", nullable=false)
private int id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

@Column(name="nome", nullable=false)
private String nome;
public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}

@Column(name="cognome", nullable=false)
private String cognome;
public String getCognome() {
	return cognome;
}
public void setCognome(String cognome) {
	this.cognome = cognome;
}

@Column(name="comune", nullable=false)
private String comune;
public String getComune() {
	return comune;
}
public void setComune(String comune) {
	this.comune = comune;
}

@Column(name="cf", nullable=false,unique=true)
private String cf;
public String getcf() {
	return cf;
}
public void setcf(String cf) {
	this.cf = cf;
}

@Column(name="sesso", nullable=false)
private String sesso;
public String getSesso() {
	return sesso;
}
public void setSesso(String sesso) {
	this.sesso = sesso;
}

@Column(name="data_di_nascita")
private LocalDate data_di_nascita;
public LocalDate getData_di_nascita() {
	return data_di_nascita;
}
public void setData_di_nascita(LocalDate data_di_nascita) {
	this.data_di_nascita = data_di_nascita;
}
@Column(name="stipendio", nullable=false)
private double stipendio;
public double getStipendio() {
	return stipendio;
}
public void setStipendio(double stipendio) {
	this.stipendio = stipendio;
}

@OneToOne(fetch= FetchType.EAGER, mappedBy="dipendente", cascade= CascadeType.ALL)
private Account account;
public Account getAccount() {
	return account;
}
public void setAccount(Account account) {
	this.account = account;
}

@ManyToMany(fetch=FetchType.EAGER,cascade= {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
@JoinTable(name="dettaglioProgetti",
joinColumns=@JoinColumn(name="id_dip"),
inverseJoinColumns=@JoinColumn(name="id_progetto"))
private List<Progetto> listP= new ArrayList<Progetto>();
public List<Progetto> getList() {
	return listP;
}
public void setList(List<Progetto> listP) {
	this.listP = listP;
}
public Dipendente() {}
public Dipendente(String nome, String cognome, String comune, String cf, String sesso,
		LocalDate data_di_nascita, double stipendio) {
	super();
	
	this.nome = nome;
	this.cognome = cognome;
	this.comune = comune;
	this.cf = cf;
	this.sesso = sesso;
	this.data_di_nascita = data_di_nascita;
	this.stipendio = stipendio;
}

@Override
public String toString() {
	return "Dipendente [nome=" + nome + ", cognome=" + cognome + ", comune=" + comune + ", cf="
			+ cf + ", sesso=" + sesso + ", data_di_nascita=" + data_di_nascita + ", stipendio=" + stipendio
			+", account=" + account+"]";
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((account == null) ? 0 : account.hashCode());
	result = prime * result + ((cf == null) ? 0 : cf.hashCode());
	result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
	result = prime * result + ((comune == null) ? 0 : comune.hashCode());
	result = prime * result + ((data_di_nascita == null) ? 0 : data_di_nascita.hashCode());
	result = prime * result + id;
	result = prime * result + ((listP == null) ? 0 : listP.hashCode());
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	result = prime * result + ((sesso == null) ? 0 : sesso.hashCode());
	long temp;
	temp = Double.doubleToLongBits(stipendio);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Dipendente other = (Dipendente) obj;
	if (account == null) {
		if (other.account != null)
			return false;
	} else if (!account.equals(other.account))
		return false;
	if (cf == null) {
		if (other.cf != null)
			return false;
	} else if (!cf.equals(other.cf))
		return false;
	if (cognome == null) {
		if (other.cognome != null)
			return false;
	} else if (!cognome.equals(other.cognome))
		return false;
	if (comune == null) {
		if (other.comune != null)
			return false;
	} else if (!comune.equals(other.comune))
		return false;
	if (data_di_nascita == null) {
		if (other.data_di_nascita != null)
			return false;
	} else if (!data_di_nascita.equals(other.data_di_nascita))
		return false;
	if (id != other.id)
		return false;
	if (listP == null) {
		if (other.listP != null)
			return false;
	} else if (!listP.equals(other.listP))
		return false;
	if (nome == null) {
		if (other.nome != null)
			return false;
	} else if (!nome.equals(other.nome))
		return false;
	if (sesso == null) {
		if (other.sesso != null)
			return false;
	} else if (!sesso.equals(other.sesso))
		return false;
	if (Double.doubleToLongBits(stipendio) != Double.doubleToLongBits(other.stipendio))
		return false;
	return true;
}


}
