package it.MainSpr.CrudSpr.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import it.MainSpr.CrudSpr.jpa.entity.Account;
import it.MainSpr.CrudSpr.jpa.entity.Dipendente;
import it.MainSpr.CrudSpr.service.interfaces.ServiceDip;

@CrossOrigin(origins = "http://localhost:8085", maxAge = 3600)
@RestController
@RequestMapping({"/SearchCtrl"})
public class ControllerSearch {

	@Autowired
	private ServiceDip serviceDip;
	
	//Ricerca dipendente per id, codice fiscale; clausola where su stipendio, comune, progetto
	@ResponseBody
	@RequestMapping(value="searchDip", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	 public String searchDip(@RequestHeader HttpHeaders headers, @RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	    int scelta=Integer.parseInt(json.get("scelta").toString());
		switch(scelta) {
		case 1:
		    int id=Integer.parseInt(json.get("id").toString());
		    Dipendente d=serviceDip.getDip(id);
		    System.out.println(d);
			break;
		case 2:
		    String cf=json.get("cf").toString();
            Dipendente d2=serviceDip.getDip(cf);
            System.out.println(d2);
			break;
		case 3:
		    Double stipendio=Double.parseDouble(json.get("stipendio").toString());
		    List<Dipendente> listDip=serviceDip.getListDip(stipendio);
		    System.out.println(listDip);
			break;
		case 4:
			String comune=json.get("comune").toString();
			List<Dipendente> listDipC=serviceDip.getListDip(comune);
			System.out.println(listDipC);
		    
			break;
		case 5:
			String progetto=json.get("progetto").toString();
			List<Dipendente> listDipP=serviceDip.getListDipProject(progetto);
			System.out.println(listDipP);
		    
			break;	
		}
		return jsonReturn.toString();
	}
	
	//Ricerca account per id ed user; lista account con clausola where su id_ruolo o id_dip
	@ResponseBody
	@RequestMapping(value="searchAcc", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	 public String searchAcc(@RequestHeader HttpHeaders headers, @RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	    int scelta=Integer.parseInt(json.get("scelta").toString());
		switch(scelta) {
		case 1:
		    int id=Integer.parseInt(json.get("id").toString());
		    Account a=serviceDip.getAcc(id);
		    System.out.println(a);
			break;
		case 2:
		    String user=json.get("user").toString();
		    Account a2=serviceDip.getAcc(user);
            System.out.println(a2);
			break;
		case 3:
			int id_ruolo=Integer.parseInt(json.get("id_ruolo").toString());
		    List<Account> listAcc=serviceDip.getListAccR(id_ruolo);
		    System.out.println(listAcc);
			break;
		case 4:
			int id_dip=Integer.parseInt(json.get("id_dip").toString());
			Account listAccD=serviceDip.getListAcc(id_dip);
			System.out.println(listAccD);    
			break;	
		}
		return jsonReturn.toString();
	}
	
	
}
