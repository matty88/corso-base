package it.MainSpr.CrudSpr.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.MainSpr.CrudSpr.jpa.entity.Ruolo;

public interface RepoRuolo extends JpaRepository<Ruolo, Integer>{
   Ruolo findByid(int id);
   Ruolo findByruolo(String ruolo);
}
