package it.MainSpr.CrudSpr.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.MainSpr.CrudSpr.jpa.entity.Account;
import it.MainSpr.CrudSpr.jpa.entity.Dipendente;
import it.MainSpr.CrudSpr.jpa.entity.Progetto;
import it.MainSpr.CrudSpr.jpa.entity.Ruolo;
import it.MainSpr.CrudSpr.jpa.repository.RepoAccount;
import it.MainSpr.CrudSpr.jpa.repository.RepoDipendente;
import it.MainSpr.CrudSpr.jpa.repository.RepoProgetto;
import it.MainSpr.CrudSpr.jpa.repository.RepoRuolo;
import it.MainSpr.CrudSpr.service.interfaces.ServiceDip;

@Service
public class ServiceImplDip implements ServiceDip{

	@Autowired 
	private RepoDipendente repoDipendente;
	@Autowired 
	private RepoAccount repoAccount;
	@Autowired 
	private RepoRuolo repoRuolo;
	@Autowired 
	private RepoProgetto repoProgetto;
	

	//Metodi di Inserimento
	@Override
	public void insertD(Dipendente d) {
		System.out.println(d);
		repoDipendente.save(d);
		
	}
	
	@Override
	public void insertD(List<Dipendente> d) {
		System.out.println(d);
		repoDipendente.saveAll(d);
		
	}
	
	@Override
	public void insertP(Progetto p) {
		repoProgetto.save(p);
		
	}


	@Override
	public Object[] convertJsonDipendente(JSONObject json) {
		Object[] obj= new Object[3];
		Dipendente d= new Dipendente(
				json.get("nome").toString(),
				json.get("cognome").toString(),
				json.get("comune").toString(),
				json.get("cf").toString(),
				json.getString("sesso").toString(),
				LocalDate.parse(json.getString("data")),
				Double.parseDouble(json.get("stipendio").toString())
				);
		Account a= new Account(
				json.get("user").toString(),
				json.get("password").toString()
				);
		String ruolo=json.get("ruolo").toString();
		
		obj[0]=d;obj[1]=a;obj[2]=ruolo;
		return obj;

	}

	//Metodi di Modifica
	@Override
	public void modificaD(Dipendente d) {
		repoDipendente.save(d);
	}
	//Metodi di Eliminazione
	@Override
	public void deleteD(Dipendente d) {
          repoDipendente.delete(d);		
	}
	//Metodi di Ricerca
	@Override
	
    public Dipendente getDip(int id) {
		return repoDipendente.findById(id);
	}

	@Override
	public Dipendente getDip(String cf) {
		return repoDipendente.findBycf(cf);
	}
	@Override
	public List<Dipendente> getAllDip(String progetto) {
		return repoDipendente.findbyJPLproject(progetto);
	}
	@Override
	public Account getAcc(int id) {
		return repoAccount.findById(id);
	}

	@Override
	public Ruolo getRu(int id) {
		return repoRuolo.findByid(id);
	}


	@Override
	public Ruolo getRu(String ruolo) {
		return repoRuolo.findByruolo(ruolo);
	}


	@Override
	public Account getAcc(String user) {
		return repoAccount.findByuser(user);
	}

	@Override
	public List<Dipendente> getListDip(Double stipendio) {
		return repoDipendente.findbyJPLstipendio(stipendio);
	}

	@Override
	public List<Dipendente> getListDip(String comune) {
		return repoDipendente.findbyJPLcomune(comune);
	}

	@Override
	public List<Account> getListAccR(int id_ruolo) {
		return repoAccount.findbyJPLruolo(id_ruolo);
	}

	@Override
	public Account getListAcc(int id_dip) {
		return repoAccount.findbyJPLdip(id_dip);
	}

	@Override
	public List<Dipendente> getListDipProject(String progetto) {
		return repoDipendente.findbyJPLproject(progetto);
	}

	@Override
	public Progetto getProj(int id) {
		
		return repoProgetto.findByid(id);
	}
	
	@Override
	public Progetto getProj(String progetto) {
		
		return repoProgetto.findByprogetto(progetto);
	}

	

	

}
