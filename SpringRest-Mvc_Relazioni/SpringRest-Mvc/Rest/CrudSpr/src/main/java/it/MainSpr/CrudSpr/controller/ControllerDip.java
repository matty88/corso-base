package it.MainSpr.CrudSpr.controller;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.MainSpr.CrudSpr.jpa.entity.Account;
import it.MainSpr.CrudSpr.jpa.entity.Dipendente;
import it.MainSpr.CrudSpr.jpa.entity.Progetto;
import it.MainSpr.CrudSpr.jpa.entity.Ruolo;
import it.MainSpr.CrudSpr.service.interfaces.ServiceDip;

@CrossOrigin(origins = "http://localhost:8085", maxAge = 3600)
@RestController
@RequestMapping({"/CtrlDipendenti"})
public class ControllerDip {
	
	@Autowired
	private ServiceDip serviceDip;
	
	
	//Metodi di Inserimento
	
	//Inserimento dipendente
	@ResponseBody
	@RequestMapping(value="insertDip", method=RequestMethod.POST, produces= MediaType.APPLICATION_JSON_VALUE)
	public String insertDip(@RequestHeader HttpHeaders headers, @RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	    
	    if(json.has("cf")) {
	    	if(json.has("nome") && json.has("cognome") && json.has("comune") && json.has("sesso") && json.has("data") && json.has("stipendio") && json.has("user") && json.has("password")) 
	    	 { 
	    		Object[] obj=serviceDip.convertJsonDipendente(json);
	    		Dipendente d=(Dipendente)obj[0];
	    		String cfCtrl=d.getcf();
	    		Account a =(Account)obj[1];
	    		String userCtrl=a.getUser();
	    		String ruolo=(String)obj[2];
	    		Ruolo r=serviceDip.getRu(ruolo);
	    		
	    		if(serviceDip.getDip(cfCtrl)!=null) {
		 	    	jsonReturn.put("messaggio", "cf gia presente");  			
	    		}else if(serviceDip.getAcc(userCtrl)!=null) {
		 	    	jsonReturn.put("messaggio", "user gia presente");  			

	    		}else{
	    			a.setDipendente(d);
		    		a.setRuolo(r);
		    		d.setAccount(a);
		    		serviceDip.insertD(d);
		    		jsonReturn.put("messaggio", "inserito: "+d);
	    		};
	    	    
	 		    
	    	 }else {
	 	    	jsonReturn.put("messaggio", "Inserimento fallito");

	    	 }
	    	}
	    	
	    return jsonReturn.toString();
	}
	
	//Inserimento progetto e dipendente 
	@ResponseBody
    @RequestMapping(value="addProject", method=RequestMethod.POST, produces= MediaType.APPLICATION_JSON_VALUE)
	public String projectDip(@RequestHeader HttpHeaders headers, @RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	   
		Object[] obj=serviceDip.convertJsonDipendente(json);
		Dipendente d=(Dipendente)obj[0];
		Account a =(Account)obj[1];
		String ruolo=(String)obj[2];
		Ruolo r=serviceDip.getRu(ruolo);		
		a.setRuolo(r);
		a.setDipendente(d);
		d.setAccount(a);
		Progetto p1=new Progetto("Progetto3");
		
		List<Progetto> list= new ArrayList<Progetto>();
		List<Dipendente> listD= new ArrayList<Dipendente>();
		listD.add(d);
		list.add(p1);
		p1.setList(listD);
		//d.setList(list);
		serviceDip.insertP(p1);
		jsonReturn.put("messaggio", "inserimento effettuato");
		
		return jsonReturn.toString();
		/*
	    Dipendente d=serviceDip.getDip(39);
	    
	    Progetto p=serviceDip.getProj(13);
	    List<Progetto> list= new ArrayList<Progetto>();
		list.add(p);
         d.setList(list);
	    serviceDip.insertD(d);
	    */
	}
	
	//Inserimento progetto con lista dipendenti
	@ResponseBody
	@RequestMapping(value="insertDipToP", method=RequestMethod.POST, produces= MediaType.APPLICATION_JSON_VALUE)
	public void insertDipToP(@RequestHeader HttpHeaders header,@RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	    String progetto=json.getString("progetto");
	    List<Dipendente> listD=serviceDip.getAllDip(progetto);
	    System.out.println("listaD1"+listD);
	    int nDipAdd=Integer.parseInt(json.getString("nDipAdd"));
	    for (int i=1; i<=nDipAdd;i++) {
	    	String iS=Integer.toString(i);
	    	int idDadd=Integer.parseInt(json.getString("idDadd"+iS));
	    	Dipendente d=serviceDip.getDip(idDadd);
	    	listD.add(d);
	    }
	    System.out.println("listaD2"+listD);
	    Progetto p=serviceDip.getProj(progetto);
	    p.setList(listD);
	    serviceDip.insertP(p);
	}
	//Metodi di Modifica
	@ResponseBody
	@RequestMapping(value="modificaD", method=RequestMethod.POST, produces= MediaType.APPLICATION_JSON_VALUE)
	public String modificaD(@RequestHeader HttpHeaders header,@RequestBody String request) {
		JSONObject jsonReturn = new JSONObject();
	    JSONObject json = new JSONObject(request);
	    Dipendente d,d2,dOld;int idM;Account aOld,a,a2;int a22=0;

		
			
	        idM=json.getInt("idM");
	        dOld=serviceDip.getDip(idM);
	        System.out.println("dOld: "+dOld);
	        if(dOld!=null) {
				aOld=dOld.getAccount();
		        a22=aOld.getId();
	        }else {
	        	aOld=null;
	        }
	        System.out.println("aOld: "+aOld+"a22"+a22);

			String cfCtrl=json.getString("cf");
			String userCtrl=json.getString("user");
			d2=serviceDip.getDip(cfCtrl);
	        System.out.println("d2: "+d2);

			a2=serviceDip.getAcc(userCtrl);
	        System.out.println("a2: "+a2);

		if(d2!=null && d2.getId()!=idM) {
 	    	jsonReturn.put("messaggio", "cf gia presente");  			
		}else if(a2!=null && !a2.getUser().equalsIgnoreCase(aOld.getUser())) {
 	    	jsonReturn.put("messaggio", "user gia presente");  			

		}else{
			Object[] obj=serviceDip.convertJsonDipendente(json);
			d=(Dipendente)obj[0]; a=(Account)obj[1];String ruolo=(String)obj[2];
			d.setId(idM);
    		Ruolo r=serviceDip.getRu(ruolo);
			a.setRuolo(r);
			if(a22!=0) {
				a.setId(a22);
			}
			a.setDipendente(d);
			d.setAccount(a);
			serviceDip.modificaD(d);
			jsonReturn.put("messaggio", "dipendente modificato");  	
		};
	    
	    return jsonReturn.toString();
	}
  
	//Metodi di Eliminazione
	@ResponseBody
	@RequestMapping(value="deleteD", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public String deleteD(@RequestHeader HttpHeaders header, @RequestBody String request) {
		JSONObject jsonReturn=new JSONObject();
		JSONObject json=new JSONObject(request);
		Dipendente d;
		String cf=json.get("cf").toString();
		int id=Integer.parseInt(json.get("id").toString());
		int scelta=Integer.parseInt(json.get("scelta").toString());

		switch(scelta) {
		//elimina dipendente per codice fiscale
		case 1:
			d=serviceDip.getDip(cf);
			serviceDip.deleteD(d);
			jsonReturn.put("messaggio", "Dipendente"+d.getcf()+" eliminato");
            break;
		case 2:
			//elimina dipendente per id
			d=serviceDip.getDip(id);
			serviceDip.deleteD(d);
			jsonReturn.put("messaggio", "Dipendente"+d.getId()+" eliminato");
            break;
		}
        
		return jsonReturn.toString();
 	  
    }
	
}
