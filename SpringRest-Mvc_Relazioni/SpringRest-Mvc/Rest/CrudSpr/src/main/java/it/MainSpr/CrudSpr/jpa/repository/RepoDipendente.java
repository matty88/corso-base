package it.MainSpr.CrudSpr.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.MainSpr.CrudSpr.jpa.entity.Dipendente;

public interface RepoDipendente extends JpaRepository<Dipendente, Integer>{
	
	Dipendente findById(int id);
	Dipendente findBycf(String cf);
	
	@Query("from Dipendente where stipendio>?1")
	List<Dipendente> findbyJPLstipendio(Double stipendio);
	
	@Query("from Dipendente where comune=?1")
	List<Dipendente> findbyJPLcomune(String comune);
	
	@Query("FROM Dipendente d JOIN d.listP p WHERE p.progetto =?1")
	//@Query("FROM Dipendente d JOIN Progetto p WHERE p.progetto =?1")
	
	List<Dipendente> findbyJPLproject(String progetto);
	
	
}
