<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<th>Conta numeri dispari fino a <%=request.getParameter("max")%></th>
		</tr>
		<%
			int max = Integer.parseInt(request.getParameter("max"));
		for (int index=1;index<=max;index++) {
			if (index%2!=0) {
		%>
		<tr>
			<td><%=index%></td>
		</tr>
		<%
			}
		}
		%>
	</table>
	<a href="index.html">Torna indietro</a>
</body>
</html>