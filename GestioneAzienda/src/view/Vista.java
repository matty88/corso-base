package view;

import model.Dipendente;
import model.Dirigente;
import model.Manager;
import model.Persona;

public class Vista {

	public void printMsg(String msg) {
		System.out.println(msg);
	}

	//UPCASTING GRAZIE AL POLIMORFISMO, SE ABBIAMO UN METODO CHE LAVORA CON OGGETTI SIMILI, SE METTIAMO IL METODO PID' GENERICO FUNZIONERA' PER TUTTI(PERCHE' CHIARAMENTE QUELLI PI� DETTAGLIATI RENDERANNO IMPOSSIBILE FAR STAMPARE PERSONA PERCH� NON HA GLI ARGOMENTI DI QUELLI PIU' DETTAGLIATI)
	public void stampa(Persona p) {
		System.out.println(p);
	}

	public void stampaScheda(Persona p) {
		System.out.println("Nome: "+p.getNome());
		System.out.println("Cognome: "+p.getCognome());
		System.out.println("Et�: "+p.getEta());
		if(p instanceof Dirigente) {
			Dirigente dir = (Dirigente) p;
			System.out.println("Stipendio: "+dir.getStipendio());
			System.out.println("Ruolo: "+dir.getRuolo());
			System.out.println("Area responsabilit�: "+dir.getAreaResponsabilita());
			System.out.println("Livello funzionale: "+dir.getLivelloFunzionale());
		} else if (p instanceof Manager) {
			Manager man = (Manager) p;
			System.out.println("Stipendio: "+man.getStipendio());
			System.out.println("Ruolo: "+man.getRuolo());
			System.out.println("Area responsabilit�: "+man.getAreaResponsabilita());			
		} else if (p instanceof Dipendente) {
			Dipendente d = (Dipendente) p;
			System.out.println("Stipendio: "+d.getStipendio());
			System.out.println("Ruolo: "+d.getRuolo());
		}	
	}	
}

//IL LIMITE DEL POLIMORFISMO � CHE TI LASCIA PASSARE QUALUNQUE TIPO DI INDIRIZZO SIMILE, MA QUANDO VA DIRETTAMENTE ALL 'OGGETTO LUI VEDE SOLO QUELLO INDICATO
