package controller;

import model.Dipendente;
import model.Dirigente;
import model.Manager;
import model.Persona;
import view.Vista;

public class Avvio {

	public static void main(String[] args) {
		Vista v = new Vista();
		
		//PERSONA
		Persona p = new Persona();
		p.setNome("Mario");
		p.setCognome("Rossi");
		p.setEta(30);
		v.printMsg("***PERSONA***");
		v.stampa(p);
		
		Persona p1 = new Persona("Mario", "Rossi", 30);
		v.stampa(p1);
		
		//DIPENDENTE
		Dipendente d = new Dipendente();
		d.setNome("Francesca");
		d.setCognome("Esposito");
		d.setEta(28);
		d.setStipendio(1750.00);
		d.setRuolo("Programmatore");
		v.printMsg("\n***DIPENDENTE***");
		v.stampa(d);
		
		Dipendente d1 = new Dipendente("Francesca", "Esposito", 28, 1750.00, "Programmatore");
		v.stampa(d1);
		
		//MANAGER
		Manager man = new Manager();
		man.setNome("Alessandro");
		man.setCognome("Bianchi");
		man.setEta(35);
		man.setStipendio(2150.00);
		man.setRuolo("Project Manager");
		man.setAreaResponsabilita("CED");
		v.printMsg("\n***MANAGER***");
		v.stampa(man);
		
		Manager man1 = new Manager("Alessandro","Bianchi",35,2150.00,"Project Manager","CED");
		v.stampa(man1);
		
		//Dirigente
		Dirigente dir = new Dirigente();
		dir.setNome("Valentina");
		dir.setCognome("Colucci");
		dir.setEta(42);
		dir.setStipendio(3750.00);
		dir.setRuolo("Direttore Amministrativo");
		dir.setAreaResponsabilita("CED");
		dir.setLivelloFunzionale("F3");
		v.printMsg("\n***DIRIGENTE***");
		v.stampa(dir);
		
		Dirigente dir1 = new Dirigente("Valentina","Colucci",42,3750.00,"Direttore Amministrativo","CED","F3");
		v.stampa(dir1);
		
		v.printMsg("\n");
		//v.stampaScheda(p);
		v.stampaScheda(dir);
	}

}
