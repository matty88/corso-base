package model;

public class Dipendente extends Persona {

	private Double stipendio;
	private String ruolo;
	
	public Double getStipendio() {
		return stipendio;
	}
	public void setStipendio(Double stipendio) {
		this.stipendio = stipendio;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	public Dipendente(String nome, String cognome, int eta, Double stipendio, String ruolo) {
		super(nome, cognome, eta);
		this.stipendio = stipendio;
		this.ruolo = ruolo;
	}
	
	public Dipendente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
//	public Dipendente(String nome, String cognome, int eta) {
//		super(nome, cognome, eta);
//		// TODO Auto-generated constructor stub
//	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		result = prime * result + ((stipendio == null) ? 0 : stipendio.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Stipendio = " + stipendio + ", Ruolo = " + ruolo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dipendente other = (Dipendente) obj;
		if (ruolo == null) {
			if (other.ruolo != null)
				return false;
		} else if (!ruolo.equals(other.ruolo))
			return false;
		if (stipendio == null) {
			if (other.stipendio != null)
				return false;
		} else if (!stipendio.equals(other.stipendio))
			return false;
		return true;
	}

}
