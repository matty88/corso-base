package model;

public class Manager extends Dipendente {
	
	private String areaResponsabilita;

	public String getAreaResponsabilita() {
		return areaResponsabilita;
	}

	public void setAreaResponsabilita(String areaResponsabilita) {
		this.areaResponsabilita = areaResponsabilita;
	}

	public Manager(String nome, String cognome, int eta, Double stipendio, String ruolo, String areaResponsabilita) {
		super(nome, cognome, eta, stipendio, ruolo);
		this.areaResponsabilita = areaResponsabilita;
	}

	public Manager() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Area responsabilitÓ = " +areaResponsabilita;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((areaResponsabilita == null) ? 0 : areaResponsabilita.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manager other = (Manager) obj;
		if (areaResponsabilita == null) {
			if (other.areaResponsabilita != null)
				return false;
		} else if (!areaResponsabilita.equals(other.areaResponsabilita))
			return false;
		return true;
	}

//	public Manager(String nome, String cognome, int eta, Double stipendio, String ruolo) {
//		super(nome, cognome, eta, stipendio, ruolo);
//		// TODO Auto-generated constructor stub
//	}
	
	

}
