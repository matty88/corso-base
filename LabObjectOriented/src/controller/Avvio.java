package controller;

import model.Persona;
import model.TestIncapsulamentoAltro;

public class Avvio {

	public static void main(String[] args) {
		Persona p =new Persona();                          //NEL CASO IN CUI VOGLIAMO CREARE UN OGGETTO VUOTO
		Persona p1 =new Persona("Mario","Rossi",30,true);  //NEL CASO IN CUI ABBIAMO SOLO IL COSTRUTTORE PARAMETRIZZATO
		System.out.println(p.toString());
		System.out.println(p1.toString());
		p.nome = "Mario";
		p.cognome = "Commone";
		p.eta = 32;
		p.sesso = true;
		System.out.println(p.toString());
		
		//INCAPSULAMENTO NELLO STESSO PACKAGE
		TestIncapsulamento t = new TestIncapsulamento();
		//NELLO STESSO PACKAGE SOLO IL PRIVATE NON SI VEDE
		t.a=10;
		t.c=20;
		t.d=30;
		System.out.println(t.a+t.c+t.d);
		
		//INCAPSULAMENTO CON CLASSI IN PACKAGE DIVERSI
		TestIncapsulamentoAltro ta = new TestIncapsulamentoAltro();
		//IN PACKAGE DIVERSI SI VEDRA' SOLO IL PUBLIC
		//IL MODIFICATORE D'ACCESSO DEFAULT IN QUESTO CASO SI COMPORTERA' COME IL PRIVATE(PERCHE' IN PACKAGE DIVERSO)
		ta.a=40;
		System.out.println(ta.a);
		
		//EQUALS
		Persona p2 = new Persona("Mario","Rossi",32,true);
		Persona p3 = new Persona("Mario","Rossi",32,true);
		//SARA' TRUE SOLO SE NEL MODEL GENERIAMO GLI EQUALS
		System.out.println(p2.equals(p3));
		System.out.println(p2.equals(p2));
		System.out.println(p2.hashCode());
		System.out.println(p3.hashCode());
	}
}
