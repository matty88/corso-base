package model;

public class Dipendente extends Persona {
	
	private double stipendio;
	public String ruolo;
	
	public double getStipendio() {
		return stipendio;
	}
	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	
	
	public Dipendente() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Dipendente(String nome, String cognome, int eta, boolean sesso) {
		super(nome, cognome, eta, sesso);
		// TODO Auto-generated constructor stub
	}
	public Dipendente(double stipendio, String ruolo) {
		super();
		this.stipendio = stipendio;
		this.ruolo = ruolo;
	}
	
	@Override
	public String toString() {
		return "Dipendente [stipendio=" + stipendio + ", ruolo=" + ruolo + ", nome=" + nome + ", cognome=" + cognome
				+ ", eta=" + eta + ", sesso=" + sesso + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		long temp;
		temp = Double.doubleToLongBits(stipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dipendente other = (Dipendente) obj;
		if (ruolo == null) {
			if (other.ruolo != null)
				return false;
		} else if (!ruolo.equals(other.ruolo))
			return false;
		if (Double.doubleToLongBits(stipendio) != Double.doubleToLongBits(other.stipendio))
			return false;
		return true;
	}
	
	
	
	
}
