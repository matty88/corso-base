package model;

import java.util.List;

public interface IPersonaCrud {
	
	//MODELLI DEI METODI CHE VERRANNO IMPLEMENTATI NELLA CLASSE
	public boolean inserisci(Persona p);   //ABSTRACT
	
	public boolean modifica(Persona p);
	
	public boolean cancella(int id);
	
	public Persona leggi(int id);
	
	public List<Persona> leggi();
}
