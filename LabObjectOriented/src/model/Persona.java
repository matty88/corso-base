package model;

public class Persona {

	public String nome;
	public String cognome;
	public int eta;
	public boolean sesso;
	
	//IN QUESTO CASO SUPER.TOSTRING() STAMPERA' L'INDIRIZZO DEL PADRE(OBJECT)
	@Override
	public String toString() {
		return super.toString()+  ", nome=" + nome + ", cognome=" + cognome + ", eta=" + eta+ ", sesso="+sesso;
	}
		
//	//COSTRUTTORE DI DEFAULT CON VALORI INIZIALIZZATI
//	public Persona() {
//		nome = "";
//		cognome = "";
//		eta = 0;
//		sesso = true;
//	}
	
	public Persona () {}

	public Persona(String nome, String cognome, int eta, boolean sesso) {
		this.nome = nome;        //attributo(variabile d'istanza) = argomento (variabile locale)  //this si usa solo nel caso in cui argomenti e attributi hanno lo stesso nome
		this.cognome = cognome;  //this � riferimento della oggetto che andremo a creare (this.Persona.cognome)
		this.eta = eta;
		this.sesso = sesso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public boolean isSesso() {
		return sesso;
	}

	public void setSesso(boolean sesso) {
		this.sesso = sesso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + eta;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + (sesso ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (eta != other.eta)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sesso != other.sesso)
			return false;
		return true;
	}
}

//QUANDO SI CREA UN MODEL:
//TUTTI GLI ATTRIBUTI DEVONO ESSERE PRIVATI
//CI DEVONO ESSERE SET E GET PER OGNI ATTRIBUTO
//I METODI GET BOOLEANO PER CONVENZIONE INIZIANO PER is
//SOVRASCRIVERE TO STRING
//SOVRASCRIVERE HASHCODE & EQUALS
//HASHCODE CONTROLLA CONTENUTO DELL'OGGETTO, SE HANNO STESSO CONTENUTO TI RESTITUISCE UN NUMERO INTERO UGUALE
//COSTRUTTORE DI DEFAULT E PARAMETRIZZATO
