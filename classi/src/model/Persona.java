package model;
//LE CLASSI SONO CONTENITORI DI METODI O ATTRIBUTI E SERVONO PER DARE ORDINE
//PACKAGE E' UN CONTENITORE DI CLASSI
//IN JAVA ESISTONO DELLE CLASSI CHE CONTENGONO UN INSIEME DI VARIABILI CHIAMATE ATTRIBUTI
//NELL'ESEMPIO IN BASSO LA STRUTTURA CREATA PERMETTE DI CREARE VARIABILI CHE VENGONO PASSATE PER RIFERIMENTO
//IL TIPO DELLA CLASSE CORRISPONDE AD UN INDIRIZZO DI MEMORIA
//NELLE CLASSI NON VANNO MAI ESEGUITI I COMANDI CHE VANNO ESEGUITI SOLO NEI METODI

//-i nomi delle entit� sono sempre singolari
//-in un model gli attributi devono essere sempre privati
//-get e set per ogni attributo
//-tastodx > source > generate hashcode e equals > spunta tutte le voci 01.38
//-creare metodo toString()    //restituisce il concatenamento di tutti gli attributi
//-costruttore di default e parametrizzato

//hashcode confronta se due oggetti sono uguali
//split per stampare in verticale se gli attributi sono numerosi

//quando si crea un entit� deve necessariamente esistere pure un CRUD corrispondente
//il crud � un service(insieme di metodi) specializzato per gestire l'inserimento o repository
//crud di un entit� o di tutte le entit� quindi esiste sempre.
//Va messa nel model



public class Persona {
	 public String Nome;
	 public String Cognome;
	 public int et�;
	}
