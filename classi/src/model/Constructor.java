package model;

public class Constructor {
	//IN OGNI CLASS DI TIPO MODEL ESISTONO 2 METODI SPECIALI
	//CONSTRUCTOR(SENZA ARGOMENTI CHIAMATO DI DEFAULT CHE ESISTE SOLO PER POTER ISTANZIARE(NEW PERSONA())
	//E CON ARGOMENTI CHIAMATO PARAMETRIZZATO, MENTRE ISTANZI ASSEGNI VALORE AGLI ATTRIBUTI SENZA SET E GET
	//SAREBBE NEW QUANDO SI ISTANZIA UNA CLASSE
	
	//DI SOLITO HA LO STESSO NOME DELLA CLASSE
	
	//persone[i]=new Persona();   //SI ISTANZIA PER OGNI VARIABILE DEL VETTORE
	
	//le variabili per riferimento(quindi quelle non primitive) hanno come valore di default null
	//quindi vanno creato col new(costruttore)
	
	//p=new Persona() indirizzo posizione in cui � stato creato l'oggetto
    //	con new creo l'oggetto e l'indirizzo dell'oggetto viene assegnato a p
    //	il contenuto di questa variabile viene assegnato poi in ingresso ai vari metodi
	//Persona p1;
	//p1= new Persona();   // altro modo per instanziare una classe
	
	//Persona p1 = new Persona();
	//Persona p2 = new Persona();
	//Persona p3 = new Persona();
	//Persona[] pers = new Persona[3];    //� la stessa cosa
	//pers[0]=new Persona();
	//pers[1]=new Persona();
	//pers[2]=new Persona();
	
	
	
	
	
}
