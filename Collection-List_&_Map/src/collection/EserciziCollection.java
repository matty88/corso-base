package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import model.Persona;

public class EserciziCollection {

	public void eserciziListSet() {

		//LIST
		ArrayList<Persona>sList=new ArrayList<>();
		sList.add(new Persona("Mario","Commone",32));
		sList.add(new Persona("Giuseppe","Rossi",43));
		sList.add(new Persona("Mario","Commone",32));
		//		for(int i=0;i<sList.size();i++) {
		//			System.out.println(sList.get(i));
		//		}
		for(Persona p:sList) {                   //SCORRE L'ARRAYLIST SLIST ED OGNI INDIRIZZO LO ASSEGNA IN p
			System.out.println(p.toString());
		}

		//ORDINARE PER NOME CON SORT
		sList.sort((p1,p2) -> p1.getNome().compareTo(p2.getNome()));     
		for(Persona p:sList) {                 
			System.out.println(p.toString());
		}

		//ORDINARE PER ETA' CON SORT
		sList.sort((p1,p2) -> p1.getEta()-p2.getEta());        //COMPARETO INLINE
		for(Persona p:sList) {                  
			System.out.println(p.toString());
		}

		//ORDINARE PER COGNOME CON SORT
		//		Collections.sort(sList);                //SOLO IMPLEMENTANDO comparable<Persona> nella classe Persona col metodo compareTo(sempre nella classe)
		//		for(Persona p:sList) {                  
		//			System.out.println(p.toString());
		//		}

		//SET
		HashSet<Persona>pSet=new HashSet<Persona>(sList);      //CREO INSIEME DANDOGLI L'ARRAYLIST CREATO IN PRECEDENZA LUI ELIMINA TUTTI I DUPLICATI
		for(Persona p:pSet) {                  
			System.out.println(p.toString());
		}

		TreeSet<Persona>tSet=new TreeSet<Persona>(sList);      //CREO INSIEME DANDOGLI L'ARRAYLIST CREATO IN PRECEDENZA LUI ELIMINA TUTTI I DUPLICATI ORDINANDO IN ORDINE ALFABETICO
		for(Persona p:tSet) {                  
			System.out.println(p.toString());
		}		
	}
	
	//MAP
	public void eserciziHashMap() {
		HashMap<String,Persona> pMap = new HashMap<String,Persona>();      //A DIFFERENZA DELL ARRAYLIST LA CHIAVE LA INSERIAMO NOI  "CHIAVE" "VALORE"
		pMap.put("uno",new Persona("Alessandro","Rossi",50));
		pMap.put("uno",new Persona("Giuseppe","Verdi",40));
		pMap.put("uno",new Persona("Alessandro","Rossi",50));
		for(String key:pMap.keySet()) {    //keySet RESTITUISCE UN ARRAY DI TIPO SET CHE CONTIENE TUTTE LE CHIAVI ED ELIMINA QUELLE UGUALI
			System.out.println(pMap.get(key).toString());
		}
		for(Persona p:pMap.values()) {    //RESTITUISCE I VALORI ELIMINANDO COMUNQUE UNA CHIAVE SE UGUALE AD UN'ALTRA
			System.out.println(p);
		}

	}


}
