package controller;

import java.util.ArrayList;
import model.Persona;
import view.Metod;

public class EserciziCRUD {
	Metod m = new Metod();

	public void gestisciPersoneVettore() {
		int insPer = m.leggiIntero("Quanti utenti vuoi inserire? ");
		Persona[] p = new Persona[insPer];
		for(int i=0;i<insPer;i++) {
			p[i] = new Persona();
			p[i].setNome(m.leggiStringa("Inserisci nome persona: "+(i+1)+": "));
			p[i].setCognome(m.leggiStringa("Inserisci cognome persona: "+(i+1)+": "));
			p[i].setEta(m.leggiIntero("Inserisci et� persona: "+(i+1)+": "));
		}
		for(int i=0;i<p.length;i++) {
			//System.out.println((i+1)+")"+p[i].toString());
			System.out.println("\n"+(i+1)+")"+"Nome: "+p[i].getNome());
			System.out.println((i+1)+")"+"Cognome: "+p[i].getCognome());
			System.out.println((i+1)+")"+"Et�: "+p[i].getEta());
		}
	}	

	public void gestisciPersoneArray() {
		ArrayList<Persona> persone = new ArrayList<Persona>();
		String resp;
		int i=0;
		do {	
			persone.add(new Persona());
			persone.get(i).setNome(m.leggiStringa("Inserisci nome persona "+(i+1)+": "));
			persone.get(i).setCognome(m.leggiStringa("Inserisci cognome persona "+(i+1)+": "));
			persone.get(i).setEta(m.leggiIntero("Inserisci et� persona "+(i+1)+": "));
			resp=m.leggiStringa("Vuoi inserire una nuova persona (S/N)? ");
			i++;
		}while(resp.equalsIgnoreCase("si") || resp.equalsIgnoreCase("s"));
		for(i=0;i<persone.size();i++) {
			System.out.println("\n"+(i+1)+")"+"Nome: "+persone.get(i).getNome());
			System.out.println((i+1)+")"+"Cognome: "+persone.get(i).getCognome());
			System.out.println((i+1)+")"+"Et�: "+persone.get(i).getEta());
		}
	}
}
