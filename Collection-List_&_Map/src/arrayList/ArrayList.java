package arrayList;

public class ArrayList {
	//COME SE FOSSE UN VETTORE CON LA DIFFERENZA CHE IL VETTORE E' A DIMENSIONE FISSA[5] MENTRE
	//L'ARRAYLIST E' UN VETTORE DINAMICO
	
	//ArrayList<Persona> persone = new ArrayList<Persona>(); 
	//ArrayList<Persona> persone = new ArrayList<>();           //DA JAVA 8 IN POI
	//metodo(List<Persona>persone) {}
	
	//PERSONA E' IL TIPO MA POTREBBE ESSERE INT DOUBLE ETC.
	
	//ArrayList<Persona> listaPersone;     //rif #43fwd       creiamo una variabile che contiene l'indirizzo di un array di tipo persona
	//listaPersone.add(new Persona());           //crea oggetto e aggiunge variabile al vettore conservando l'indirizzo
	//listaPersone.add(new Persona());
	//listaPersone.add(new Persona());
	//listaPersone.get(0).setNome("mario");
	//listaPersone.get(1).setNome("mario");
	//listaPersone.get(2).setNome("mario");
	//listaPersone.remove(1);              //il 2 diventa 1 perch� cancelliamo l'indirizzo di quello precedente(che per� in memoria continuer� ad esistere)
	//Persona miaPersona;
	//miaPersona=listaPersone.get(1);
	//miaPersona.getNome()              //visualizzerebbe comunque l'oggetto restato in moemoria

	//se l'oggetto non � associato a nessuna variabile viene eliminato totalmente dal garbage collector
	
//	il repository(dove finiscono gli indirizzi degli oggetti creati con new) di un app di solito � l'arraylist o una tabella
//	arraylist � una raccolta degli indirizzi degli oggetti
	
	//non c'� bisogno di indicizzare con gli arraylist
	
	//ArrayList<Persona> persone = variabile che conserva l'indirizzo dell arraylist
}
