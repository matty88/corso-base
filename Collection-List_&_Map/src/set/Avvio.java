package set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Avvio {

	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		set.add("cognome");
		set.add("cognome");
		System.out.println("***HASHSET***");
		set.forEach(System.out::println);

		List<String> list = new ArrayList<>();
		list.add("cognome");
		list.add("cognome");
		System.out.println("***ARRAYLIST***");
		list.forEach(System.out::println);
	}
}
