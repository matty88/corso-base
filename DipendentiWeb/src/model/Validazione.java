package model;

public class Validazione {
	
	public boolean obbligatorio(String valore) {
		return !valore.isEmpty();
	}

	public boolean intero(String valore, int cifre) {
		return valore.matches("\\d{1,"+cifre+"}");
	}
	
	public boolean decimale(String valore, int cifre) {
		return valore.matches("\\d{1,"+cifre+"}[.]\\d{2}");
	}
	
	public boolean data(String valore) {
		return valore.matches("0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d");
	}
	
	public boolean codiceFiscale(String valore) {
		return valore.matches("[A-Z]{6}\\d{2}[A-Z]\\d{2}[A-Z]\\d{3}[A-Z]");
	}
}