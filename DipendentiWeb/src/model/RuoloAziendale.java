package model;

public class RuoloAziendale {
	private int id;
	private String ruoloAziendale;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRuoloAziendale() {
		return ruoloAziendale;
	}
	public void setRuoloAziendale(String ruoloAziendale) {
		this.ruoloAziendale = ruoloAziendale;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((ruoloAziendale == null) ? 0 : ruoloAziendale.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuoloAziendale other = (RuoloAziendale) obj;
		if (id != other.id)
			return false;
		if (ruoloAziendale == null) {
			if (other.ruoloAziendale != null)
				return false;
		} else if (!ruoloAziendale.equals(other.ruoloAziendale))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return id + "," + ruoloAziendale;
	}
	public RuoloAziendale() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RuoloAziendale(int id, String ruoloAziendale) {
		super();
		this.id = id;
		this.ruoloAziendale = ruoloAziendale;
	}

	
}
