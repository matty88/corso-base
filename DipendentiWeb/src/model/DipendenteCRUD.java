package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class DipendenteCRUD {

	public int getTotalePagine(int lunghezzaPagina) {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		int record=-1;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         
			String sql="select count(*) as record from dipendenti";      
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rs=ps.executeQuery();
			if(rs.first()) {
				record=rs.getInt("record");
			}
			ps.close();
			rs.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura numero record!"+e.getMessage());
			return record;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return record;
		}
		return (int)Math.ceil((double)record/lunghezzaPagina);
	}

	public boolean inserisci(Dipendente d) {
		//E' UN OGGETTO RESTITUITO DA UN METODO DELLA CLASSE CONNECTION CHE PERMETTE LA SCRITTURA DI UN'OPERAZIONE SQL
		//CONTIENE IL COMANDO CHE DOVRA' ESSERE ESEGUITO SUL DATABASE
		//CONTROLLA CHE NON VENGA INSERITO DEL CODICE MALEVOLO
		//A DIFFERENZA DELLO STATEMENT CHE NON E' PARAMETRIZZABILE(non permette di fare operazioni di set)//STATEMENT SI USA QUANDO NON HO ? OVVERO QUANDO L'OPERAZIONE SUL DB NON E' MODIFICABILE DALL'UTENTE
		PreparedStatement ps = null;
		//L'OGGETTO CONNECTION E' IL RISULTATO DELL'APERTURA DELLA CONNESSIONE VERSO IL DB MEDIANTE IL DRIVER
		Connection conn = null;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="insert into dipendenti (email,password,nome,cognome,luogo_di_nascita,data_di_nascita,sesso,stipendio,codice_fiscale,id_titolo_di_studio,id_ruolo_aziendale) values (?,?,?,?,?,?,?,?,?,?,?)";
			ps=conn.prepareStatement(sql);
			//SOSTITUISCE IL VALORE RESTITUITO DAL METODO GETEMAIL COL PRIMO ?/PARAMETER DELLA STRINGA IN ALTO
			ps.setString(1,d.getEmail());
			ps.setString(2,d.getPassword());
			ps.setString(3,d.getNome());
			ps.setString(4,d.getCognome());
			ps.setString(5,d.getLuogoDiNascita());
			ps.setDate(6,d.getDataDiNascita());
			ps.setString(7,d.getSesso());
			ps.setDouble(8,d.getStipendio());
			ps.setString(9,d.getCodiceFiscale());
			ps.setInt(10,d.getIdTitoloDiStudio());
			ps.setInt(11,d.getIdRuolo());
			//ESEGUE L'OPERAZIONE SUL DATABASE
			ps.executeUpdate();
			//CHIUDE L'OGGETTO PREPARE STATEMENT
			ps.close();	
		}catch(SQLException e) {
			System.out.println("Errore d'inserimento! "+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		} 
//		finally {	
//			if (ps != null) {
//				try {
//					ps.close();
//				} catch (SQLException e) {}
//			}
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {}
//			}
//		}
		return true;
	}

	public boolean modifica(int id, Dipendente d) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="update dipendenti set email=?,password=?,nome=?,cognome=?,luogo_di_nascita=?,data_di_nascita=?,sesso=?,stipendio=?,codice_fiscale=?,id_titolo_di_studio=?,id_ruolo_aziendale=? where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,d.getEmail());
			ps.setString(2,d.getPassword());
			ps.setString(3,d.getNome());
			ps.setString(4,d.getCognome());
			ps.setString(5,d.getLuogoDiNascita());
			ps.setDate(6,d.getDataDiNascita());
			ps.setString(7,d.getSesso());
			ps.setDouble(8,d.getStipendio());
			ps.setString(9,d.getCodiceFiscale());
			ps.setInt(10,d.getIdTitoloDiStudio());
			ps.setInt(11,d.getIdRuolo());
			ps.setInt(12,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di modifica!"+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public boolean cancella(int id) {
		// TODO Auto-generated method stub
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="delete from dipendenti where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di cancellazione!");
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public Dipendente leggi(int id) {
		//E' IL RISULTATO DELL'EXECUTE QUERY. E' UN OGGETTO CHE CONTIENE IL RISULTATO DELLA QUERY
		ResultSet rs=null;
		Dipendente dip=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			//*=ALL
			String sql="select * from dipendenti where id=?";
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1,id);
			rs=ps.executeQuery();
			if(rs.first()){
				dip=new Dipendente(
						//ATTRAVERSO I METODI IMPERATIVI FIRST E NEXT POSSO SCORRERE(IN CASO DI NEXT) O PRENDO IL PRIMO RISULTATO(IN CASO DI FIRST)
						//IL RISULTATO DELLA QUERY ED INTERCETTARE LE SINGOLE COLONNE MEDIANTE UN GET COL LORO NOME
						rs.getInt("id"), 
						rs.getString("email"),
						rs.getString("password"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo_di_nascita"), 
						rs.getDate("data_di_nascita"), 					
						rs.getString("sesso"), 
						rs.getString("codice_fiscale"), 
						rs.getDouble("stipendio"), 						
						rs.getInt("id_titolo_di_studio"),
						rs.getInt("id_ruolo_aziendale"));
			}
			rs.close();
			ps.close(); 
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dip;
	}

	public ArrayList<Dipendente> leggi() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		Dipendente dip;
		ArrayList<Dipendente> dipendenti=new ArrayList<Dipendente>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from dipendenti";
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new Dipendente(
						//RS E' CONTENITORE DELLE RIGHE RISULTATO DELL'ESECUZIONE DELLA QUERY
						rs.getInt("id"), 
						rs.getString("email"),
						rs.getString("password"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo_di_nascita"), 
						rs.getDate("data_di_nascita"), 					
						rs.getString("sesso"), 
						rs.getString("codice_fiscale"), 
						rs.getDouble("stipendio"), 						
						rs.getInt("id_titolo_di_studio"),
						rs.getInt("id_ruolo_aziendale"));
				dipendenti.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dipendenti;
	}

	public int contaDipendenti() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		int count=0;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select count(*) from dipendenti";
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				count = rs.getInt(1);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
		}
		return count;
	}

	public ArrayList<TitoloDiStudio> leggiTds() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		TitoloDiStudio tds;
		ArrayList<TitoloDiStudio> aTds=new ArrayList<TitoloDiStudio>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from titoli_di_studio";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				tds=new TitoloDiStudio(
						rs.getInt("id"), 
						rs.getString("titolo_di_studio"));
				aTds.add(tds);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return aTds;
	}

	public ArrayList<RuoloAziendale> leggiRuoli() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		RuoloAziendale ra;
		ArrayList<RuoloAziendale> aRa=new ArrayList<RuoloAziendale>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from ruoli_aziendali";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				ra=new RuoloAziendale(
						rs.getInt("id"), 
						rs.getString("ruolo_aziendale"));
				aRa.add(ra);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return aRa;
	}

	public ArrayList<DipendenteDTO> leggiDTO() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		DipendenteDTO dip;
		ArrayList<DipendenteDTO> dipendenti=new ArrayList<DipendenteDTO>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from v_dipendenti";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new DipendenteDTO(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo di nascita"), 
						rs.getString("data di nascita"), 
						rs.getString("sesso"), 
						rs.getDouble("stipendio"), 
						rs.getString("codice fiscale"), 
						rs.getString("titolo di studio"),
						rs.getString("ruolo aziendale"));
				dipendenti.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dipendenti;
	}

	public ArrayList<DipendenteDTO> leggiPagina(int lunghezzaPagina, int paginaCorrente) {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		DipendenteDTO dip;
		int offSet=0;
		ArrayList<DipendenteDTO> dipendenti=new ArrayList<DipendenteDTO>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from v_dipendenti order by id asc limit ? offset ?";
			ps=conn.prepareStatement(sql);
			offSet=(paginaCorrente-1)*lunghezzaPagina;
			ps.setInt(1, lunghezzaPagina);
			ps.setInt(2, offSet);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new DipendenteDTO(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo di nascita"), 
						rs.getString("data di nascita"), 
						rs.getString("sesso"), 
						rs.getDouble("stipendio"), 
						rs.getString("codice fiscale"), 
						rs.getString("titolo di studio"),
						rs.getString("ruolo aziendale"));
				dipendenti.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dipendenti;
	}

	public Dipendente login(Dipendente dip) {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		Dipendente d=null;
		try {
			conn=ConnettoreDB.getDBDipendenti();		
			String sql="select * from dipendenti where email=? and password=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, dip.getEmail());
			ps.setString(2, dip.getPassword());
			rs=ps.executeQuery();
			if(rs.next()) {
				d=new Dipendente();
				d.setEmail(rs.getString("email"));
				d.setNome(rs.getString("nome"));
				d.setCognome(rs.getString("cognome"));
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
		}
		return d;
	}

}