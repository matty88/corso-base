package model;

import java.sql.Date;

public class Dipendente extends Persona {
	
	private double stipendio;
	private int idTitoloDiStudio;
	private int idRuolo;
	
	public double getStipendio() {
		return stipendio;
	}
	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	public int getIdTitoloDiStudio() {
		return idTitoloDiStudio;
	}
	public void setIdTitoloDiStudio(int idTitoloDiStudio) {
		this.idTitoloDiStudio = idTitoloDiStudio;
	}
	public int getIdRuolo() {
		return idRuolo;
	}
	public void setIdRuolo(int idRuolo) {
		this.idRuolo = idRuolo;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", "+stipendio+ ", "
				+ idTitoloDiStudio + ", " + idRuolo;
	}
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + idRuolo;
		result = prime * result + idTitoloDiStudio;
		long temp;
		temp = Double.doubleToLongBits(stipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dipendente other = (Dipendente) obj;
		if (idRuolo != other.idRuolo)
			return false;
		if (idTitoloDiStudio != other.idTitoloDiStudio)
			return false;
		if (Double.doubleToLongBits(stipendio) != Double.doubleToLongBits(other.stipendio))
			return false;
		return true;
	}
	public Dipendente(int id, String email, String password, String nome, String cognome, String luogoDiNascita,
			Date dataDiNascita, String sesso, String codiceFiscale, double stipendio, int idTitoloDiStudio,
			int idRuolo) {
		super(id, email, password, nome, cognome, luogoDiNascita, dataDiNascita, sesso, codiceFiscale);
		this.stipendio = stipendio;
		this.idTitoloDiStudio = idTitoloDiStudio;
		this.idRuolo = idRuolo;
	}
	public Dipendente() {
		super();
		// TODO Auto-generated constructor stub
	}


}
