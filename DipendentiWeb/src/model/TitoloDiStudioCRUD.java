package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TitoloDiStudioCRUD {
	
	public int inserisci(TitoloDiStudio tds) {
		int titoloDiStudioId = -1;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="insert into titoli_di_studio (titolo_di_studio) values (?)";
			ps=conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,tds.getTitoloDiStudio());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
			titoloDiStudioId = rs.getInt(1);
			}
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore d'inserimento! "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
		}
		return titoloDiStudioId;
	}

	public boolean modifica(int id, TitoloDiStudio tds) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="update titoli_di_studio set titolo_di_studio=? where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,tds.getTitoloDiStudio());
			ps.setInt(2,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di modifica!"+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public boolean cancella(int id) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="delete from titoli_di_studio where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di cancellazione!");
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public TitoloDiStudio leggi(int id) {
		ResultSet rs=null;
		TitoloDiStudio tds=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from titoli_di_studio where id=?";
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1,id);
			rs=ps.executeQuery();
			if(rs.first()){
				tds=new TitoloDiStudio(
						rs.getInt("id"), 
						rs.getString("titolo_dI_studio"));
			}
			rs.close();
			ps.close(); 
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return tds;
	}

	public ArrayList<TitoloDiStudio> leggi() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		TitoloDiStudio tds;
		ArrayList<TitoloDiStudio> titoli=new ArrayList<TitoloDiStudio>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from titoli_di_studio";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				tds=new TitoloDiStudio(
						rs.getInt("id"),
						rs.getString("titolo_di_studio"));
				titoli.add(tds);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return titoli;
	}
}
