package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnettoreDB {
		private static Connection conn=null;
		public static Connection getDBDipendenti() {
		     try {
		          if(conn==null) {
		            Class.forName("com.mysql.cj.jdbc.Driver");
		            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_dipendenti?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
		            }
		         } catch(ClassNotFoundException e){
		             e.printStackTrace();
		         } catch(SQLException e) {
		             e.printStackTrace();
		         }
		     return conn;
		   }
		private ConnettoreDB(){}
		}
