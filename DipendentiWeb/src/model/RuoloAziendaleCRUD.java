package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RuoloAziendaleCRUD {

	public int inserisci(RuoloAziendale ra) {
		int ruoloAziendaleId = -1;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="insert into ruoli_aziendali (ruolo_aziendale) values (?)";
			ps=conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,ra.getRuoloAziendale());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				ruoloAziendaleId = rs.getInt(1);
				}
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore d'inserimento! "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
		}
		return ruoloAziendaleId;
	}

	public boolean modifica(int id, RuoloAziendale ra) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="update ruoli_aziendali set ruolo_aziendale=? where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,ra.getRuoloAziendale());
			ps.setInt(2,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di modifica!"+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public boolean cancella(int id) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="delete from ruoli_aziendali where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di cancellazione!");
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public RuoloAziendale leggi(int id) {
		ResultSet rs=null;
		RuoloAziendale ra=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from ruoli_aziendali where id=?";
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1,id);
			rs=ps.executeQuery();
			if(rs.first()){
				ra=new RuoloAziendale(
						rs.getInt("id"), 
						rs.getString("ruolo_aziendale"));
			}
			rs.close();
			ps.close(); 
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return ra;
	}

	public ArrayList<RuoloAziendale> leggi() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		RuoloAziendale ra;
		ArrayList<RuoloAziendale> ruoli=new ArrayList<RuoloAziendale>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from ruoli_aziendali";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				ra=new RuoloAziendale(
						rs.getInt("id"),
						rs.getString("ruolo_aziendale"));
				ruoli.add(ra);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return ruoli;
	}
}
