package controller;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Dipendente;
import model.DipendenteCRUD;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(
		filterName="login"
		)
public class LoginFilter implements Filter {

	/**
	 * Default constructor. 
	 */
	public LoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//String path = ((HttpServletRequest) request).getRequestURI();
		DipendenteCRUD dc = new DipendenteCRUD();
		Dipendente d = null;
		Dipendente userFromDB = null;
		RequestDispatcher view = null;
//		if (path.startsWith("/logout.html/")) {
//			chain.doFilter(request, response);
//		} else {
			//RECUPERO LA SESSIONE DALLA REQUEST. IL PARAMETRO FALSE SERVE PER NON FAR APRIRE UNA
			//NUOVA CONNESSIONE NEL CASO IN CUI QUESTA NON CI FOSSE
			//QUANDO METTO TRUE CONTROLLA SE C'E' GIA' UNA SESSIONE E LA RITORNA SE C'E', SE NON C'E' LA CREA
			//IL FALSE CONTROLLA SE C'E' LA SESSIONE, SE NON C'E' RITORNA NULLA
			HttpSession sessione = ((HttpServletRequest) request).getSession(false);
			if(sessione !=null && sessione.getAttribute("user") != null) {
				//SE C'E' SESSIONE ED AL SUO INTERNO C'E' UN OGEGTTO USER SIGNIFICA CHE L'UTENTE
				//E' GIA' LOGGATO. DI CONSEGUENZA RINNOVO IL TEMPO DELLA SESSIONE PER EVITARE
				//CHE SCADA
				sessione.setMaxInactiveInterval(1800);
			} else {
				d=new Dipendente();
				d.setEmail(request.getParameter("email"));
				d.setPassword(request.getParameter("password"));
				//D E' SOLO CONTENITORE DI USER E PASSWORD CHE VENGONO DAI PARAMETER E MI SERVE
				//PER FARE OPERAZIONE DI LETTURA SUL DATABASE MA NULLA MI AVREBBE IMPEDITO
				//INVECE DI DI DI USARE SOLO USERNAME E PASSWORD
				if(d.getEmail() !=null && !d.getEmail().isEmpty()) {
					//ALTRIMENTI VEDIAMO SE L'UTENTE CERCA DI LOGGARSI.
					//SE STA CERCANDO DI LOGGARSI SIGNIFICA CHE HA ISNERITO USERNAME E PASSWORD
					//SE HA INSERITO USER E PASS E QUESTE ESISTONO SUL DATABASE OTTENIAMO USERFROM DB
					//CHE CONTROLLA SUL DB SE ESISTE L'UTENTE
					userFromDB = dc.login(d);	
					//USERFROMDB E' L'UTENTE LETTO SUL DATABASE ATTRAVERSO USERNAME E PASSWORD
					//E' SALVATO NELLA SESSIONE DELL'UTENTE LOGGATO E NE CONTIENE LE INFORMAZIONI
					//COME NOME, COGNOME, MAIL ETC.
				}
				//SE UTENTE ESISTE
				if(userFromDB !=null) {
					//SALVIAMO L'OGGETTO USERFROMDB SULLA SESSIONE CORRISPONDENTE ALLA CHIAVE USER
					sessione = ((HttpServletRequest) request).getSession();
					sessione.setAttribute("user", userFromDB);
					sessione.setMaxInactiveInterval(1800);
				} else {
					//ALTRIMENTI REINDIRIZZIAMO L'UTENTE SULLA LOGIN
					view=request.getRequestDispatcher("login.jsp");
					view.forward(request, response);
					return;
				}
			}		
		chain.doFilter(request, response);
	}


	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
