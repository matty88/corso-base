package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Dipendente;
import model.DipendenteCRUD;
import model.RuoloAziendale;
import model.RuoloAziendaleCRUD;
import model.TitoloDiStudio;
import model.TitoloDiStudioCRUD;
import model.Validazione;

/**
 * Servlet implementation class GestioneDipendenti
 */
@WebServlet("/dipendenti")
public class GestioneDipendenti extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestioneDipendenti() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		DipendenteCRUD dc = new DipendenteCRUD();
		TitoloDiStudioCRUD tc = new TitoloDiStudioCRUD();
		RuoloAziendaleCRUD rc = new RuoloAziendaleCRUD();
		Dipendente d = null;
		RequestDispatcher view = null;
		int scelta=0, id=0, titoloDiStudio=0, ruolo=0;
		int lunghezzaPagina = 10;
		int paginaCorrente = 1;
		int operazione;
		ArrayList<String> errori = new ArrayList<String>();
		Validazione v = new Validazione();
		HttpSession sessione=null;
		sessione=request.getSession();
		if(sessione != null && sessione.getAttribute("lunghezzaPagina") == null) {
			sessione.setAttribute("lunghezzaPagina",10);
			sessione.setAttribute("paginaCorrente",1);
		}
		scelta=Integer.parseInt(request.getParameter("scelta"));
		switch(scelta) {
		case 1:
			request.setAttribute("titoli", dc.leggiTds());
			request.setAttribute("ruoli", dc.leggiRuoli());
			view=request.getRequestDispatcher("scheda_dipendenti.jsp");
			view.forward(request, response);
			break;
		case 2:
			id=Integer.parseInt(request.getParameter("id"));
			d=dc.leggi(id);
			request.setAttribute("titoli", dc.leggiTds());
			request.setAttribute("ruoli", dc.leggiRuoli());
			request.setAttribute("dipendente",d);
			view=request.getRequestDispatcher("scheda_dipendenti_mod.jsp");
			view.forward(request, response);
			break;
		case 3:
			id=Integer.parseInt(request.getParameter("id"));
			dc.cancella(id);
			response.sendRedirect("cancellazione.html");
			break;
		case 4:
			id=Integer.parseInt(request.getParameter("id"));
			d=dc.leggi(id);
			request.setAttribute("titoli", dc.leggiTds());
			request.setAttribute("ruoli", dc.leggiRuoli());
			request.setAttribute("dipendente",d);
			view=request.getRequestDispatcher("dettaglio_utente.jsp");
			view.forward(request, response);
			break;
//		case 5:
//			lunghezzaPagina=(Integer)sessione.getAttribute("lunghezzaPagina");
//			request.setAttribute("dipendenti", dc.leggiPagina(lunghezzaPagina,1));
//			request.setAttribute("maxPagina", dc.contaDipendenti());
//			view=request.getRequestDispatcher("visualizza_dipendenti.jsp");
//			view.forward(request, response);
//			break;
		case 6:	
			d=new Dipendente();
			if(v.obbligatorio(request.getParameter("email"))) {
				d.setEmail(request.getParameter("email"));
			} else {
				errori.add("Errore: indirizzo email obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("password"))) {
				d.setPassword(request.getParameter("password"));
			} else {
				errori.add("Errore: password obbligatoria!");
			}
			if(v.obbligatorio(request.getParameter("nome"))) {
				d.setNome(request.getParameter("nome"));
			} else {
				errori.add("Errore: nome obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("cognome"))) {
				d.setCognome(request.getParameter("cognome"));
			} else {
				errori.add("Errore: cognome obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("luogo_nascita"))) {
				d.setLuogoDiNascita(request.getParameter("luogo_nascita"));
			} else {
				errori.add("Errore: luogo di nascita obbligatorio!");
			}
			d.setDataDiNascita(Date.valueOf((request.getParameter("data_nascita"))));
			if(v.codiceFiscale(request.getParameter("codice_fiscale"))) {
				d.setCodiceFiscale(request.getParameter("codice_fiscale"));
			} else {
				errori.add("Errore: codice fiscale non valido!");
			}
			if(v.decimale(request.getParameter("stipendio"),4)) {
				d.setStipendio(Double.parseDouble(request.getParameter("stipendio")));
			} else {
				errori.add("Errore: stipendio non inserito correttamente!");
			}
			d.setSesso(request.getParameter("sesso"));
			titoloDiStudio=Integer.parseInt(request.getParameter("titolo_studio"));
			if(titoloDiStudio==-1) {
				errori.add("Errore: scegli un titolo di studio!");
			}		
			else {
				d.setIdTitoloDiStudio(titoloDiStudio);
			}
			ruolo=Integer.parseInt(request.getParameter("ruolo_aziendale"));
			if(ruolo==-1) {
				errori.add("Errore: scegli un ruolo aziendale!");
			} else {
				d.setIdRuolo(ruolo);
			}

			if(errori.size()==0) {
				if(titoloDiStudio==0) {
					TitoloDiStudio nuovoTitolo = new TitoloDiStudio();
					nuovoTitolo.setTitoloDiStudio(request.getParameter("nuovo_titolo"));			
					d.setIdTitoloDiStudio(tc.inserisci(nuovoTitolo));
				}
				if(ruolo==0) {
					RuoloAziendale nuovoRuolo = new RuoloAziendale();
					nuovoRuolo.setRuoloAziendale(request.getParameter("nuovo_ruolo"));			
					d.setIdRuolo(rc.inserisci(nuovoRuolo));
				}
				dc.inserisci(d);
				response.sendRedirect("inserimento.html");
			} else {
				request.setAttribute("titoli", dc.leggiTds());
				request.setAttribute("ruoli", dc.leggiRuoli());
				request.setAttribute("errori", errori);
				view=request.getRequestDispatcher("scheda_dipendenti.jsp");
				view.forward(request, response);
			}
			break;
		case 7:
			d=new Dipendente();
			d.setId(Integer.parseInt(request.getParameter("id")));
			if(v.obbligatorio(request.getParameter("email"))) {
				d.setEmail(request.getParameter("email"));
			} else {
				errori.add("Errore: indirizzo email obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("password"))) {
				d.setPassword(request.getParameter("password"));
			} else {
				errori.add("Errore: password obbligatoria!");
			}
			if(v.obbligatorio(request.getParameter("nome"))) {
				d.setNome(request.getParameter("nome"));
			} else {
				errori.add("Errore: nome obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("cognome"))) {
				d.setCognome(request.getParameter("cognome"));
			} else {
				errori.add("Errore: cognome obbligatorio!");
			}
			if(v.obbligatorio(request.getParameter("luogo_nascita"))) {
				d.setLuogoDiNascita(request.getParameter("luogo_nascita"));
			} else {
				errori.add("Errore: luogo di nascita obbligatorio!");
			}
			d.setDataDiNascita(Date.valueOf((request.getParameter("data_nascita"))));
			if(v.codiceFiscale(request.getParameter("codice_fiscale"))) {
				d.setCodiceFiscale(request.getParameter("codice_fiscale"));
			} else {
				errori.add("Errore: codice fiscale non valido!");
			}
			if(v.decimale(request.getParameter("stipendio"),4)) {
				d.setStipendio(Double.parseDouble(request.getParameter("stipendio")));
			} else {
				errori.add("Errore: stipendio non inserito correttamente!");
			}
			d.setSesso(request.getParameter("sesso"));
			titoloDiStudio=Integer.parseInt(request.getParameter("titolo_studio"));			
			if(titoloDiStudio==-1) {
				errori.add("Errore: scegli un titolo di studio!");
			}		
			else {
				d.setIdTitoloDiStudio(titoloDiStudio);
			}
			ruolo=Integer.parseInt(request.getParameter("ruolo_aziendale"));
			if(ruolo==-1) {
				errori.add("Errore: scegli un ruolo aziendale!");
			} else {
				d.setIdRuolo(ruolo);
			}
			if(errori.size()==0) {
				if(titoloDiStudio==0) {
					TitoloDiStudio nuovoTitolo = new TitoloDiStudio();
					nuovoTitolo.setTitoloDiStudio(request.getParameter("nuovo_titolo"));			
					d.setIdTitoloDiStudio(tc.inserisci(nuovoTitolo));
				}
				if(ruolo==0) {
					RuoloAziendale nuovoRuolo = new RuoloAziendale();
					nuovoRuolo.setRuoloAziendale(request.getParameter("nuovo_ruolo"));			
					d.setIdRuolo(rc.inserisci(nuovoRuolo));
				}
				dc.modifica(d.getId(),d);
				response.sendRedirect("inserimento.html");
			} else {
				request.setAttribute("titoli", dc.leggiTds());
				request.setAttribute("ruoli", dc.leggiRuoli());
				request.setAttribute("errori", errori);
				view=request.getRequestDispatcher("scheda_dipendenti_mod.jsp");
				view.forward(request, response);
			}
//			dc.modifica(d.getId(),d);
//			response.sendRedirect("inserimento.html");
			break;
		case 8:
			String operazioneParam = request.getParameter("operazione");
			//SE OPERAZIONEPARAM E' DIVERSO DA NULL ALLORA CONVERTO OPERAZIONEPARAM IN INTERO 
			//ALTRIMENTI LO SETTO A 0
			operazione = operazioneParam !=null ? Integer.parseInt(operazioneParam):0;  //CONVERSIONE OVVERO PRENDE UN OGGETTO DI UN TIPO E LO CONVERTE IN UN ALTRO TIPO
			if(sessione != null) {
			//IL CAST DA PER SCONTATO CHE IL TIPO SIA IN QUESTO CASO INTERO E LO VA AD UTILIZZARE
			lunghezzaPagina=(Integer)sessione.getAttribute("lunghezzaPagina");
			paginaCorrente=(Integer)sessione.getAttribute("paginaCorrente");
			}
			switch(operazione) {
			case 1:
				paginaCorrente = 1;
				break;
			case 2:
				if(paginaCorrente>1) {
					paginaCorrente--;				
				}
				break;
			case 3:
				if(paginaCorrente<dc.getTotalePagine(lunghezzaPagina)) {
					paginaCorrente++;			
				}
				break;
			case 4:
				paginaCorrente = dc.getTotalePagine(lunghezzaPagina);
				break;
			case 5:
				String sizePagina = request.getParameter("sizePagina");
				if(sizePagina !=null) {
					lunghezzaPagina = Integer.parseInt(request.getParameter("sizePagina"));
					sessione.setAttribute("lunghezzaPagina", lunghezzaPagina);
				}
				//OGNI VOLTA CHE CAMBIO IL SIZE DELLA PAGINA LUI RIPARTE DALLA PRIMA PAGINA
				//QUINDI IMPOSTO LA PAGINA CORRENTE A 1
				paginaCorrente = 1;		
				break;
			}
			sessione.setAttribute("paginaCorrente", paginaCorrente);
			request.setAttribute("dipendenti", dc.leggiPagina(lunghezzaPagina,paginaCorrente));
			sessione.setAttribute("maxPagina", dc.contaDipendenti());
			view=request.getRequestDispatcher("visualizza_dipendenti.jsp");
			view.forward(request, response);
			break;
		case 9:
			sessione.invalidate();
			response.sendRedirect("login.jsp");
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
