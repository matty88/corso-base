<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accedi</title>
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="content">
		<form class="form-signin" method="post">
			<h1 class="h3 mb-3 font-weight-normal accedi"><i class="fa fa-sign-in" aria-hidden="true"></i>LOGIN</h1>
			<label for="email" class="sr-only">Email</label> <input type="email"
				id="email" name="email" class="form-control"
				placeholder="&#xf0e0 Email" required autofocus> <label
				for="password" class="sr-only">Password</label> <input
				type="password" id="password" name="password" class="form-control"
				placeholder="&#xf084 Password" required>

			<div class="checkbox mb-3">
				<label class="remember-me"> <input type="checkbox"
					value="remember-me" checked> Mantieni l'accesso
				</label>
			</div>
			<div class="actions">
				<button class="btn btn-primary" type="submit">Login</button>

			</div>
		</form>
	</div>


	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.content {
	z-index: 1;
	transform: translate(-50%, -50%) matrix(1, 0, 0, 1, 0, 0);
	width: 400px;
	height: auto;
	position: absolute;
	left: 50%;
	top: 40%;
	/*border-radius: 2%;
	border: 1px solid white;
	background: rgba(255, 255, 255, 0); 
	padding: 30px;*/
}

input {
	margin: 10px 0 5px;
	font-family: FontAwesome, 'Montserrat', sans-serif;
	font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
}

.fa {
	margin-right: 10px;
}

.accedi {
	text-align: center;
	font-size: 15px;
}

.actions button {
	font-size: 12px;
	text-transform: uppercase;
}

.form-control, .remember-me {
	font-size: 13px !important;
}

.elenco {
	float: right;
}
</style>


	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
</body>
</html>