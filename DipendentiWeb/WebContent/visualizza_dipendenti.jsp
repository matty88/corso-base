<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.util.ArrayList, model.DipendenteDTO, model.Dipendente"%>
<!DOCTYPE html>
<html>
<head>
<script>
function conferma(id) {
	if(confirm("Confermi la cancellazione?")) {
		location.href="dipendenti?scelta=3&id="+id;
		return true;
	}
	return false;
}
</script>

<meta charset="ISO-8859-1">
<title>Visualizza dipendenti</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script>
$(function(){
$('#sizePagina').on('change', function () {
var size = $(this).val(); 
if (size) { 
window.location = 'dipendenti?scelta=8&operazione=5&sizePagina='+size; 
}
return false;
});
});
</script>
</head>
<body>
	<nav class="navbar navbar-dark bg-dark">
		<a href="dipendenti?scelta=8" class="navbar-brand"><i
			class="fas fa-tasks"></i>ELENCO DIPENDENTI</a>
					<form class="form-inline">
			<div class="actions">
				<span>BENTORNATO ${sessionScope.user.nome}</span>
				<button class="btn btn-info" type="button"
					onclick="location.href='dipendenti?scelta=9'">LOGOUT</button>
			</div>
		</form>
	</nav>
	<div class="content">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">ID</th>
					<th scope="col">NOME</th>
					<th scope="col">COGNOME</th>
					<th scope="col">LUOGO DI NASCITA</th>
					<th scope="col">DATA DI NASCITA</th>
					<th scope="col">SESSO</th>
					<th scope="col">TITOLO DI STUDIO</th>
					<th scope="col">CODICE FISCALE</th>
					<th scope="col">RUOLO AZIENDALE</th>
					<th scope="col">STIPENDIO</th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<%
				ArrayList<DipendenteDTO> dipendenti = null;
			dipendenti = (ArrayList<DipendenteDTO>) request.getAttribute("dipendenti");
			for (DipendenteDTO d : dipendenti) {
			%>
			<tbody>
				<tr>
					<th scope="row"></th>
					<td><%=d.getId()%></td>
					<td><%=d.getNome()%></td>
					<td><%=d.getCognome()%></td>
					<td><%=d.getLuogoDiNascita()%></td>
					<td><%=d.getDataDiNascita()%></td>
					<td><%=d.getSesso()%></td>
					<td><%=d.getTitoloDiStudio()%></td>
					<td><%=d.getCodiceFiscale()%></td>
					<td><%=d.getRuolo()%></td>
					<td><%=d.getStipendio()%></td>
					<td class="buttons"><button class="btn btn-dark btn-sm"
							onclick="location.href='dipendenti?scelta=4&id=<%=d.getId()%>'">
							
						</button></td>
					<td class="buttons"><button class="btn btn-primary btn-sm"
							onclick="location.href='dipendenti?scelta=2&id=<%=d.getId()%>'">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</button></td>
					<td class="buttons"><button class="btn btn-danger btn-sm"
							onclick="conferma(<%=d.getId()%>)">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</button></td>
			</tbody>
			<%
				}
			%>
		</table>
		<div class="actions">
			<button type="button" class="btn btn-success"
				onclick="location.href='dipendenti?scelta=1'">AGGIUNGI
				DIPENDENTE</button>
			<button type="button" class="btn btn-info"
				onclick="location.href='index.html'">TORNA AL MENU</button>
			<div class="navigation">
				<select id="sizePagina" name="sizePagina">
					<%
						int maxPagina = Integer.parseInt(session.getAttribute("maxPagina").toString());
					int lunghezzaPagina = Integer.parseInt(session.getAttribute("lunghezzaPagina").toString());
					for (int i = 1; i <= maxPagina; i++) {
						if (i == lunghezzaPagina) {
					%><option value="<%=i%>" selected><%=i%></option>
					<%
						} else {
					%><option value="<%=i%>"><%=i%></option>
					<%
						}
					}
					%>
				</select>
				<button type="button" name="operazione" class="btn btn-warning"
					onclick="location.href='dipendenti?scelta=8&operazione=1'"><<</button>
				<button type="button" name="operazione" class="btn btn-warning"
					onclick="location.href='dipendenti?scelta=8&operazione=2'"><</button>
				<button type="button" name="operazione" class="btn btn-warning"
					onclick="location.href='dipendenti?scelta=8&operazione=3'">></button>
				<button type="button" name="operazione" class="btn btn-warning"
					onclick="location.href='dipendenti?scelta=8&operazione=4'">>></button>
			</div>
		</div>
	</div>

	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-brand {
	font-size: 13px;
	padding-top: 1px !important;
}

.fas {
	margin-right: 10px;
}

.content {
	width: 87%;
	height: auto;
	margin: 0 auto;
	padding-top: 40px;
}

table {
	margin: 0 auto;
}

td, th {
	font-size: 11px;
	background-color: #FFF;
	text-align: center;
}

.buttons {
	width: 20px;
	text-align: center;
}

.buttons button {
	padding: 1px;
	width: 35px;
}

.actions button, span {
	font-size: 12px;
}

.navigation {
	float: right;
}

#sizePagina {
	border-radius: .25rem
}

span {
	color: #fff;
	margin-right: 15px;
	text-transform: uppercase;
}
</style>

</body>
</html>