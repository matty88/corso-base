<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.util.ArrayList, model.TitoloDiStudio, model.RuoloAziendale"%>
<!DOCTYPE html>
<html>
<head>
<script>
	function dimmiChiSono() {
		var nome = document.getElementById("nome").value;
		var cognome = document.getElementById("cognome").value;
		if (nome == "" || cognome == "") {
			alert("ERRORE: Prego inserire un nome ed un cognome!");
		} else {
			alert("Sei " + nome + " " + cognome);
		}
	}
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/b056d25a56.js"
	crossorigin="anonymous"></script>
<script>
	$(function() {
		$('#titolo_studio').change(function() {
			if ($(this).find('option:selected').val() == "0") {
				$('#nuovo_titolo').show();
				$('#nuovo_titolo').val("");
			} else {
				$('#nuovo_titolo').hide();
				$('#nuovo_titolo').val("");
			}
		});
	});
</script>
<script>
	$(function() {
		$('#ruolo_aziendale').change(function() {
			if ($(this).find('option:selected').val() == "0") {
				$('#nuovo_ruolo').show();
				$('#nuovo_ruolo').val("");
			} else {
				$('#nuovo_ruolo').hide();
				$('#nuovo_ruolo').val("");
			}
		});
	});
</script>
<meta charset="ISO-8859-1">
<title>Aggiungi dipendente</title>
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-dark bg-dark">
		<a class="navbar-brand"><i class="fas fa-plus"></i>AGGIUNGI
			DIPENDENTE</a>
		<div class="actions">
			<form class="form-inline">
				<button class="btn btn-success elenco" type="button"
					onclick="location.href='dipendenti?scelta=8'">ELENCO
					DIPENDENTI</button>
				<button style="margin-left: 5px" class="btn btn-info elenco"
					type="button" onclick="location.href='dipendenti?scelta=9'">LOGOUT</button>
			</form>
		</div>
	</nav>
	<div class="content">
		<%
			ArrayList<String> errori = (ArrayList<String>) request.getAttribute("errori");
		if (errori != null) {
			for (String errore : errori) {
		%>
		<span style="color: red"><%=errore%></span> <br />
		<%
			}
		}
		%><br />
		<form action="dipendenti?scelta=6" method="post">
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="email">E-mail</label> <input type="email"
						class="form-control" name="email" id="email"
						value="${param.email}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="password">Password</label> <input type="password"
						class="form-control" name="password" id="password"
						value="${param.password}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="nome">Nome</label> <input type="text"
						class="form-control" name="nome" id="nome" value="${param.nome}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="cognome">Cognome</label> <input type="text"
						class="form-control" name="cognome" id="cognome"
						value="${param.cognome}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="luogo_nascita">Luogo di nascita</label> <input
						type="text" name="luogo_nascita" class="form-control"
						id="luogo_nascita" value="${param.luogo_nascita}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="data_nascita">Data di nascita</label> <input
						class="form-control" id="data_nascita" type="date"
						name="data_nascita" placeholder="" title="format : "
						value="${param.data_nascita}" />
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-12 mb-3">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" id="maschio"
							name="sesso" value="maschio" checked /> <label
							class="form-check-label" for="maschio">Maschio</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" id="femmina"
							name="sesso" value="femmina"> <label
							class="form-check-label" for="femmina">Femmina</label>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<label for="codice_fiscale">Codice fiscale</label> <input
						type="text" name="codice_fiscale" class="form-control"
						id="codice_fiscale" value="${param.codice_fiscale}" />
				</div>
				<div class="col-md-6 mb-3">
					<label for="stipendio">Stipendio</label> <input type="text"
						name="stipendio" class="form-control" id="stipendio"
						value="${param.stipendio}" />
				</div>
			</div>

			<div class="form-row">
				<div class="form-group col-md-6 mb-3">
					<label for="titolo_studio">Titolo di studio</label> 
					<select
						id="titolo_studio" name="titolo_studio" class="form-control">
						<option value=-1 selected>Seleziona un titolo di studio...</option>
						<%
							ArrayList<TitoloDiStudio> titoli = null;
						titoli = (ArrayList<TitoloDiStudio>) request.getAttribute("titoli");
						for (TitoloDiStudio t : titoli) {
						%>
						<option value="<%=t.getId()%>"><%=t.getTitoloDiStudio()%></option>
						<%
							}
						%>
						<option value="0">Inserisci nuovo titolo di studio</option>
					</select>
					<input type="text" class="form-control" id="nuovo_titolo"
						name="nuovo_titolo" style="display: none; margin-top:5px">
				</div>
				<div class="form-group col-md-6 mb-3">
					<label for="ruolo_aziendale">Ruolo aziendale</label> <select
						id="ruolo_aziendale" name="ruolo_aziendale" class="form-control">
						<option value=-1 selected>Seleziona un ruolo aziendale...</option>
						<%
							ArrayList<RuoloAziendale> ruoli = null;
						ruoli = (ArrayList<RuoloAziendale>) request.getAttribute("ruoli");
						for (RuoloAziendale r : ruoli) {
						%>
						<option value="<%=r.getId()%>"><%=r.getRuoloAziendale()%></option>
						<%
							}
						%>
						<option value="0">Inserisci nuovo ruolo aziendale</option>
					</select> <input type="text"
						class="form-control" id="nuovo_ruolo" name="nuovo_ruolo" style="display: none; margin-top:5px">
				</div>
			</div>
			<div class="form-group">
				<div class="form-check">
					<input class="form-check-input" type="checkbox"
						name="autorizzazione" id="autorizzazione" checked> <label
						id="auth" class="form-check-label" for="autorizzazione">Autorizzo
						il trattamento dei dati</label>
				</div>
			</div>
			<div class="actions">
				<button class="btn btn-primary" type="submit">INVIA</button>
				<button class="btn btn-danger" type="reset">RESET</button>
				<button class="btn btn-success" type="button"
					onclick="dimmiChiSono()">CHI SEI</button>

			</div>
		</form>
	</div>

	<style>
body {
	margin: 0;
	padding: 0;
	font-family: 'Montserrat', sans-serif;
	background-image: url('https://www.mariocommone.it/images/bg-2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
}

.navbar {
	height: 45px;
}

.navbar-brand {
	font-size: 13px;
	padding-top: 1px !important;
}

.fas {
	margin-right: 10px;
}

.actions button {
	font-size: 12px;
	text-transform: uppercase;
}

.content {
	width: 60%;
	height: auto;
	margin: 0 auto;
}

label {
	text-transform: uppercase;
	font-size: 12px;
	color: #2c2c2c;
}

.form-control {
	font-size: 13px !important;
}

.actions button {
	font-size: 12px;
}

#auth {
	color: #2c2c2c;
}

.elenco {
	float: right;
}
</style>

</body>
</html>