package esercizioTre;

import java.util.Scanner;

public class EsercizioTre {

	public static void main(String[] args) {
		String risposta ="";
		do {
		numeri();
		} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));
	}
	
	public static void numeri() {
		int somma;
		int[] numero = new int[10];
		int index = 0;
		while(index<=9) {
			numero[index] = leggiIntero("Dai un numero alla variabile " + index + " del vettore:");
			index++;
		}
		somma = 0;
		System.out.println("***SOMMA***");
		for(index=0; index <=9; index++) {
			//System.out.println(index+1 +")"+numero[index]);
			//somma = somma+numero[index];
			somma+=numero[index];
		}
		System.out.println(somma);
	}
	
	public static int leggiIntero(String num) {
		Scanner input=new Scanner(System.in);
		System.out.print(num);
		return Integer.parseInt(input.nextLine()); 
	}

}
