package esercizioDue;

import java.util.Scanner;

public class EsercizioDue {

	public static void main(String[] args) {
		Vista vista = new Vista();
		String risposta;
		do {
	nominativi();
	risposta = vista.leggiStringa("Vuoi ripetere l'inserimento?:");
	} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));
	}
	
	public static void nominativi() {
		Vista vista = new Vista();
		String[] nome = new String[5];
		int index = 0;
		while(index<=4) {
			nome[index] = vista.leggiStringa("Dai un nome alla variabile " + index + " del vettore:");
			index++;
		}
		System.out.println("***VETTORE***");
		for(index=0; index <=4; index++) {
			System.out.println(index+1 +")"+nome[index]);
		}
	}
}
