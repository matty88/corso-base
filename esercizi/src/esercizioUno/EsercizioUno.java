package esercizioUno;
import java.util.Scanner;

public class EsercizioUno {

	public static void main(String[] args) {
		Vista vista = new Vista();
		String risposta;
		do {
			vettori();
			risposta = vista.leggiStringa("Vuoi ripetere l'inserimento?:");
		} while(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("s"));
	}

	public static void vettori() {
		int[] numero = new int[10];
		int index = 0;
		while(index<=9) {
			numero[index] = leggiIntero("Dai un numero alla variabile " + index + " del vettore:");
			/*Scanner input= new Scanner(System.in);
		    res = input.nextInt();*/
			index++;
		}
		System.out.println("***VETTORE***");
		for(index = 0; index<=9; index++) {
			System.out.println(index+1 +")"+numero[index]);
		}
	}

	public static int leggiIntero(String num) {
		Scanner input=new Scanner(System.in);
		System.out.print(num);
		return Integer.parseInt(input.nextLine()); 
	}
}


