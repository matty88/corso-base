package matrici;
import java.util.Scanner;

//SONO DEGLI ARRAY BIDIMENSIONALI, RISPETTO AI VETTORI QUINDI HANNO PIU' INDICI
public class Matrici {

	public static void main(String[] args) {
		int[][] matrice = new int[10][10];               //(riga,colonna)
		int[][][] matriceDue = new int [10][5][2];       //(riga,colonna, profonditÓ  //cubi) (si parte sempre da 0)
		/*
		 temp[0].length = numero colonne(indicando la prima riga)
		 length = numero righe
		 */
	}

	public void matriceTemperature() {
		int[][] temp=new int[1][2];
		temp[0][0]=10;
		temp[1][0]=12;
		temp[0][1]=14;
		temp[1][1]=10;
		temp[0][2]=12;
		temp[1][2]=14;
		for(int y=0;y<temp[0].length;y++) {
			for(int x=0;x<temp.length;x++) {
				System.out.println("temp["+(x+1)+"]["+(y+1)+"]:"+temp[x][y]);
			}
		}
	}
	public void matriceTemperatureconInput() {
	       Scanner input=new Scanner(System.in);
	       int[][] temp=new int[3][7];
	       System.out.println("--- Inserimento temperature ---");
	       for(int y=0;y<temp[0].length;y++) {
	            for(int x=0;x<temp.length;x++) {
	               System.out.print("temp["+(x+1)+"]["+(y+1)+"]:");
	               temp[x][y]=Integer.parseInt(input.nextLine());
	           }
	       }
	       System.out.println("--- Visualizzazione temperature ---");
	       for(int y=0;y<temp[0].length;y++) {
	            for(int x=0;x<temp.length;x++) {
	               System.out.println("temp["+(x+1)+"]["+(y+1)+"]:"+temp[x][y]);
	           }
	       }
	   }

	public void matriceTemperatureconInputEGiorno() {
	       Scanner input=new Scanner(System.in);
	       int[][] temp=new int[3][7];
	       String[] giorni= {"Lun","Mar","Mer","Gio","Ven","Sab","Dom"};
	       System.out.println("--- Inserimento temperature ---");
	       for(int y=0;y<temp[0].length;y++) {
	            for(int x=0;x<temp.length;x++) {
	               System.out.print("temp["+giorni[y]+"]["+(x+1)+"]:");
	               temp[x][y]=Integer.parseInt(input.nextLine());
	           }
	       }
	       System.out.println("--- Visualizzazione temperature ---");
	       for(int y=0;y<temp[0].length;y++) {
	            for(int x=0;x<temp.length;x++) {
	               System.out.println("temp["+giorni[y]+"]["+(x+1)+"]:"+temp[x][y]);
	           }
	       }
	   }
}
