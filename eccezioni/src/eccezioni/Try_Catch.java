package eccezioni;

import java.util.Scanner;

//per gestire try catch si usa quasi sempre il do while

public class Try_Catch {
	public void errore() {   
		Scanner io=new Scanner(System.in);
		int valore=0;
		boolean flag;
		do {
			flag=false;
			try {
				System.out.println("Inserisci un valore intero: ");
				valore=Integer.parseInt(io.nextLine());
			}catch(NumberFormatException e) {
				System.out.println("Errore:"+e.getMessage());
				flag=true;
			}
		}while(flag);
		System.out.println(valore);
	}
}
