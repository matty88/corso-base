package view;

import java.util.ArrayList;
import java.util.Collections;

import model.Persona;

public class Vista {
	InputOutput io = new InputOutput();

	public void menuIniziale() {
		System.out.println("1) Inserisci");
		System.out.println("2) Modifica");
		System.out.println("3) Cancella");   
		System.out.println("4) Cerca");   
		System.out.println("5) Visualizza");
		System.out.println("6) Esci");
	}

	public void menuRicerca() {
		System.out.println("1) Ricerca per nome");
		System.out.println("2) Ricerca per cognome");
		System.out.println("3) Torna al menu principale");
	}

	public void schedaPersonaInserimento(Persona p) {
		p.setId(io.leggiIntero("Id: "));   
		p.setNome(io.leggiStringa("Nome: "));
		p.setCognome(io.leggiStringa("Cognome: "));
		p.setLuogoDiNascita(io.leggiStringa("Nato a: "));
		//p.setDataDiNascita(io.leggiStringa("Nato il: "));
		p.setSesso(io.leggiStringa("Sesso: "));
	}

	public void stampaSchedaPersona(Persona p) {
		System.out.println("Id: "+p.getId());
		System.out.println("Nome: "+p.getNome());
		System.out.println("Cognome: "+p.getCognome());
		System.out.println("Luogo di nascita: "+p.getLuogoDiNascita());
		System.out.println("Data di nascita: "+p.getDataDiNascita());
		System.out.println("sesso: "+p.getSesso());
	}

	public void stampaPersona(Persona p) {
		System.out.println(p.toString());
	}

	public void stampaPersonaArray(ArrayList<Persona> p) {
		for(int index=0; index<p.size(); index++) {
			System.out.println(p.get(index).toString());
		}
	}	

	public void modificaPersona(Persona p) {
		String appAttributo="";  
		appAttributo= io.leggiStringa("Nome:["+p.getNome()+"]:");
		if(!appAttributo.equals("")) {
			p.setNome(appAttributo);
		}
		appAttributo= io.leggiStringa("Cognome:["+p.getCognome()+"]:");
		if(!appAttributo.equals("")) {
			p.setCognome(appAttributo);
		}
		appAttributo= io.leggiStringa("Luogo di nascita:["+p.getLuogoDiNascita()+"]:");
		if(!appAttributo.equals("")) {
			p.setLuogoDiNascita(appAttributo);
		}
//		appAttributo= io.leggiStringa("Data di nascita:["+p.getDataDiNascita()+"]:");
//		if(!appAttributo.equals("")) {
//			p.setDataDiNascita(appAttributo);
//		}
		appAttributo= io.leggiStringa("Sesso:["+p.getSesso()+"]:");
		if(!appAttributo.equals("")) {
			p.setSesso(appAttributo);
		}
	}

	public int cercaId(int id, ArrayList<Persona> persone) {
		for(int i=0;i<persone.size();i++) {
			if(id==persone.get(i).getId()) {
				return i;
			}
		}
		return -1;
	}

	public void ricercaPerNome(String nome, ArrayList<Persona>persone) {
		boolean flag = false;
		for(int i=0; i<persone.size(); i++) {
			if(nome.equalsIgnoreCase(persone.get(i).getNome())) {
				System.out.println("");
				System.out.println(persone.get(i).toString()+"\n");
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il nome: "+nome);
		}
	}

	public void ricercaPerCognome(String nome, ArrayList<Persona>persone) {
		boolean flag = false;
		for(int i=0; i<persone.size(); i++) {
			if(nome.equalsIgnoreCase(persone.get(i).getCognome())) {
				System.out.println("");
				System.out.println(persone.get(i).toString()+"\n");
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il cognome: "+nome);
		}
	}

	public void ordinamentoCognome(ArrayList<Persona>persone) {
		String cognome, cognomeDue;
		for(int i=0;i<persone.size();i++) {
			for(int x=0;x<(persone.size()-1-i);x++) {
				cognome=persone.get(x).getCognome();
				cognomeDue=persone.get(x+1).getCognome();
				if(cognome.compareTo(cognomeDue)>0) {
					Collections.swap(persone, x, x+1);
//					Persona a=persone.get(x);
//					Persona b=persone.get(x+1);
//					persone.set(x, b);
//					persone.set(x+1, a);
				}
			}
		}
		for(int cont=0;cont<persone.size();cont++) {
			System.out.println("");
			System.out.println(persone.get(cont).toString()+"\n");;
		}
	}
	
	public void ordinamentoCognomeBis(ArrayList<Persona>persone) {
		persone.sort((Persona p1, Persona p2)-> p1.getCognome().compareTo(p2.getCognome()));
		//persone.forEach(p-> System.out.println(p));
		persone.forEach(System.out::println);     //::SI USA QUANDO L'INPUT E' DELLA STESSA CLASSE DELL'ELEMENTO DELLA LISTA(DOUBLE COLON)
	}
	
//	public static void stampa(Persona p) {
//		System.out.println(p);
//	}

	public void ricercaCognomeNome(String nome, ArrayList<Persona>persone) {
		String key;
		boolean flag=false;
		for(int i=0;i<persone.size();i++) {
			key=persone.get(i).getCognome();
			key+=persone.get(i).getNome();
			if(key.equalsIgnoreCase(nome)) {
				flag=true;
				System.out.println(persone.get(i).toString()+"\n");
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il cognome e nome: "+nome);
		}
	}

	public void printMsg(String msg) {
		System.out.println(msg);
	}
}
