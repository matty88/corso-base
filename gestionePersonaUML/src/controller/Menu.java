package controller;

import java.util.ArrayList;
import model.Persona;
import model.PersonaCRUD;
import view.InputOutput;
import view.Vista;

public class Menu {	
	Persona p;
	Vista v = new Vista();		
	InputOutput io = new InputOutput();		
	PersonaCRUD pc = new PersonaCRUD();

	public void menuPrincipale() {
		int scelta;
		String risp="";
		do {
			v.menuIniziale();
			scelta=io.leggiIntero("\nScegli una voce di menu: ");
			switch(scelta) {              
			case 1:
				v.printMsg("\nInserisci utente");
				p = new Persona();
				v.schedaPersonaInserimento(p);
				pc.inserisci(p);
				v.printMsg("\nInserimento eseguito con successo!");
				break;           
			case 2:
				v.printMsg("");
				v.stampaPersonaArray(pc.leggi());
				do {
					scelta=io.leggiIntero("Scegli Id della persona da modificare: ");
					scelta=v.cercaId(scelta, pc.leggi());
					if(scelta==-1) {
						risp=io.leggiStringa("Id non trovato, premi INVIO per riprovare.");
					}
				}while(scelta==-1);
				if(scelta!=-1) {
					p=pc.leggi(scelta);
					v.stampaSchedaPersona(p);
					risp=io.leggiStringa("Vuoi confermare la modifica? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						v.modificaPersona(p);
						v.printMsg("Modifica eseguita con successo!");
						v.stampaPersonaArray(pc.leggi());
					} else {
						risp=io.leggiStringa("Modifica annullata, premi INVIO per continuare.");
					}
				}
				break;
			case 3:
				v.printMsg("");
				v.stampaPersonaArray(pc.leggi());
				do {
					scelta=io.leggiIntero("Scegli Id della persona da cancellare: ");
					scelta=v.cercaId(scelta, pc.leggi());
					if(scelta==-1) {
						risp=io.leggiStringa("Id non trovato, premi INVIO per riprovare.");
					}
				}while(scelta==-1);
				if(scelta!=-1) {
					p=pc.leggi(scelta);
					v.stampaSchedaPersona(p);
					risp=io.leggiStringa("Vuoi confermare la cancellazione? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						pc.cancella(scelta);
						v.printMsg("\nCancellazione effettuata con successo");
					} else {
						risp=io.leggiStringa("Cancellazione annullata, premi INVIO per continuare.");
					}
				}
				break; 
			case 4:
				ricerca(pc.leggi());
				break; 
			case 5:
				System.out.println("");
				v.stampaPersonaArray(pc.leggi());
				System.out.println("");
				break; 
			case 6:
				pc.salva();
				break;			
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 6");   
			}
		} while(scelta!=6);
			v.printMsg("Programma terminato");
	}

	private void ricerca(ArrayList<Persona> persone) {		
		int scelta;
		String nome, risposta;
		do{
			v.menuRicerca();
			scelta=io.leggiIntero("Scegli: ");
			switch(scelta) {
			case 1:
				nome=io.leggiStringa("\nInserisci nome: ");
				v.ricercaPerNome(nome, pc.leggi());
				break;
			case 2:
				nome=io.leggiStringa("\nInserisci cognome: ");
				v.ricercaPerCognome(nome, pc.leggi());
				risposta=io.leggiStringa("\nVisualizza per cognome (S/N)?: ");
				if(risposta.equalsIgnoreCase("s") || risposta.equalsIgnoreCase("si")) {
				//v.ordinamentoCognome(pc.leggi());
				v.ordinamentoCognomeBis(pc.leggi());
				//pc.leggi().forEach(Vista::stampa);
				} else {
					risposta=io.leggiStringa("\nPremi INVIO per continuare.");
				}
				break;
			case 3:
				break;
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 2");
				break;
			}
		}while(scelta!=3);
	}
}
