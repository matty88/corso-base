package model;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class PersonaCRUD {

	ArrayList<Persona> persone = new ArrayList<Persona>();

	public boolean inserisci(Persona p) {   
		persone.add(p);
		return true;
	}

	public boolean modifica (int index, Persona p) {  
		persone.set(index, p);   
		return true;
	}

	public boolean cancella (int index) {        
		persone.remove(index);
		return true;
	}

	public boolean cancella(Persona p) {       
		persone.remove(p);
		return true;
	}

	public Persona leggi(int index) {          
		return persone.get(index);                
	}

	public ArrayList<Persona> leggi() {
		return persone;                        
	}

	public void carica() {          
		String[] attributi;            //VARIABILE CHE CONSERVA UN INDIRIZZO DI UN VETTORE DI STRINGHE, NON � UN VETTORE PERCH� IL VETTORE LO CREA LO SPLIT
		Persona p;
		try {
			FileReader file = new FileReader("c://Dati//file.dat");      //CLASSE FILE READER PER LEGGERE
			BufferedReader input = new BufferedReader(file);
			String line = null;
			SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yy");
			while ((line = input.readLine()) != null) {           //LEGGI MENTRE NON OTTIENI NULL(OVVERO NO NCI SONO PI� RIGHE)
				attributi=line.split(",");                       //SPLIT RITAGLIA AD OGNI VIRGOLA LA STRINGA LINE CHE E' IN FORMATO CSV E LO CREA/TRASFORMA IN VETTORE DI STRINGHE
				p=new Persona();
				p.setId(Integer.parseInt(attributi[0]));          //CONVERTI STRINGA IN INTERO ESSENDO UN ID
				p.setNome(attributi[1].trim());                  //TRIM PERCH� NEL TO STRING CI SONO DEGLI SPAZI DOPO LA VIRGOLA CHE VANNO ELIMINATI
				p.setCognome(attributi[2].trim());
				p.setLuogoDiNascita(attributi[3].trim());
				try {
					p.setDataDiNascita(formatoData.parse(attributi[4].trim()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				p.setSesso(attributi[2]);
				persone.add(p);

			}
			input.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}       
	}

	public void salva() {                 //salva contenuto in memoria in un file su disco
		try{                                      //INSERIAMO IL CODICE CHE PU� GENERARE UN ECCEZIONE ALL'INTERNO DEL TRY CHE GESTISCE LE ECCEZIONI
			FileWriter file = new FileWriter("c://Dati//file.dat");     //CLASSE FILEWRITER PER CREARE FILE(SCRIVE IN FILE-contrario di scanner ovvero parte da memoria(unit� centrale) e arriva in disco(periferica)) //percorso SALVATAGGIO DATI
			BufferedWriter output = new BufferedWriter(file);     //DICIAMO DIVE SCRIVERE
			//for(Persona p: persone) {   16-07
			for(int x =0;x<persone.size();x++) {
				//output.write(p.toString());
				output.write(persone.get(x).toString());               //ESEGUE IL TO STRING E LO VA A SCRIVERE NEL FILE
				output.newLine();                                 //VAI ACCAPO 
			}
			output.close();                               //CHIUDE IL FILE
		}catch (IOException e) {                      //CATCH � UN EVENTO CHE SI SCATENA QUANDO DI GENERE L'ERRORE //IN QUESTO CASO SE L'ERRORE � IN INPUT OUTPUT //SOLO EXCEPTION SE L'ERRORE PU� ESSERE DI QUALSIASI TIPO
			//e.printStackTrace();                        //IN e VIENE PASSATO L'INDIRIZZO DI DOVE SI VERIFICA L'ERRORE CHE VIENE VISUALIZZATO DA PRINT STACK TRACE
			System.out.println("Attenzione errore:"+e.getMessage());      //STESSA COSA DI PRINT STACK TRACE MA PI� ELEGANTE
		}
	}
}
