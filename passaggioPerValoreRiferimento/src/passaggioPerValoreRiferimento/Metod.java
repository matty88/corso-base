package passaggioPerValoreRiferimento;

public class Metod {
	public void invertiUno(int a, int b) {      //essendo un tipo primitivo il passaggio avviene per valore
		int temp;
		System.out.println("(inverti) prima swap a="+a+" b="+b);
		temp = a;
		a = b;
		b = temp;
		System.out.println("(inverti) dopo swap a="+a+" b="+b);
	}

	public void invertiDue(int[] ab) {        //passiamo l'indirizzo di memoria
		int temp;
		System.out.println("(Invert) prima swap a="+ab[0]+" b="+ab[1]);
		temp = ab[0];
		ab[0] = ab[1];
		ab[1] = temp;
		System.out.println("(Inverti) dopo swap a="+ab[0]+" b="+ab[1]);
	}

	//public void invertiTre(Valori valori) {     //non essendo un tipo primitivo il passaggio avviene per riferimento(originale)   
//		int temp;
//		System.out.println("(Invert) prima swap a="+valori.a+" b="+valori.b);
//		temp = valori.a;
//		valori.a = valori.b;
//		valori.b = temp;
//		System.out.println("(Inverti) dopo swap a="+valori.a+" b="+valori.b);
//	}

	public void visualizzaUno(int a1, int a2, int a3, int a4, int a5) {
		System.out.println("a1="+a1+",a2="+a2+",a3="+a3+",a41="+a4+",a5="+a5);
	}

	public void visualizzaDue(int[] interi) {
		System.out.println("a1="+interi[0]+",a2="+interi[1]+",a3="+interi[2]+",a41="+interi[3]+",a5="+interi[4]);
	}

	public void stampaPersona(String nome, String cognome, int eta) {
		System.out.println("Nome:"+nome+", Cognome:"+cognome+ ", Et�:"+eta);
	}

	//public void stampaPersonaDue(Persona p) {
//		System.out.println("Nome:"+p.nome+", Cognome:"+p.cognome+ ", Et�:"+p.eta);
//	}

}
