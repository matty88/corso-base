package types;
import java.util.Scanner;

public class Metodi {

	public int leggiIntero(String num) {
		Scanner input=new Scanner(System.in);
		System.out.print(num);
		return Integer.parseInt(input.nextLine());       
	}

	public String leggiStringa(String msg) {
		Scanner input=new Scanner(System.in);
		System.out.print(msg);
		return input.nextLine();       
	}

	public int leggiInteroCustom(String num) {
		while (true) {  //WHILE TRUE
			try { 
				Scanner input=new Scanner(System.in);
				System.out.print(num);
				return Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}
}
