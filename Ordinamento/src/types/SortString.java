package types;

public class SortString {

	public void bubbleSortString() {
		String[] vett = new String[10];
		inserimentoArrayString(vett);
		ordinaArrayBubbleSortString(vett);
		stampaArrayString(vett);
	}

	public void selectionSortString() {
		String[] vett = new String[10];
		inserimentoArrayString(vett);
		ordinaArraySelectionSortString(vett);
		stampaArrayString(vett);
	}

	private void ordinaArrayBubbleSortString(String[] vett) {
		String temp;
		System.out.println("***ALGORITMO BUBBLE SORT***");
		for(int i=0;i<vett.length;i++) {
			for(int x=0;x<(vett.length-1-i);x++) {   //-i PER FARE MENO CICLI
				if(vett[x].compareTo(vett[x+1])>0) {
					//SWAP
					temp=vett[x];
					vett[x]=vett[x+1];
					vett[x+1]=temp;
				}
			}
		}
	}

	private void ordinaArraySelectionSortString(String[] vett) {
		String temp, min;
		int indexMin;
		System.out.println("***ALGORITMO SELECTION SORT***");
		for(int i=0; i<vett.length-1; i++) {   //CICLA FINO AL PENULTIMO PERCHE' L'ULTIMO SARA' GIA' IN ORDINE
			//TROVA IL VALORE MINIMO
			min=vett[i];
			//INDICE VALORE MINIMO
			indexMin=i;
			for(int x=i+1; x<vett.length; x++) {
				if(vett[x].compareTo(min)<0) {
					min = vett[x];
					indexMin = x;
					//SWAP CAMBIA VALORE MINIMO CON IL PRIMO VALORE
					temp=vett[i];
					vett[i]=vett[indexMin];
					vett[indexMin]=temp;
				}
			}
		}		
	}

	private void inserimentoArrayString(String[] vett) {
		Metodi metodi = new Metodi();
		for(int index=0; index<=9; index++) {
			vett[index] = metodi.leggiStringa("Dai un nome alla variabile " + (index+1) + " del vettore:");
		}
	}

	private void stampaArrayString(String[] vett) {
		for(int cont=0;cont<vett.length;cont++) {
			System.out.print("["+vett[cont]+"]");
		}
	}
}
