package types;

public class Start {

	public static void main(String[] args) {
		Metodi metodi = new Metodi();
		String res, start;
		do {
			//Sort es = new Sort();
			SortString esString = new SortString();
			start = metodi.leggiStringa("Quale algoritmo vuoi eseguire? Bubble o Selection?:");
			if(start.equalsIgnoreCase("bubble") || start.equalsIgnoreCase("b")) {
				esString.bubbleSortString();
			} else if (
					start.equalsIgnoreCase("selection") || start.equalsIgnoreCase("s")) {
				esString.selectionSortString();
			} else {
				System.out.println("Non hai inserito un comando valido");
			}
			//es.bubbleSort();
			//es.selectionSort();
			//esString.bubbleSortString();
			//esString.selectionSortString();
			System.out.println("\n");
			res = metodi.leggiStringa("Vuoi ripetere l'inserimento?:");
		} while(res.equalsIgnoreCase("si") || res.equalsIgnoreCase("s"));
	}
}
