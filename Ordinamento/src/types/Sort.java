package types;
//I SORT SONO ALGORITMI DI ORDINAMENTO CHE PERMETTONO QUINDI DI ORDINARE GLI ELEMENTI DI UN VETTORE
//CON IL TERMINE ORDINAMENTI INTENDIAMO LA DISPOSIZIONE DI OGGETTI IN BASE AD UNA SEQUENZA CRESCENTE O DECRESCENTE


public class Sort {

	public void bubbleSort() {
		int[] vett = new int[10];
		inserimentoArray(vett);
		ordinaArrayBubbleSort(vett);
		stampaArray(vett);
	}

	public void selectionSort() {
		int[] vett = new int[10];
		inserimentoArray(vett);
		ordinaArraySelectionSort(vett);
		stampaArray(vett);
	}

	private void ordinaArrayBubbleSort(int[] vett) {
		int temp;
		System.out.println("***ALGORITMO BUBBLE SORT***");
		for(int i=0;i<vett.length;i++) {
			for(int x=0;x<(vett.length-1-i);x++) {   //-i PER FARE MENO CICLI
				if(vett[x]>vett[x+1]) {
					//SWAP
					temp=vett[x];
					vett[x]=vett[x+1];
					vett[x+1]=temp;
				}
			}
		}
	}

	private void ordinaArraySelectionSort(int[] vett) {
		int temp, min, indexMin;
		System.out.println("***ALGORITMO SELECTION SORT***");
		for(int i=0; i<vett.length-1; i++) {   //CICLA FINO AL PENULTIMO PERCHE' L'ULTIMO SARA' GIA' IN ORDINE
			//TROVA IL VALORE MINIMO
			min=vett[i];
			//INDICE VALORE MINIMO
			indexMin=i;
			for(int x=i+1; x<vett.length; x++) {
				if(vett[x] < min) {
					min = vett[x];
					indexMin = x;
					//SWAP CAMBIA VALORE MINIMO CON IL PRIMO VALORE
					temp=vett[i];
					vett[i]=vett[indexMin];
					vett[indexMin]=temp;
				}
			}
		}		
	}

	private void inserimentoArray(int[] vett) {
		Metodi metodi = new Metodi();
		for(int index=0; index<=9; index++) {
			vett[index] = metodi.leggiInteroCustom("Dai un numero alla variabile " + (index+1) + " del vettore:");
		}
	}

	private void stampaArray(int[] vett) {
		for(int cont=0;cont<vett.length;cont++) {
			System.out.print("["+vett[cont]+"]");
		}
	}
}
