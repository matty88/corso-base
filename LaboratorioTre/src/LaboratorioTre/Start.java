package LaboratorioTre;

public class Start {

	public static void main(String[] args) {
		Metodi m = new Metodi();
		String res;
		do {
			//Dipendente[] dipendenti=m.inserisciDipendenti(m.leggiIntero("Inserisci numero dipendenti da inserire:"));
			int numeroDipendenti = m.leggiIntero("Inserisci numero dipendenti da inserire:");
			Dipendente[] dipendenti= m.inserisciDipendenti(numeroDipendenti);
			System.out.println("\n");
			m.stampaDipendenti(dipendenti);
			res = m.leggiStringa("Vuoi ripetere l'inserimento?:");
		} while(res.equalsIgnoreCase("si") || res.equalsIgnoreCase("s"));
	}
}
