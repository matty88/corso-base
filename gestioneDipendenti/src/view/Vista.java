package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Dipendente;
import model.DipendenteDTO;
import model.RuoloAziendaleDTO;
import model.TitoloDiStudioDTO;

public class Vista {
	InputOutput io = new InputOutput();

	public void menuIniziale() {
		System.out.println("1) Inserisci");
		System.out.println("2) Modifica");
		System.out.println("3) Cancella");   
		System.out.println("4) Cerca");   
		System.out.println("5) Visualizza");
		System.out.println("6) Esci");
	}

	public void menuRicerca() {
		System.out.println("1) Ricerca per nome");
		System.out.println("2) Ricerca per cognome");
		System.out.println("3) Torna al menu principale");
	}
	
	public void menuInizialeDTO() {
		System.out.println("1) Visualizza dipendente per Id");
		System.out.println("2) Visualizza dipendente per cognome e nome");
		System.out.println("3) Visualizza dipendente per ruolo aziendale");   
		System.out.println("4) Visualizza dipendente per titolo di studio");   
		System.out.println("5) Visualizza numero posizioni aziendali");
		System.out.println("6) Torna al menu principale");
	}

	public void schedaDipendenteInserimento(Dipendente d) {
		//d.setId(io.leggiIntero("Id: "));   
		d.setNome(io.leggiStringa("Nome: "));
		d.setCognome(io.leggiStringa("Cognome: "));
		d.setLuogoDiNascita(io.leggiStringa("Nato a: "));
		d.setDataDiNascita(io.leggiStringa("Nato il: "));
		d.setSesso(io.leggiStringa("Sesso: "));
		d.setStipendio(io.leggiDecimale("Stipendio: "));
		d.setCodiceFiscale(io.leggiStringa("Codice fiscale: ").toUpperCase());
		d.setIdTitoloDiStudio(io.leggiIntero("Titolo di studio: "));
		d.setIdRuolo(io.leggiIntero("Ruolo aziendale: "));
	}

	public void stampaSchedaDipendente(Dipendente d) {
		System.out.println("Id: "+d.getId());
		System.out.println("Nome: "+d.getNome());
		System.out.println("Cognome: "+d.getCognome());
		System.out.println("Luogo di nascita: "+d.getLuogoDiNascita());
		System.out.println("Data di nascita: "+d.getDataDiNascita());
		System.out.println("Sesso: "+d.getSesso());
		System.out.println("Stipendio: "+d.getStipendio());
		System.out.println("Codice fiscale: "+d.getCodiceFiscale());
		System.out.println("Titolo di studio: "+d.getIdTitoloDiStudio());
		System.out.println("Ruolo aziendale: "+d.getIdRuolo());
	}

	public void stampaDipendente(Dipendente d) {
		System.out.println(d.toString());
	}

	public void stampaDipendenteArray(ArrayList<Dipendente> d) {
		for(int index=0; index<d.size(); index++) {
			System.out.println("\n"+d.get(index).toString()+"\n");
		}
	}	
	
	public void stampaDipendenteDTO(ArrayList<DipendenteDTO> d) {
		for(int index=0; index<d.size(); index++) {
			System.out.println("\n"+d.get(index).toString()+"\n");
		}
	}
	
	public void stampaRuoloAziendaleDTO(ArrayList<RuoloAziendaleDTO> list) {
		for(int index=0; index<list.size(); index++) {
			System.out.println("\n"+list.get(index).toString()+"\n");
		}
	}
	
	public void stampaTitoloDiStudioDTO(ArrayList<TitoloDiStudioDTO> list) {
		for(int index=0; index<list.size(); index++) {
			System.out.println("\n"+list.get(index).toString()+"\n");
		}
	}
	
	public void stampaDipendenteAziendalePerRuoloDTO(ArrayList<RuoloAziendaleDTO> list) {
		for(int index=0; index<list.size(); index++) {
			System.out.print("\nRuolo aziendale: "+list.get(index).getRuoloAziendale()+",  ");
			System.out.println("Numero di ruoli: "+list.get(index).getRuoli()+"\n");
		}
	}
	

	public void modificaDipendente(Dipendente d) {
		String appAttributo=""; 
		double appAttributoDecimale=0.0;
		int appAttributoIntero=0;
		appAttributo= io.leggiStringa("Nome:["+d.getNome()+"]:");
		if(!appAttributo.equals("")) {
			d.setNome(appAttributo);
		}
		appAttributo= io.leggiStringa("Cognome:["+d.getCognome()+"]:");
		if(!appAttributo.equals("")) {
			d.setCognome(appAttributo);
		}
		appAttributo= io.leggiStringa("Luogo di nascita:["+d.getLuogoDiNascita()+"]:");
		if(!appAttributo.equals("")) {
			d.setLuogoDiNascita(appAttributo);
		}
		appAttributo= io.leggiStringa("Data di nascita:["+d.getDataDiNascita()+"]:");
		if(!appAttributo.equals("")) {
			d.setDataDiNascita(appAttributo);
		}
		appAttributo= io.leggiStringa("Sesso:["+d.getSesso()+"]:");
		if(!appAttributo.equals("")) {
			d.setSesso(appAttributo);
		}
		appAttributoDecimale= io.leggiDecimale("Stipendio:["+d.getStipendio()+"]:");
		if(appAttributoDecimale!=0.0) {
			d.setStipendio(appAttributoDecimale);
		}
		appAttributo= io.leggiStringa("Codice fiscale:["+d.getCodiceFiscale().toUpperCase()+"]:");
		if(!appAttributo.equals("")) {
			d.setCodiceFiscale(appAttributo);
		}
		appAttributoIntero= io.leggiIntero("Titolo di studio:["+d.getIdTitoloDiStudio()+"]:");
		if(appAttributoIntero!=0) {
			d.setIdTitoloDiStudio(appAttributoIntero);
		}
		appAttributoIntero= io.leggiIntero("Ruolo aziendale:["+d.getIdRuolo()+"]:");
		if(appAttributoIntero!=0) {
			d.setIdRuolo(appAttributoIntero);
		}
	}

//	public int cercaId(int id, ArrayList<Dipendente> d) {
//		for(int i=0;i<d.size();i++) {
//			if(id==d.get(i).getId()) {
//				return i;
//			}
//		}
//		return -1;
//	}

	public void ricercaPerNome(String nome, ArrayList<Dipendente> d) {
		boolean flag = false;
		for(int i=0; i<d.size(); i++) {
			if(nome.equalsIgnoreCase(d.get(i).getNome())) {
				System.out.println("");
				System.out.println(d.get(i).toString()+"\n");
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il nome: "+nome);
		}
	}

	public void ricercaPerCognome(String nome, ArrayList<Dipendente>d) {
		boolean flag = false;
		for(int i=0; i<d.size(); i++) {
			if(nome.equalsIgnoreCase(d.get(i).getCognome())) {
				System.out.println("");
				System.out.println(d.get(i).toString()+"\n");
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il cognome: "+nome);
		}
	}

	public void ordinamentoCognome(ArrayList<Dipendente>d) {
		String cognome, cognomeDue;
		for(int i=0;i<d.size();i++) {
			for(int x=0;x<(d.size()-1-i);x++) {
				cognome=d.get(x).getCognome();
				cognomeDue=d.get(x+1).getCognome();
				if(cognome.compareTo(cognomeDue)>0) {
					Collections.swap(d, x, x+1);
//					Dipendente a=d.get(x);
//					Dipendente b=d.get(x+1);
//					d.set(x, b);
//					d.set(x+1, a);
				}
			}
		}
		for(int cont=0;cont<d.size();cont++) {
			System.out.println("");
			System.out.println(d.get(cont).toString()+"\n");;
		}
	}
	
	public void ordinamentoCognomeBis(ArrayList<Dipendente>d) {
		d.sort((Dipendente p1, Dipendente p2)-> p1.getCognome().compareTo(p2.getCognome()));
		//d.forEach(d-> System.out.println(d));
		d.forEach(System.out::println);     //::SI USA QUANDO L'INPUT E' DELLA STESSA CLASSE DELL'ELEMENTO DELLA LISTA(DOUBLE COLON)
	}
	
//	public static void stampa(Persona p) {
//		System.out.println(p);
//	}

	public void ricercaCognomeNome(String nome, ArrayList<Dipendente>d) {
		String key;
		boolean flag=false;
		for(int i=0;i<d.size();i++) {
			key=d.get(i).getCognome();
			key+=d.get(i).getNome();
			if(key.equalsIgnoreCase(nome)) {
				flag=true;
				System.out.println(d.get(i).toString()+"\n");
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il cognome e nome: "+nome);
		}
	}

	public void printMsg(String msg) {
		System.out.println(msg);
	}
}
