package model;

public class DipendenteDTO {
	private String nome;
	private String cognome;
	private String dataDiNascita;
	private String luogoDiNascita;
	private String titoloDiStudio;
	private String ruoloAziendale;


	@Override
	public String toString() {
		return "Cognome: " + cognome + ", Nome: " + nome + ", Data Di nascita: " + dataDiNascita
				+ ", Luogo di nascita: " + luogoDiNascita + ", Titolo di studio: " + titoloDiStudio + ", Ruolo aziendale: "
				+ ruoloAziendale;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDataDiNascita() {
		return dataDiNascita;
	}
	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}
	public String getLuogoDiNascita() {
		return luogoDiNascita;
	}
	public void setLuogoDiNascita(String luogoDiNascita) {
		this.luogoDiNascita = luogoDiNascita;
	}
	public String getTitoloDiStudio() {
		return titoloDiStudio;
	}
	public void setTitoloDiStudio(String titoloDiStudio) {
		this.titoloDiStudio = titoloDiStudio;
	}
	public String getRuoloAziendale() {
		return ruoloAziendale;
	}
	public void setRuoloAziendale(String ruoloAziendale) {
		this.ruoloAziendale = ruoloAziendale;
	}
	
	
	public DipendenteDTO(String nome, String cognome, String dataDiNascita, String luogoDiNascita,
			String titoloDiStudio, String ruoloAziendale) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.dataDiNascita = dataDiNascita;
		this.luogoDiNascita = luogoDiNascita;
		this.titoloDiStudio = titoloDiStudio;
		this.ruoloAziendale = ruoloAziendale;
	}

	public DipendenteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + ((dataDiNascita == null) ? 0 : dataDiNascita.hashCode());
		result = prime * result + ((luogoDiNascita == null) ? 0 : luogoDiNascita.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((ruoloAziendale == null) ? 0 : ruoloAziendale.hashCode());
		result = prime * result + ((titoloDiStudio == null) ? 0 : titoloDiStudio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DipendenteDTO other = (DipendenteDTO) obj;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (dataDiNascita == null) {
			if (other.dataDiNascita != null)
				return false;
		} else if (!dataDiNascita.equals(other.dataDiNascita))
			return false;
		if (luogoDiNascita == null) {
			if (other.luogoDiNascita != null)
				return false;
		} else if (!luogoDiNascita.equals(other.luogoDiNascita))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (ruoloAziendale == null) {
			if (other.ruoloAziendale != null)
				return false;
		} else if (!ruoloAziendale.equals(other.ruoloAziendale))
			return false;
		if (titoloDiStudio == null) {
			if (other.titoloDiStudio != null)
				return false;
		} else if (!titoloDiStudio.equals(other.titoloDiStudio))
			return false;
		return true;
	}

}
