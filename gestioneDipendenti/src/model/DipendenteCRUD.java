package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class DipendenteCRUD {

	public int getNumeroRecord() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		int record=-1;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         //� la classe che esegue il metodo statico che apre la connessione
			String sql="select count(*) as record from dipendenti";      //conta i record in una tabella   //as = da un nome per leggere
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			if(rs.first()) {
				record=rs.getInt("record");
			}
			ps.close();
			rs.close();
			return record;
		}catch(SQLException e) {
			System.out.println("Errore di lettura numero record!"+e.getMessage());
			return record;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return record;
		}
	}

	public boolean inserisci(Dipendente d) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="insert into dipendenti (nome,cognome,luogo_di_nascita,data_di_nascita,sesso,stipendio,codice_fiscale,id_titolo_di_studio,id_ruolo_aziendale) values (?,?,?,?,?,?,?,?,?)";
			ps=conn.prepareStatement(sql);
			ps.setString(1,d.getNome());
			ps.setString(2,d.getCognome());
			ps.setString(3,d.getLuogoDiNascita());
			ps.setString(4,d.getDataDiNascita());
			ps.setString(5,d.getSesso());
			ps.setDouble(6,d.getStipendio());
			ps.setString(7,d.getCodiceFiscale());
			ps.setInt(8,d.getIdTitoloDiStudio());
			ps.setInt(9,d.getIdRuolo());
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore d'inserimento! "+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public boolean modifica(int id, Dipendente d) {
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="update dipendenti set nome=?,cognome=?,luogo_di_nascita=?,data_di_nascita=?,sesso=?,stipendio=?,codice_fiscale=?,id_titolo_di_studio=?,id_ruolo_aziendale=? where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,d.getNome());
			ps.setString(2,d.getCognome());
			ps.setString(3,d.getLuogoDiNascita());
			ps.setString(4,d.getDataDiNascita());
			ps.setString(5,d.getSesso());
			ps.setDouble(6,d.getStipendio());
			ps.setString(7,d.getCodiceFiscale());
			ps.setInt(8,d.getIdTitoloDiStudio());
			ps.setInt(9,d.getIdRuolo());
			ps.setInt(10,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di modifica!"+e.getMessage());
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public boolean cancella(int id) {
		// TODO Auto-generated method stub
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="delete from dipendenti where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1,id);		
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di cancellazione!");
			return false;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return false;
		}
		return true;
	}

	public Dipendente leggi(int id) {
		ResultSet rs=null;
		Dipendente dip=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from dipendenti where id=?";
			ps=conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1,id);
			rs=ps.executeQuery();
			if(rs.first()){
				dip=new Dipendente(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo_di_nascita"), 
						rs.getString("data_di_nascita"), 
						rs.getString("sesso"), 
						rs.getString("codice_fiscale"), 
						rs.getDouble("stipendio"), 
						rs.getInt("id_titolo_di_studio"),
						rs.getInt("id_ruolo_aziendale"));
			}
			rs.close();
			ps.close(); 
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dip;
	}

	public ArrayList<Dipendente> leggi() {
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		Dipendente dip;
		ArrayList<Dipendente> dipendenti=new ArrayList<Dipendente>();
		try {
			conn=ConnettoreDB.getDBDipendenti();
			String sql="select * from dipendenti";

			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new Dipendente(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("cognome"), 
						rs.getString("luogo_di_nascita"), 
						rs.getString("data_di_nascita"), 
						rs.getString("sesso"), 
						rs.getString("codice_fiscale"), 
						rs.getDouble("stipendio"), 
						rs.getInt("id_titolo_di_studio"),
						rs.getInt("id_ruolo_aziendale"));
				dipendenti.add(dip);
			}
			rs.close();
			ps.close();
		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return dipendenti;
	}
	
	
}