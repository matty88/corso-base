package model;

public class RuoloAziendaleDTO {
	private String ruoloAziendale;
	private int ruoli;
	private double totaleStipendio;
	
	@Override
	public String toString() {
		return "Ruolo aziendale: " + ruoloAziendale + ", Ruoli: " + ruoli + ", Totale stipendio: "
				+ totaleStipendio;
	}

	public String getRuoloAziendale() {
		return ruoloAziendale;
	}

	public void setRuoloAziendale(String ruoloAziendale) {
		this.ruoloAziendale = ruoloAziendale;
	}

	public int getRuoli() {
		return ruoli;
	}

	public void setRuoli(int ruoli) {
		this.ruoli = ruoli;
	}

	public double getTotaleStipendio() {
		return totaleStipendio;
	}

	public void setTotaleStipendio(double totaleStipendio) {
		this.totaleStipendio = totaleStipendio;
	}

	public RuoloAziendaleDTO(String ruoloAziendale, int ruoli, double totaleStipendio) {
		super();
		this.ruoloAziendale = ruoloAziendale;
		this.ruoli = ruoli;
		this.totaleStipendio = totaleStipendio;
	}

	public RuoloAziendaleDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ruoli;
		result = prime * result + ((ruoloAziendale == null) ? 0 : ruoloAziendale.hashCode());
		long temp;
		temp = Double.doubleToLongBits(totaleStipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuoloAziendaleDTO other = (RuoloAziendaleDTO) obj;
		if (ruoli != other.ruoli)
			return false;
		if (ruoloAziendale == null) {
			if (other.ruoloAziendale != null)
				return false;
		} else if (!ruoloAziendale.equals(other.ruoloAziendale))
			return false;
		if (Double.doubleToLongBits(totaleStipendio) != Double.doubleToLongBits(other.totaleStipendio))
			return false;
		return true;
	}
	
	
}

