package model;

public class TitoloDiStudioDTO {
	private String titoloDiStudio;
	private int titoliDiStudio;
	private double totaleStipendio;
	
	@Override
	public String toString() {
		return "Titolo di studio: " + titoloDiStudio + ", Numero titoli di studio: " + titoliDiStudio
				+ ", Totale stipendio: " + totaleStipendio;
	}

	public String getTitoloDiStudio() {
		return titoloDiStudio;
	}

	public void setTitoloDiStudio(String titoloDiStudio) {
		this.titoloDiStudio = titoloDiStudio;
	}

	public int getTitoliDiStudio() {
		return titoliDiStudio;
	}

	public void setTitoliDiStudio(int titoliDiStudio) {
		this.titoliDiStudio = titoliDiStudio;
	}

	public double getTotaleStipendio() {
		return totaleStipendio;
	}

	public void setTotaleStipendio(double totaleStipendio) {
		this.totaleStipendio = totaleStipendio;
	}

	public TitoloDiStudioDTO(String titoloDiStudio, int titoliDiStudio, double totaleStipendio) {
		super();
		this.titoloDiStudio = titoloDiStudio;
		this.titoliDiStudio = titoliDiStudio;
		this.totaleStipendio = totaleStipendio;
	}

	public TitoloDiStudioDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + titoliDiStudio;
		result = prime * result + ((titoloDiStudio == null) ? 0 : titoloDiStudio.hashCode());
		long temp;
		temp = Double.doubleToLongBits(totaleStipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitoloDiStudioDTO other = (TitoloDiStudioDTO) obj;
		if (titoliDiStudio != other.titoliDiStudio)
			return false;
		if (titoloDiStudio == null) {
			if (other.titoloDiStudio != null)
				return false;
		} else if (!titoloDiStudio.equals(other.titoloDiStudio))
			return false;
		if (Double.doubleToLongBits(totaleStipendio) != Double.doubleToLongBits(other.totaleStipendio))
			return false;
		return true;
	}
	
}
