package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Prospetti {

	public ArrayList<DipendenteDTO> prospettoDipendentiOrdinatoPerCognomeNome() {
		DipendenteDTO dip=null;
		ArrayList<DipendenteDTO> d=new ArrayList<>();
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         
			String sql="select * from v_dipendenti order by cognome, nome";      
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				dip=new DipendenteDTO( 
						rs.getString("Nome"), 
						rs.getString("Cognome"), 
						rs.getString("Data_di_nascita"), 
						rs.getString("Luogo_di_nascita"),
						rs.getString("Titolo_di_studio"),
						rs.getString("Ruolo_aziendale"));			
				d.add(dip);
			}
			rs.close();
			ps.close();

		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return d;
	}

	public ArrayList<RuoloAziendaleDTO> prospettoRuoloAziendale() {
		RuoloAziendaleDTO ra=null;
		ArrayList<RuoloAziendaleDTO> r=new ArrayList<>();
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         
			String sql="select * from v_ruoli_aziendali order by Ruolo_aziendale";      
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				ra=new RuoloAziendaleDTO( 
						rs.getString("Ruolo_aziendale"), 
						rs.getInt("Ruoli"), 
						rs.getDouble("Totale_stipendio"));		
				r.add(ra);
			}
			rs.close();
			ps.close();

		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return r;
	}

	public ArrayList<TitoloDiStudioDTO> prospettoTitoloDiStudio() {
		TitoloDiStudioDTO ts=null;
		ArrayList<TitoloDiStudioDTO> t=new ArrayList<>();
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         
			String sql="select * from v_titolo_di_studio order by Titolo_di_studio";      
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				ts=new TitoloDiStudioDTO( 
						rs.getString("Titolo_di_studio"), 
						rs.getInt("Titoli_di_studio"), 
						rs.getDouble("Totale_stipendio"));		
				t.add(ts);
			}
			rs.close();
			ps.close();

		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return t;
	}

	public ArrayList<RuoloAziendaleDTO> prospettoDipendenteAziendalePerRuolo() {
		RuoloAziendaleDTO ra=null;
		ArrayList<RuoloAziendaleDTO> r=new ArrayList<>();
		ResultSet rs=null;
		PreparedStatement ps;
		Connection conn;
		try {
			conn=ConnettoreDB.getDBDipendenti();                         
			String sql="select ruolo_aziendale, count(*) as 'Ruoli' from v_dipendenti group by ruolo_aziendale";      
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				ra=new RuoloAziendaleDTO( 
						rs.getString("Ruolo_aziendale"), 
						rs.getInt("Ruoli"), 
						0);		
				r.add(ra);
			}
			rs.close();
			ps.close();

		}catch(SQLException e) {
			System.out.println("Errore di lettura! "+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Errore di connessione!");
			return null;
		}
		return r;
	}

}
