package controller;

import java.util.ArrayList;
import java.util.List;

import model.Dipendente;
import model.DipendenteCRUD;
import model.Prospetti;
import view.InputOutput;
import view.Vista;

public class Menu {	
	Dipendente d, dip, dipendenteDaAggiornare;
	Prospetti pr = new Prospetti();
	Vista v = new Vista();		
	InputOutput io = new InputOutput();		
	DipendenteCRUD dc = new DipendenteCRUD();
	int scelta, idUtente;
	String risp="";

	public void menuPrincipale() {
		do {
			v.menuIniziale();
			scelta=io.leggiIntero("\nScegli una voce di menu: ");
			switch(scelta) {              
			case 1:
				addDipendente();
				break;           
			case 2:
				updateDipendente();
				break;
			case 3:
				deleteDipendente();
				break; 
			case 4:
				searchDipendente(dc.leggi());
				break; 
			case 5:
				menuDTO();		
				break; 
			case 6:
				break;			
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 6");   
			}
		} while(scelta!=6);
		v.printMsg("Programma terminato");
	}

	public void deleteDipendente() {
		v.printMsg("");
		v.stampaDipendenteArray(dc.leggi());
		do {
			idUtente=io.leggiIntero("Scegli Id del dipendente da cancellare: ");
			dip = dc.leggi(idUtente);
			if(dip==null) {
				risp=io.leggiStringa("Id non trovato, premi INVIO per riprovare.");
			}
		}while(dip==null);
		if(dip!=null) {
			v.stampaSchedaDipendente(dip);
			risp=io.leggiStringa("Vuoi confermare la cancellazione? (S/N)");
			if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
				dc.cancella(idUtente);
				v.printMsg("\nCancellazione effettuata con successo");
			} else {
				risp=io.leggiStringa("Cancellazione annullata, premi INVIO per continuare.");
			}
		}
	}

	public void updateDipendente() {
		v.printMsg("");
		v.stampaDipendenteArray(dc.leggi());
		do {
			idUtente=io.leggiIntero("Scegli Id del dipendente da modificare: ");
			dipendenteDaAggiornare = dc.leggi(idUtente);
			if(dipendenteDaAggiornare==null) {
				risp=io.leggiStringa("Id non trovato, premi INVIO per riprovare.");
			}
		}while(dipendenteDaAggiornare==null);
		if(dipendenteDaAggiornare!=null) {
			v.stampaSchedaDipendente(dipendenteDaAggiornare);
			risp=io.leggiStringa("Vuoi confermare la modifica? (S/N)");
			if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
				v.modificaDipendente(dipendenteDaAggiornare);
				dc.modifica(idUtente, dipendenteDaAggiornare);
				v.printMsg("Modifica eseguita con successo!");
				v.stampaDipendenteArray(dc.leggi());
			} else {
				risp=io.leggiStringa("Modifica annullata, premi INVIO per continuare.");
			}
		}
	}

	public void addDipendente() {
		v.printMsg("\nInserisci dipendente");
		d = new Dipendente();
		v.schedaDipendenteInserimento(d);
		dc.inserisci(d);
		v.printMsg("\nInserimento eseguito con successo!");
	}

	private void searchDipendente(ArrayList<Dipendente> d) {		
		int scelta;
		String nome, risposta;
		do{
			v.menuRicerca();
			scelta=io.leggiIntero("Scegli: ");
			switch(scelta) {
			case 1:
				nome=io.leggiStringa("\nInserisci nome: ");
				v.ricercaPerNome(nome, dc.leggi());
				break;
			case 2:
				nome=io.leggiStringa("\nInserisci cognome: ");
				v.ricercaPerCognome(nome, dc.leggi());
				risposta=io.leggiStringa("\nVisualizza per cognome (S/N)?: ");
				if(risposta.equalsIgnoreCase("s") || risposta.equalsIgnoreCase("si")) {
					//v.ordinamentoCognome(dc.leggi());
					v.ordinamentoCognomeBis(dc.leggi());
					//dc.leggi().forEach(Vista::stampa);
				} else {
					risposta=io.leggiStringa("\nPremi INVIO per continuare.");
				}
				break;
			case 3:
				break;
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 2");
				break;
			}
		}while(scelta!=3);
	}
	
	private void menuDTO() {
		int scelta;
		do {
			v.menuInizialeDTO();
			scelta=io.leggiIntero("\nScegli una voce di menu: ");
			switch(scelta) {              
			case 1:
				v.stampaDipendenteArray(dc.leggi());
				break;           
			case 2:
				v.stampaDipendenteDTO(pr.prospettoDipendentiOrdinatoPerCognomeNome());
				break;
			case 3:
				v.stampaRuoloAziendaleDTO(pr.prospettoRuoloAziendale());
				break; 
			case 4:
				v.stampaTitoloDiStudioDTO(pr.prospettoTitoloDiStudio());
				break; 
			case 5:
				v.stampaDipendenteAziendalePerRuoloDTO(pr.prospettoDipendenteAziendalePerRuolo());		
				break; 
			case 6:
				break;			
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 6");   
			}
		} while(scelta!=6);
	}
}
