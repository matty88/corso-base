package view;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import model.Dipendente;
import model.DipendenteDTO;


public class Vista {

	Scanner scanner = new Scanner(System.in);

	public void printMsg(String msg) {
		System.out.println(msg);
	}

	public void menuInizialeInserimento() {
		printMsg("1) Inserisci");
		printMsg("2) Modifica");
		printMsg("3) Cancella");   
		printMsg("4) Cerca");   
		printMsg("5) Visualizza");
		printMsg("6) Salva");
		printMsg("7) Esci");
	}

	public void menuPagine() {
		printMsg("1) Prima pagina 2) Pagina precedente 3) Pagina successiva 4) Ultima pagina 5) Scegli");
	}

	public void schedaDipendenteInserimento(Dipendente d){	   
		d.setNome(leggiStringa("Nome: "));
		d.setCognome(leggiStringa("Cognome: "));
		d.setDataDiNascita(Date.valueOf((leggiStringa("Data di nascita: "))));
		d.setLuogoDiNascita(leggiStringa("Luogo di nascita: "));
		d.setSesso(leggiStringa("Sesso: "));
		d.setCodiceFiscale(leggiStringa("Codice fiscale: ").toUpperCase());;
		d.setStipendio(leggiDecimale("Stipendio: "));
		d.setIdRuoloAziendale(leggiIntero("Ruolo aziendale: "));
		d.setIdTitoloDiStudio(leggiIntero("Titolo di studio: "));
	}

	public void modificaDipendente(Dipendente d){
		String appAttributo=""; 
		double appAttributoDecimale=0.0;
		int appAttributoIntero=0;
		appAttributo= leggiStringa("Nome:["+d.getNome()+"]:");
		if(!appAttributo.equals("")) {
			d.setNome(appAttributo);
		}
		appAttributo= leggiStringa("Cognome:["+d.getCognome()+"]:");
		if(!appAttributo.equals("")) {
			d.setCognome(appAttributo);
		}
		appAttributo= leggiStringa("Data di nascita:["+d.getDataDiNascita()+"]:");
		if(!appAttributo.equals("")) {
			d.setDataDiNascita(Date.valueOf((appAttributo)));
		}
		appAttributo= leggiStringa("Luogo di nascita:["+d.getLuogoDiNascita()+"]:");
		if(!appAttributo.equals("")) {
			d.setLuogoDiNascita(appAttributo);
		}
		appAttributo= leggiStringa("Sesso:["+d.getSesso()+"]:");
		if(!appAttributo.equals("")) {
			d.setSesso(appAttributo);
		}
		appAttributo= leggiStringa("Codice fiscale:["+d.getCodiceFiscale().toUpperCase()+"]:");
		if(!appAttributo.equals("")) {
			d.setCodiceFiscale(appAttributo);
		}
		appAttributoDecimale= leggiDecimale("Stipendio:["+d.getStipendio()+"]:");
		if(appAttributoDecimale!=0.0) {
			d.setStipendio(appAttributoDecimale);
		}
		appAttributoIntero= leggiIntero("Titolo di studio:["+d.getIdTitoloDiStudio()+"]:");
		if(appAttributoIntero!=0) {
			d.setIdTitoloDiStudio(appAttributoIntero);
		}
		appAttributoIntero= leggiIntero("Ruolo aziendale:["+d.getIdRuoloAziendale()+"]:");
		if(appAttributoIntero!=0) {
			d.setIdRuoloAziendale(appAttributoIntero);
		}
	}

	public void cerca(int id, HashMap<Integer,Dipendente>dMap) {
		if (dMap.containsKey(id)) {
			System.out.println(dMap.get(id));
		}
	}

	public void stampaMap(HashMap<Integer,Dipendente>dMap) {
		for(int key:dMap.keySet()) {
			System.out.println(dMap.get(key));
		}
	}

	public void stampaDipendenteDTO(ArrayList<DipendenteDTO> dList) {
		for(DipendenteDTO d: dList) {
			System.out.println(d);
		}
	}

	public void ricerca(int id, ArrayList<DipendenteDTO> dList) {
		boolean flag = false;
		for(int i=0; i<dList.size(); i++) {
			if(id==dList.get(i).getId()) {
				printMsg("");
				printMsg(dList.get(i).toString());
				flag = true;
			}
		}
		if (!flag) {
			printMsg("Non ho trovato niente con il nome: "+id);
		}
	}

	public String leggiStringa(String msg) {
		printMsg(msg);
		return scanner.nextLine();
	} 

	public int leggiIntero(String num) {
		while(true){  //WHILE TRUE
			try { 
				printMsg(num);
				return Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}

	public double leggiDecimale(String num) {  
		while(true){ 
			try {          
				printMsg(num);                           
				return Double.parseDouble(scanner.nextLine());    
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero decimale valido!");
			}
		}
	}

	public Date leggiData(String msg) {
		while(true){
			try {
				printMsg(msg);
				return Date.valueOf(scanner.nextLine());
			} catch(IllegalArgumentException e){
				printMsg("Errore formato data: "+e.getMessage());
			}
		}
	}
}
