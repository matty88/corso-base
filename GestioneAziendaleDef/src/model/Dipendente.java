package model;

import java.sql.Date;

public class Dipendente extends Persona {
	
	private double stipendio;
	private int idRuoloAziendale;
	private int idTitoloDiStudio;
	
	public double getStipendio() {
		return stipendio;
	}
	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	public int getIdRuoloAziendale() {
		return idRuoloAziendale;
	}
	public void setIdRuoloAziendale(int idRuoloAziendale) {
		this.idRuoloAziendale = idRuoloAziendale;
	}
	public int getIdTitoloDiStudio() {
		return idTitoloDiStudio;
	}
	public void setIdTitoloDiStudio(int idTitoloDiStudio) {
		this.idTitoloDiStudio = idTitoloDiStudio;
	}
	
	public Dipendente(int id, String nome, String cognome, Date dataDiNascita, String luogoDiNascita, String sesso,
			String codiceFiscale, double stipendio, int idRuoloAziendale, int idTitoloDiStudio) {
		super(id, nome, cognome, dataDiNascita, luogoDiNascita, sesso, codiceFiscale);
		this.stipendio = stipendio;
		this.idRuoloAziendale = idRuoloAziendale;
		this.idTitoloDiStudio = idTitoloDiStudio;
	}
	
	public Dipendente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return super.toString()+", " +stipendio + ", " +idRuoloAziendale + ", "+ idTitoloDiStudio;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + idRuoloAziendale;
		result = prime * result + idTitoloDiStudio;
		long temp;
		temp = Double.doubleToLongBits(stipendio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dipendente other = (Dipendente) obj;
		if (idRuoloAziendale != other.idRuoloAziendale)
			return false;
		if (idTitoloDiStudio != other.idTitoloDiStudio)
			return false;
		if (Double.doubleToLongBits(stipendio) != Double.doubleToLongBits(other.stipendio))
			return false;
		return true;
	}

}
