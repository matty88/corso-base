package model;

import java.util.ArrayList;

public interface IDipendenteCrudDB {
	
	public boolean inserisci(Dipendente d); 
	
	public boolean modifica(int id, Dipendente d);
	
	public boolean cancella(int id);
	
	public Dipendente leggi(int id);
	
	public ArrayList<DipendenteDTO> leggi(int lunghezzaPagina, int paginaCorrente);
	
	public ArrayList<DipendenteDTO> leggiDTO();
	
	public int getTotalePagine(int lunghezzaPagina);

}
