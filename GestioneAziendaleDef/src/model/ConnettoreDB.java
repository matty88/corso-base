package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnettoreDB {
	private static Connection conn=null;
	public static Connection getDB() {
	     try {
	          if(conn==null) {      //SE CONN E' NULL, ALLORA ENTRA, ESEGUE LA CONNESSIONE E POI RESTITUISCE CONN, SE CONN ESISTE GIA' ALLORA RESTITUIRA' DIRETTAMENTE CONN
	            Class.forName("com.mysql.cj.jdbc.Driver");   //CARICARE IL DRIVER PER APRIRE LA CONNESSIONE CON FORNAME CHE E' UN METODO STATICA CHE STA NELLA CLASSE CLASS. E' IL PRIMO COMANDO PER GESTIRE UNA CONNESSIONE CON UN DATABASE
	            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_gestione_azienda?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");   //SE VA A BUON FINE IL CARICAMENTO DEL DRIVER, SI APRE LA CONNESSIONE COL COMANDO getConnection. LA STRINGA DI CONNESSIONE VUOLE IL DBMS,NOME DEL SERVER CON IP,NOME DB,TIMEZONE,NOME UTENTE E PASSWORD
	            }
	         } catch(ClassNotFoundException e){        //ECCEZIONE CREATA SE IL DRIVER NON C'E' O E' SBAGLIATO QUINDI IL FORNAME
	             e.printStackTrace();
	         } catch(SQLException e) {
	             e.printStackTrace();
	         }
	     return conn;
	   }
	private ConnettoreDB(){}
	}

//QUESTA E' UNA CLASSE SINGLETON ED E' STATO INSERITO QUI PERCH� VOGLIAMO EVITARE DI CREARE
//TANTE CONNESSIONI
//COS'E' IL SINGLETON: UNA CLASSE O PATTERN SINGLETON E' UNA CLASSE CHE NON DEVE ISTANZIARE OGGETTI
//PER CREARE UNA CLASSE NORMALE CHE NON ISTANZIA OGGETTI BASTA DICHIARARE IL COSTRUTTORE PRIVATO CHE
//DI CONSEGUENZA NON PUO' ISTANZIARE NULLA
//I METODI STATICI POSSONO ESSERE USATI SENZA ISTANZA CHIAMANDO LA CLASSE ConnettoreDB.getDB()
