package model;

public class TitoloDiStudio {
	
	private int id;
	private String titoloDiStudio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitoloDiStudio() {
		return titoloDiStudio;
	}
	public void setTitoloDiStudio(String titoloDiStudio) {
		this.titoloDiStudio = titoloDiStudio;
	}
	
	public TitoloDiStudio(int id, String titoloDiStudio) {
		super();
		this.id = id;
		this.titoloDiStudio = titoloDiStudio;
	}
	
	public TitoloDiStudio() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", titoloDiStudio=" + titoloDiStudio;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((titoloDiStudio == null) ? 0 : titoloDiStudio.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitoloDiStudio other = (TitoloDiStudio) obj;
		if (id != other.id)
			return false;
		if (titoloDiStudio == null) {
			if (other.titoloDiStudio != null)
				return false;
		} else if (!titoloDiStudio.equals(other.titoloDiStudio))
			return false;
		return true;
	}

}
