package controller;

import model.Dipendente;
import model.DipendenteCrud;
import model.DipendenteCrudDB;
import view.Vista;

public class Avvio {

	public static void main(String[] args) {  
		Vista v = new Vista();
		DipendenteCrud dc = new DipendenteCrud();
		DipendenteCrudDB dcdb = new DipendenteCrudDB();
		Dipendente d;
		int scelta;
		String risp;
		do {
			v.menuInizialeInserimento();
			scelta=v.leggiIntero("Scegli una voce di menu: ");
			switch(scelta) {              
			case 1:
				d=new Dipendente();
				v.schedaDipendenteInserimento(d);
				boolean inserimento = dcdb.inserisci(d);
				if(inserimento) {
					v.printMsg("\nInserimento avvenuto con successo!");
				} else {
					v.printMsg("\nId gi� esistente, inserisci un nuovo Id!");
				}
				break;           
			case 2:
				v.printMsg("");
				paginazione(v, dcdb);
				scelta=v.leggiIntero("Scegli Id della persona da modificare: ");
				d=dcdb.leggi(scelta);
				if(d!=null) {
					v.ricerca(scelta,dcdb.leggiDTO());
					risp=v.leggiStringa("Vuoi confermare la modifica? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						v.modificaDipendente(d);
						dcdb.modifica(scelta, d);
						v.printMsg("Modifica eseguita con successo!");
						paginazione(v, dcdb);
					} else {
						risp=v.leggiStringa("Modifica annullata, premi invio per riprovare.");
					}
				} else {
					risp=v.leggiStringa("Id non trovato!");
				}
				break;
			case 3:
				v.printMsg("");
				paginazione(v, dcdb);
				scelta=v.leggiIntero("Scegli Id della persona da cancellare: ");
				d=dcdb.leggi(scelta);
				if(d!=null) {			
					v.ricerca(scelta,dcdb.leggiDTO());
					risp=v.leggiStringa("Vuoi confermare la cancellazione? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						dcdb.cancella(scelta);
						v.printMsg("\nCancellazione effettuata con successo");
						paginazione(v, dcdb);
					} else {
						risp=v.leggiStringa("Cancellazione annullata, premi INVIO per continuare.");
					} 
				} else {
					risp=v.leggiStringa("Id non trovato! Premi invio per riprovare!");
				}	
				break; 
			case 4:
				scelta=v.leggiIntero("Inserisci Id da ricercare: ");
				v.ricerca(scelta,dcdb.leggiDTO());
				break; 
			case 5:
				paginazione(v, dcdb);
				break;
			case 6:
				boolean salvataggio=dc.salva();
				if (salvataggio) {
					v.printMsg("File salvato con successo!");
				} else {
					v.printMsg("Salvataggio non riuscito. Riprova!");	
				}
				break;
			case 7:
				break;
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 7");   
			}
		} while(scelta!=7);
		v.printMsg("Programma terminato");
	}

	private static void paginazione(Vista v, DipendenteCrudDB dcdb) {
		int sceltaPag;
		int lunghezzaPagina = 3;
		int paginaCorrente = 1;
		do {
			v.stampaDipendenteDTO(dcdb.leggi(lunghezzaPagina, paginaCorrente));
			v.menuPagine();
			sceltaPag=v.leggiIntero("Scegli una voce di menu: ");
			switch(sceltaPag) {
			case 1:
				paginaCorrente = 1;
				break;
			case 2:
				if(paginaCorrente>1) {
					paginaCorrente--;				
				}
				break;
			case 3:
				if(paginaCorrente<dcdb.getTotalePagine(lunghezzaPagina)) {
					paginaCorrente++;			
				}
				break;
			case 4:
				paginaCorrente = dcdb.getTotalePagine(lunghezzaPagina);
				break;
			case 5:
				break; 
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 5");   
			}
		} while(sceltaPag!=5);
	}
}

