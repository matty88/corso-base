package model;

public class Triangolo extends FiguraGeometrica {

	private int latoA;
	private int latoB;
	private int latoC;
	
	public int getLatoA() {
		return latoA;
	}
	public void setLatoA(int latoA) {
		this.latoA = latoA;
	}
	public int getLatoB() {
		return latoB;
	}
	public void setLatoB(int latoB) {
		this.latoB = latoB;
	}
	public int getLatoC() {
		return latoC;
	}
	public void setLatoC(int latoC) {
		this.latoC = latoC;
	}
	public Triangolo(int id, int latoA, int latoB, int latoC) {
		super(id, "Triangolo");
		this.latoA = latoA;
		this.latoB = latoB;
		this.latoC = latoC;
	}
	public Triangolo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return super.toString() +"LatoA=" + latoA + ", latoB=" + latoB + ", latoC=" + latoC;
	}
	
	@Override
	public double getPerimetro() {
		return latoA+latoB+latoC;
	}

	@Override
	public double getArea() {
		return Math.sqrt((latoA+latoB+latoC/2)*((latoA+latoB+latoC)-latoA)*((latoA+latoB+latoC)-latoB)*((latoA+latoB+latoC)-latoC));
	}
	
}
