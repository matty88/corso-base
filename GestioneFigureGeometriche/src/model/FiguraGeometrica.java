package model;

public abstract class FiguraGeometrica {
	
	private int id;
	private String tipoFormaGeometrica;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipoFormaGeometrica() {
		return tipoFormaGeometrica;
	}

	public void setTipoFormaGeometrica(String tipoFormaGeometrica) {
		this.tipoFormaGeometrica = tipoFormaGeometrica;
	}

	public abstract double getPerimetro();
	
	public abstract double getArea();


	public FiguraGeometrica(int id, String tipoFormaGeometrica) {
		super();
		this.id = id;
		this.tipoFormaGeometrica = tipoFormaGeometrica;
	}

	public FiguraGeometrica() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Id=" + id + ", TipoFormaGeometrica=" + tipoFormaGeometrica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((tipoFormaGeometrica == null) ? 0 : tipoFormaGeometrica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FiguraGeometrica other = (FiguraGeometrica) obj;
		if (id != other.id)
			return false;
		if (tipoFormaGeometrica == null) {
			if (other.tipoFormaGeometrica != null)
				return false;
		} else if (!tipoFormaGeometrica.equals(other.tipoFormaGeometrica))
			return false;
		return true;
	}
}
