package model;

public class Quadrato extends FiguraGeometrica {
	
	private double lato;

	public double getLato() {
		return lato;
	}

	public void setLato(double lato) {
		this.lato = lato;
	}

	public Quadrato(int id, double lato) {
		super(id, "Quadrato");
		this.lato = lato;
	}

	public Quadrato() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() +"Lato=" + lato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(lato);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quadrato other = (Quadrato) obj;
		if (Double.doubleToLongBits(lato) != Double.doubleToLongBits(other.lato))
			return false;
		return true;
	}

	@Override
	public double getPerimetro() {
		return lato*4;
	}

	@Override
	public double getArea() {
		return Math.pow(lato, 2);
	}
}
