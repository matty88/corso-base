package model;

import java.util.ArrayList;
import java.util.List;

public class Crud {
	
	List<FiguraGeometrica>fList = new ArrayList<FiguraGeometrica>();

	public boolean inserisci(FiguraGeometrica f) {   
		fList.add(f);
		return true;
	}

	public List<FiguraGeometrica> leggi() {
		return fList;                        
	}
	
	public FiguraGeometrica leggi(int index) {          
		return fList.get(index);                
	}

	public boolean modifica(int id, FiguraGeometrica f) {  
		fList.set(id, f);   
		return true;
	}

	public boolean cancella(int id) {
		fList.remove(id);
		return true;
	}
	
	public FiguraGeometrica cercaIdFigura(int id) {
		return (id!=-1) ? leggi().stream()                         
				.filter(f -> id==f.getId())  
				.findFirst().orElse(null)           
				:null;                      
	}

}
