package model;

public class Cerchio extends FiguraGeometrica {

	private double raggio;

	public double getRaggio() {
		return raggio;
	}

	public void setRaggio(double raggio) {
		this.raggio = raggio;
	}

	public Cerchio(int id, double raggio) {
		super(id, "Cerchio");
		this.raggio = raggio;
	}

	public Cerchio() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() +"Raggio= "+raggio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(raggio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cerchio other = (Cerchio) obj;
		if (Double.doubleToLongBits(raggio) != Double.doubleToLongBits(other.raggio))
			return false;
		return true;
	}

	@Override
	public double getPerimetro() {
		return 2*Math.PI*raggio;
	}

	@Override
	public double getArea() {
		return Math.pow(raggio,2)*Math.PI;
	}
}
