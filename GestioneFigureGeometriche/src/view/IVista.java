package view;

public interface IVista {
	
	public int leggiIntero(String msg);
	
	public double leggiDecimale(String msg);
	
	public String leggiStringa(String msg);
}
