package view;

import java.util.List;
import java.util.Scanner;

import model.Cerchio;
import model.FiguraGeometrica;
import model.Quadrato;
import model.Rettangolo;
import model.Triangolo;

public class Vista {

	Scanner scanner = new Scanner(System.in);

	public void printMsg(String msg) {
		System.out.println(msg);
	}

	public void menuInizialePrincipale() {
		printMsg("1) Inserimento");
		printMsg("2) Modifica");
		printMsg("3) Cancella");
		printMsg("4) Cerca");
		printMsg("5) Visualizza");
		printMsg("6) Esci");
	}

	public void menuInizialeInserimento() {
		System.out.println("1) Cerchio");
		System.out.println("2) Quadrato");
		System.out.println("3) Rettangolo");   
		System.out.println("4) Triangolo");   
		System.out.println("5) Torna al menu principale");
	}

	public void inserimentoFiguraGeometrica(FiguraGeometrica obj) {
		printMsg("");
		obj.setId(leggiIntero("Inserisci Id:"));
		if(obj instanceof Triangolo) {
			Triangolo t = (Triangolo) obj;
			t.setTipoFormaGeometrica("Triangolo");
			t.setLatoA(leggiIntero("Inserisci valore lato A: "));
			t.setLatoB(leggiIntero("Inserisci valore lato B: "));
			t.setLatoC(leggiIntero("Inserisci valore lato C: "));
		} else if (obj instanceof Rettangolo) {
			Rettangolo r = (Rettangolo) obj;
			r.setTipoFormaGeometrica("Rettangolo");
			r.setLatoMaggiore(leggiIntero("Inserisci valore lato maggiore: "));	
			r.setLatoMinore(leggiIntero("Inserisci valore lato minore: "));	
		} else if (obj instanceof Quadrato) {	
			Quadrato q = (Quadrato) obj;
			q.setTipoFormaGeometrica("Quadrato");
			q.setLato(leggiDecimale("Inserisci valore lato: "));
		} else if (obj instanceof Cerchio) {
			Cerchio c = (Cerchio) obj;
			c.setTipoFormaGeometrica("Cerchio");
			c.setRaggio(leggiDecimale("Inserisci valore raggio: "));
		}
	}

	public void modificaForma(FiguraGeometrica obj) {
		boolean flag;
		String appAttributo; 
		printMsg("");
		if(obj instanceof Triangolo) {
			Triangolo t = (Triangolo) obj;
			do {
				flag=false;
				appAttributo = leggiStringa("Lato A:["+t.getLatoA()+"]:");
				if(!appAttributo.equals("")) {
					try {
						t.setLatoA(Integer.parseInt(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			do {
				flag=false;
				appAttributo = leggiStringa("Lato B:["+t.getLatoB()+"]:");
				if(!appAttributo.equals("")) {
					try {
						t.setLatoB(Integer.parseInt(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			do {
				flag=false;
				appAttributo = leggiStringa("Lato C:["+t.getLatoC()+"]:");
				if(!appAttributo.equals("")) {
					try {
						t.setLatoC(Integer.parseInt(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
		} else if (obj instanceof Rettangolo) {
			Rettangolo r = (Rettangolo) obj;
			do {
				flag=false;
				appAttributo = leggiStringa("Lato maggiore:["+r.getLatoMaggiore()+"]:");
				if(!appAttributo.equals("")) {
					try {
						r.setLatoMaggiore(Integer.parseInt(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
			do {
				flag=false;
				appAttributo = leggiStringa("Lato minore:["+r.getLatoMinore()+"]:");
				if(!appAttributo.equals("")) {
					try {
						r.setLatoMinore(Integer.parseInt(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero intero!");
						flag=true;
					}
				}
			}while(flag);
		} else if (obj instanceof Quadrato) {	
			Quadrato q = (Quadrato) obj;
			do {
				flag=false;
				appAttributo = leggiStringa("Lato:["+q.getLato()+"]:");
				if(!appAttributo.equals("")) {
					try {
						q.setLato(Double.parseDouble(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero decimale!");
						flag=true;
					}
				}
			}while(flag);
		} else if (obj instanceof Cerchio) {	
			Cerchio c = (Cerchio) obj;
			do {
				flag=false;
				appAttributo = leggiStringa("Lato:["+c.getRaggio()+"]:");
				if(!appAttributo.equals("")) {
					try {
						c.setRaggio(Double.parseDouble(appAttributo));
					} catch (NumberFormatException e) {
						printMsg("Inserire un numero decimale!");
						flag=true;
					}
				}
			}while(flag);
		}
	}

	public static void stampaInformazioni(FiguraGeometrica obj) {
		System.out.println("");
		System.out.println("Id: "+obj.getId());
		System.out.println("Figura geometrica: "+obj.getTipoFormaGeometrica());
		if(obj instanceof Triangolo) {
			Triangolo t = (Triangolo) obj;
			System.out.println("Lato A: "+t.getLatoA());
			System.out.println("Lato B: "+t.getLatoB());
			System.out.println("Lato C: "+t.getLatoC());
		} else if (obj instanceof Rettangolo) {
			Rettangolo r = (Rettangolo) obj;
			System.out.println("Lato maggiore: "+r.getLatoMaggiore());
			System.out.println("Lato minore: "+r.getLatoMinore());
		} else if (obj instanceof Quadrato) {
			Quadrato q = (Quadrato) obj;
			System.out.println("Lato: "+q.getLato());
		} else if (obj instanceof Cerchio) {
			Cerchio c = (Cerchio) obj;
			System.out.println("Lato: "+c.getRaggio());
		}
		System.out.println("\nIl perimetro del "+obj.getTipoFormaGeometrica()+ " �: "+obj.getPerimetro());
		System.out.println("L'area del "+obj.getTipoFormaGeometrica()+ " �: "+obj.getArea() +"\n");
	}

	public void stampaSchede(List<FiguraGeometrica> fList) {
		if(fList !=null && !fList.isEmpty()) {
			fList.forEach(Vista::stampaInformazioni);
		}
	}
	
	public void cercaScheda(FiguraGeometrica f) {
		if(f !=null) {
			stampaInformazioni(f);
		} else {
			printMsg("Nessuna figura geometrica trovata!");
		}
	}
	
	public int cercaId(int id, List<FiguraGeometrica> fList) {
		for(int i=0;i<fList.size();i++) {
			if(id==fList.get(i).getId()) {
				return i;
			}
		}
		return -1;
	}

	public String leggiStringa(String msg) {
		printMsg(msg);
		return scanner.nextLine();
	} 

	public int leggiIntero(String num) {
		while (true) {  
			try { 
				printMsg(num);
				return Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero intero valido!");
			}
		}   
	}

	public double leggiDecimale(String num) {  
		while (true) { 
			try {          
				printMsg(num);                           
				return Double.parseDouble(scanner.nextLine());    
			} catch (NumberFormatException e) {
				printMsg("***ERRORE: non hai inserito un numero decimale valido!");
			}
		}
	}
}
