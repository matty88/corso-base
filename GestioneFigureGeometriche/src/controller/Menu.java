package controller;

import model.Cerchio;
import model.Crud;
import model.FiguraGeometrica;
import model.Quadrato;
import model.Rettangolo;
import model.Triangolo;
import view.Vista;

public class Menu {
	Vista v = new Vista();
	Crud c = new Crud();
	FiguraGeometrica obj;
	
	public void menuPrincipale() {
		int scelta;
		String risp;
		do {
			v.menuInizialePrincipale();
			scelta=v.leggiIntero("Scegli una voce di menu: \n");
			switch(scelta) {              
			case 1:
				menuInserimento();
				break; 
			case 2:
				v.printMsg("");
				v.stampaSchede(c.leggi());
				do {
					scelta=v.leggiIntero("Scegli Id della persona da modificare: ");
					scelta=v.cercaId(scelta, c.leggi());
					if(scelta==-1) {
						risp=v.leggiStringa("Id non trovato, premi INVIO per riprovare.");
					}
				}while(scelta==-1);
				if(scelta!=-1) {
					obj=c.leggi(scelta);
					v.stampaSchede(c.leggi());
					risp=v.leggiStringa("Vuoi confermare la modifica? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						v.modificaForma(obj);
						v.printMsg("Modifica eseguita con successo!");
						v.stampaSchede(c.leggi());
					} else {
						risp=v.leggiStringa("Modifica annullata, premi INVIO per continuare.");
					}
				}
				break;
			case 3:			
				v.printMsg("");
				v.stampaSchede(c.leggi());
				do {
					scelta=v.leggiIntero("Scegli Id della persona da cancellare: ");
					scelta=v.cercaId(scelta, c.leggi());
					if(scelta==-1) {
						risp=v.leggiStringa("Id non trovato, premi INVIO per riprovare.");
					}
				}while(scelta==-1);
				if(scelta!=-1) {
					obj=c.leggi(scelta);
					v.stampaSchede(c.leggi());
					risp=v.leggiStringa("Vuoi confermare la cancellazione? (S/N)");
					if(risp.equalsIgnoreCase("s") || risp.equalsIgnoreCase("si")) {
						c.cancella(scelta);
						v.printMsg("\nCancellazione effettuata con successo");
					} else {
						risp=v.leggiStringa("Cancellazione annullata, premi INVIO per continuare.");
					}
				}
				break;
			case 4:
				int idFigura = v.leggiIntero("Inserisci Id: ");
				v.cercaScheda(c.cercaIdFigura(idFigura));
				break;
			case 5:
				v.stampaSchede(c.leggi());
				break;
			case 6:
				break; 
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 6");   
			}
		} while(scelta!=6);
		v.printMsg("Programma terminato");
	}
	
	private void menuInserimento() {
		int scelta;
		do {
			v.menuInizialeInserimento();
			scelta=v.leggiIntero("Quale figura geometrica vuoi inserire?: ");
			switch(scelta) {              
			case 1:
				sceltaOggetto(scelta);
				break;           
			case 2:
				sceltaOggetto(scelta);
				break;
			case 3:
				sceltaOggetto(scelta);
				break; 
			case 4:
				sceltaOggetto(scelta);
				break; 
			case 5:
				break;			
			default:
				v.printMsg("Valore non consentito: inserire un valore da 1 a 5");   
			}
		} while(scelta!=5);
	}

	private void sceltaOggetto(int num) {
		if(num==1) {
			obj = new Cerchio(); 
		} else if (num==2) {	
			obj = new Quadrato();
		} else if (num==3) {	
			obj = new Rettangolo(); 
		} else {
			obj = new Triangolo();
		}
		v.inserimentoFiguraGeometrica(obj);
		c.inserisci(obj);
		v.printMsg("\nInserimento avvenuto con successo!");
	}
}
