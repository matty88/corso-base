package gestoreFatture;

public class Fattura {
	public int numero;
	public String data;
	public String cliente;
	public double imponibile;
	
	@Override
	public String toString() {
		return "Fattura [numero=" + numero + ", cliente=" + cliente + ", data=" + data + ", imponibile=" + imponibile
				+ "]";
	}
	

}
