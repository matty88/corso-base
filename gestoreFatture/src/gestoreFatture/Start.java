package gestoreFatture;

public class Start {
	
	public static void main(String[] args) {
		ControlloFatture cf = new ControlloFatture();
		Menu menu = new Menu();
		//Fattura[] fatture= cf.registraFatture();
		Fattura[] fatture = valoriTest();	
		menu.menuIniziale(fatture);
		
	}

	private static Fattura[] valoriTest() {
		Fattura[] fatture = new Fattura[10];
		fatture[0]=new Fattura();
		fatture[1]=new Fattura();
		fatture[2]=new Fattura();
		fatture[3]=new Fattura();
		fatture[4]=new Fattura();
		fatture[5]=new Fattura();
		fatture[6]=new Fattura();
		fatture[7]=new Fattura();
		fatture[8]=new Fattura();
		fatture[9]=new Fattura();
		fatture[0].cliente="antonio";
		fatture[1].cliente="peppe";
		fatture[2].cliente="giacomo";
		fatture[3].cliente="giacomo";
		fatture[4].cliente="brancaccio";
		fatture[5].cliente="brancaccio";
		fatture[6].cliente="brancaccio";
		fatture[7].cliente="salvatore";
		fatture[8].cliente="salvatore";
		fatture[9].cliente="mario";
		fatture[0].data="5 marzo";
		fatture[1].data="8 luglio";
		fatture[2].data="30 settembre";
		fatture[3].data="6 aprile";
		fatture[4].data="4 giugno";
		fatture[5].data="29 maggio";
		fatture[6].data="4 febbraio";
		fatture[7].data="9 novembre";
		fatture[8].data="16 ottobre";
		fatture[9].data="24 dicembre";
		fatture[0].numero=10;
		fatture[1].numero=20;
		fatture[2].numero=30;
		fatture[3].numero=56;
		fatture[4].numero=54;
		fatture[5].numero=21;
		fatture[6].numero=35;
		fatture[7].numero=45;
		fatture[8].numero=46;
		fatture[9].numero=567;
		fatture[0].imponibile=50.00;
		fatture[1].imponibile=23.50;
		fatture[2].imponibile=45.00;
		fatture[3].imponibile=64.98;
		fatture[4].imponibile=38.67;
		fatture[5].imponibile=20.56;
		fatture[6].imponibile=3.56;
		fatture[7].imponibile=18.46;
		fatture[8].imponibile=90.52;
		fatture[9].imponibile=78.37;
		return fatture;
	}

}
