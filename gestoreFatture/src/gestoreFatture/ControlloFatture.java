package gestoreFatture;

public class ControlloFatture {

	Metodi m = new Metodi();

	public Fattura[] registraFatture() {
		Fattura[] fatture = new Fattura[10];
		for(int index=0;index<fatture.length;index++) {	
			fatture[index]=new Fattura();
			fatture[index].numero = m.leggiIntero("Inserisci numero della fattura "+(index+1)+":");
			fatture[index].data = m.leggiStringa("Inserisci data della fattura "+(index+1)+":");
			fatture[index].cliente = m.leggiStringa("Inserisci cliente della fattura "+(index+1)+":");
			fatture[index].imponibile = m.leggiDecimale("Inserisci imponibile della fattura "+(index+1)+":");
		}
		return fatture;
	}

	public void stampaFatture(Fattura[] fatture) {
		for(int x=0; x<fatture.length; x++ ) {
			System.out.println(x+1+")"+fatture[x]);
		}
	}

	public void stampaFattureImponibile(Fattura[] fatture) {
		double imponibileTot = 0;
		for(int x=0; x<fatture.length; x++ ) {
			double addImp = fatture[x].imponibile;
			imponibileTot += addImp;
			System.out.println(x+1+")"+fatture[x]);
		}
		System.out.println("Totale imponibile: "+imponibileTot);
	}

	public void stampaFattureImpostaImporto(Fattura[] fatture) {
		double imponibileTot = 0; double impostaTot = 0; double importoTot = 0;
		for(int x=0; x<fatture.length; x++ ) {
			double addImp = fatture[x].imponibile;
			double imposta=addImp*0.22;
			double importo=addImp+imposta;
			imponibileTot += addImp;
			impostaTot += imposta;
			importoTot += importo;
			System.out.println(x+1+")"+fatture[x]);
			System.out.println(x+1+")Imposta: "+imposta);	
			System.out.println(x+1+")Importo: "+importo);	
		}
		System.out.println("Totale imponibile: "+imponibileTot);
		System.out.println("Totale imposta: "+impostaTot);
		System.out.println("Totale importo: "+importoTot);
	}

	public void ordinamentoFatturaNumero(Fattura[] fatture) {
		for(int i=0;i<fatture.length;i++) {
			for(int x=0;x<(fatture.length-1-i);x++) {
				if(fatture[x].numero>fatture[x+1].numero) {
					//SWAP
					Fattura temp=fatture[x];
					fatture[x]=fatture[x+1];
					fatture[x+1]=temp;
				}
			}
		}
		for(int cont=0;cont<fatture.length;cont++) {
			System.out.println(cont+1+")"+"["+fatture[cont].toString()+"]");
		}
	}

	public void ordinamentoFatturaCliente(Fattura[] fatture) {
		for(int i=0;i<fatture.length;i++) {
			for(int x=0;x<(fatture.length-1-i);x++) {
				String chiaveOrdinamentoPrimaria = fatture[x].cliente+fatture[x].numero;
				String chiaveOrdinamentoSecondaria = fatture[x+1].cliente+fatture[x+1].numero;
				if(chiaveOrdinamentoPrimaria.compareTo(chiaveOrdinamentoSecondaria)>0) {
					//SWAP
					Fattura temp=fatture[x];
					fatture[x]=fatture[x+1];
					fatture[x+1]=temp;
				}
			}
		}
		for(int cont=0;cont<fatture.length;cont++) {
			System.out.println(cont+1+")"+"Fattura cliente=" + fatture[cont].cliente + ", numero=" + fatture[cont].numero + ", data=" + fatture[cont].data + ", imponibile=" + fatture[cont].imponibile);
		}
		System.out.println("STAMPA TOTALE PER CLIENTE CON ROTTURA DI CODICE");
		rotturaCodice(fatture);			
	}

	public void ricercaPerNumero(Fattura[] fatture, int num) {
		boolean flag = false;
		for(int index=0; index<10; index++) {
			if(num==fatture[index].numero) {
				System.out.println(index+1+")"+fatture[index]);
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il numero: "+num);
		}
	}

	public void ricercaPerDataFattura(Fattura[] fatture, String data) {
		boolean flag = false;
		for(int index=0; index<10; index++) {
			if(data.equals(fatture[index].data)) {
				System.out.println(index+1+")"+fatture[index]);
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con la data: "+data);
		}
	}

	public void ricercaPerCliente(Fattura[] fatture, String nome) {
		boolean flag = false;
		for(int index=0; index<10; index++) {
			if(nome.equals(fatture[index].cliente)) {
				System.out.println(index+1+")"+fatture[index]);
				flag = true;
			}
		}
		if (!flag) {
			System.out.println("Non ho trovato niente con il nome: "+nome);
		}
	}

	private void rotturaCodice (Fattura[] fatture) {
		int indice = 1;
		int conta = 1;
		double somma = 0;
		String codice = fatture[0].cliente;
		somma=somma+fatture[indice].imponibile;
		conta = conta+1;
		indice = indice+1;
		while(indice<fatture.length) {
			if(!codice.equals(fatture[indice].cliente)) {
				System.out.println("Chiave: "+codice+ "Somma: " +somma+ "Conteggio: "+conta);
				somma=0;
				conta=0;
				codice=fatture[indice].cliente;
			}
			somma=somma+fatture[indice].imponibile;
			conta = conta+1;
			indice = indice+1;
		}
		System.out.println("Cliente: " +codice+ " Totale fatturato: "+somma+ " Conteggio: "+conta);
	}
	
	//E' UN ALGORITMO CHE PERMETTE DI CREARE DEI GRUPPI DI RECORD(ENTITA') ASSOCIATI AD UNA STESSA CHIAVE
	//LA ROTTURA SI VERIFICA QUANDO LA CHIAVE CAMBIA

//	private void rotturaCodiceDue() {
//		int x=0;
//		int conta=0;
//		int somma=0;
//		String appCliente;
//		//istanza cf
//		//istanza fatture
//		cf.inserisciFatture(Fattura[] fatture);
//		cf.ordinaFatture(Fattura[] fatture);
//		appCliente=fatture[x].cliente;
//		do{
//			if fatture[x].cliente.equals(appCliente) {
//				conta++;
//				somma+=fatture[x].imponibile;
//			} else {
//				System.out.println("Numero fatture del cliente "+appCliente+":"+conta);
//				System.out.println("Totale fatturato del cliente "+appCliente+":"+somma);
//				appCliente=fatture[x].cliente;
//				conta=1;
//				somma=fatture[x].imponibile;
//			}
//			X++;
//		}while(X<fatture.length);
//		System.out.println("Numero fatture del cliente "+appCliente+":"+conta);
//		System.out.println("Totale fatturato del cliente "+appCliente+":"+somma);
//	}
}


