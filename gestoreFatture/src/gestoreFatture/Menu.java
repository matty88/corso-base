package gestoreFatture;

public class Menu {
	ControlloFatture cf = new ControlloFatture();
	Metodi m = new Metodi();
	
	private void ordina(Fattura[] fatture) {
		int scelta=0;
		do{
			System.out.println("1) ORDINA PER NUMERO FATTURA");
			System.out.println("2) ORDINA PER NOME CLIENTE (CON STAMPA ROTTURA CODICE)");
			System.out.println("3) TORNA AL MENU PRINCIPALE");
			scelta=m.leggiIntero("Scegli:");
			switch(scelta) {
			case 1:
				cf.ordinamentoFatturaNumero(fatture);
				break;
			case 2:
				cf.ordinamentoFatturaCliente(fatture);
				break;
			case 3:
			    break;
			default:
				System.out.println("Non hai inserito un elemento valido");
				break;
			}

		}while(scelta!=3);
	}
	
	private void ricerca(Fattura[] fatture) {
		int scelta=0;
		do{
			System.out.println("1) NUMERO FATTURA");
			System.out.println("2) DATA FATTURA");
			System.out.println("3) CLIENTE");
			System.out.println("4) TORNA AL MENU PRINCIPALE");
			scelta=m.leggiIntero("Scegli:");
			switch(scelta) {
			case 1:
				int numero = m.leggiIntero("Inserisci numero: ");
				cf.ricercaPerNumero(fatture, numero);
				break;
			case 2:
				String data = m.leggiStringa("Inserisci data: ");
				cf.ricercaPerDataFattura(fatture, data);
				break;
			case 3:
				String cliente = m.leggiStringa("Inserisci cliente: ");
				cf.ricercaPerCliente(fatture, cliente);
				break;
			case 4:
			    break;
			default:
				System.out.println("Non hai inserito un elemento valido");
				break;
			}

		}while(scelta!=4);
	}
	
	private void stampa(Fattura[] fatture) {

		int scelta=0;
		do{
			System.out.println("1) STAMPA FATTURA");
			System.out.println("2) STAMPA FATTURA CON IMPONIBILE TOTALE");
			System.out.println("3) STAMPA FATTURA CON IMPOSTA, IMPORTO E IMPONIBILE TOTALE");
			System.out.println("4) TORNA AL MENU PRINCIPALE");
			scelta=m.leggiIntero("Scegli:");
			switch(scelta) {
			case 1:
				cf.stampaFatture(fatture);
				break;
			case 2:
				cf.stampaFattureImponibile(fatture);
				break;
			case 3:
				cf.stampaFattureImpostaImporto(fatture);
				break;
			case 4:
			    break;
			default:
				System.out.println("Non hai inserito un elemento valido");
				break;
			}

		}while(scelta!=4);
	}

	public void menuIniziale(Fattura[] fatture) {
		int scelta=0;
		do{
			System.out.println("1) STAMPA");
			System.out.println("2) RICERCA");
			System.out.println("3) ORDINA");
			System.out.println("4) ESCI");
			scelta=m.leggiIntero("Scegli:");
			switch(scelta) {
			case 1:
				stampa(fatture);
				break;
			case 2:
				ricerca(fatture);
				break;
			case 3:
				ordina(fatture);
				break;
			case 4:
				break;
			default:
				System.out.println("Non hai inserito un elemento valido");
				break;
			}

		}while(scelta!=4);
	}

}
