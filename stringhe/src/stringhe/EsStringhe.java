package stringhe;

public class EsStringhe {
	//metodo lunghezza stringa
	public int getLunghezza(String stringa) {
		return stringa.length();                    
	}

	//metodo trim per togliere spazi a sx e dx da una stringa
	public void eliminaSpazi() {
		String stringaConSpazi ="         skill factory              ";
		System.out.println("*"+stringaConSpazi+"*");  
		System.out.println("*"+stringaConSpazi.trim()+"*");                   
	}

	//metodo tagliare stringa a prendere quello che mi serve
	public String getSottoStringa(String stringa,int inizio,int fine) {  //se non si inserisce l'argomento finale prender� la porzione di codice fino alla fine
		return stringa.substring(inizio, fine);
	}

	//metodo per stampare verticalmente e creare un vettore
	public void visualizzaVerticale(String stringa) {
		String[] paroleDellaStringa;
		paroleDellaStringa=stringa.split(" ");
		for(int x=0; x<paroleDellaStringa.length; x++) {
			System.out.println(paroleDellaStringa[x]);
		}
	}

	public void visualizzaVerticaleConSeparatore(String stringa, String separatore) {
		String[] paroleDellaStringa;
		paroleDellaStringa=stringa.split(separatore);
		for(int x=0; x<paroleDellaStringa.length; x++) {    //in questo caso length non ha le tonde perch� non � un metodo ma � una variabile che conta quanti valori ci sono nel vettore
			System.out.println(paroleDellaStringa[x]);
		}
	}


	//metodo per dividere una stringa in singoli caratteri
	public void visualizzaStringaVerticale(String stringa) {
		for(int x=0; x<stringa.length();x++) {
			System.out.println(stringa.charAt(x));
		}
	}


	//metodo ascii
	public void convertiASCII(String stringa) {
		System.out.println(stringa);
		for(int x=0; x<stringa.length();x++) {
			System.out.println((int)stringa.charAt(x)+"["+(int)stringa.charAt(x)+"]");
		}
	}


	//confronto stringhe < > altrimenti equals
	public int confrontaStringhe(String stringaUno, String stringaDue) {
		return stringaUno.compareTo(stringaDue);
	}

	//contiene
	public boolean contiene(String stringa, String sottoStringa) {
		return stringa.contains(sottoStringa);
	}

	//differenze tringhe e numeri
	public int convertiIntero(String stringa) {
		return Integer.parseInt(stringa);
	}

	public int convertiStringa(String stringa) {
		try {                                //controlla se c'� un errore
			return Integer.parseInt(stringa);
		} catch(NumberFormatException e) {
			System.out.println("Errore:" +stringa+ " non � un intero valido!");
			return -1;                        //per convenzione significa errore
		}
	}

	public double convertiDecimale(String stringa) {
		try {                                //controlla se c'� un errore
			return Double.parseDouble(stringa);
		} catch(NumberFormatException e) {
			System.out.println("Errore:" +stringa+ " non � un decimale valido!");
			return -1;                        //per convenzione significa errore
		}
	}

	//trucchetto
	public String convertiInteroInStringa(int intero) {
		return ""+intero;
	}

	public String convertiDecimaleInStringa(double decimale) {
		return ""+decimale;
	}
}
