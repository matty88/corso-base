package cicli;

import java.util.Scanner;

//IL CICLO E' UNA STRUTTURA CHE SI RIPETE SOLO SE CONDIZIONE E' VERA
//IL DO WHILE PERMETTE DI ESEGUIRE UN CICLO UNA VOLTA STABILITA UNA CONDIZIONE CHE VERRA' ESEGUITA ALMENO UNA VOLTA

public class cicli {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int cont;
		int conta;

		//---CICLO WHILE
		cont = 1;
		while(cont <= 3) {
			System.out.println(cont);
			cont++;
		}

		//---CICLO WHILE IN NEGATIVO
		conta = 5;
		while(conta >= 0) {
			System.out.println(conta);
			conta--;
		}


		//---CICLO FOR
		for(int i=0; i<=10; i++) {
			System.out.println(i);
		}


		//--CICLO DO WHILE
		String risposta;
		do {
			System.out.println("Hello world");
			Scanner scanner = new Scanner(System.in);
			System.out.println("Vuoi ripetere l'inserimento?");
			risposta = scanner.nextLine();
		} while(risposta.equalsIgnoreCase("si"));


		int numero;
		do {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Inserisci numero da 1 a 10");
			numero = Integer.parseInt(scanner.nextLine());
			if(numero<=10) {
				System.out.println(numero+" minore o uguale di 10.");
			} else {
				System.out.println(numero+" maggiore di 10. Ripeti inserimento");
			}
		} while(numero>10);
	}

	//--ES CICLO WHILE
	public static void whileEs() {
		String[] caratteri = {"*","p","-","5","#"};
		String carattereFine;
		int i;
		Scanner scanner = new Scanner(System.in);
		String risposta;
		do{
			i=0;
			System.out.println("inserisci il carattere di fine");
			carattereFine=scanner.nextLine();
			while(true) {                                          //CICLO INFINITO ESSENDO UNA CONDIZIONE SEMPRE VERA
				System.out.println(caratteri[i]);
				i++;
				if(i==caratteri.length || !caratteri[i].equals(carattereFine)) {   //ESCI QUANDO i E' UGUALE A 5 O QUANDO LEGGE IL CARATTERE FINE
					break;
				}
			}
			System.out.println("Vuoi continuare (S/N)");
			risposta= scanner.nextLine().toLowerCase();                         //SE NON SI VUOLE USARE IGNORE CASE
		}while(risposta.equals("s")|| risposta.equals("si"));
		System.out.println("Fine!");
	}


	//	public static String leggiStringa(String msg) {
	//		Scanner scanner = new Scanner(System.in);
	//		System.out.println(msg);
	//		return scanner.nextLine(); 
	//	}
}
